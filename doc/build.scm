;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2019-2025 Inria

;; To build the document, run:
;;
;;   guix build -f build.scm
;;
;; The result will be a directory containing the activity report as PS and
;; PDF.

(use-modules (guix) (gnu)
             (guix git-download)
             (guix modules)
             (guix profiles)
             (gnu packages autotools)
             (gnu packages gettext)
             (gnu packages ghostscript)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages imagemagick)
             (gnu packages lout)
             (gnu packages pkg-config)
             (gnu packages plotutils)
             (gnu packages skribilo)
             (gnu packages tex)
             (srfi srfi-1)
             (ice-9 match))

(define skribilo-with-commonmark
  (let ((commit "85947544bd57ddbfe44c1225e7cf694d100a7546")
        (revision "0"))
   (package/inherit skribilo
     (name "skribilo-with-commonmark")
     (version (git-version "0.19.0" revision commit))
     (source (origin
               (method git-fetch)
               (uri (git-reference
                     (url "https://git.savannah.gnu.org/git/skribilo.git")
                     (commit commit)))
               (file-name (git-file-name name version))
               (sha256
                (base32
                 "0xz3l9gc5cjywxwsb7l649cvl33ad313k8lr1ncdimxvc2qr0zvf"))))
    (native-inputs (list autoconf automake gnu-gettext pkg-config))
    (inputs (list guile-3.0
                  imagemagick
                  ghostscript                     ;for 'convert'
                  ploticus
                  lout))
    (propagated-inputs (list guile-reader
                             guile-lib
                             guile-commonmark)))))

(define %authoring-packages
  ;; Authoring tools needed.
  (append-map (lambda (package)
                (cons package
                      (match (package-transitive-propagated-inputs package)
                        (((labels packages) ...)
                         packages))))
              (list skribilo-with-commonmark)))

(define %artwork-repository
  (let ((commit "be38d5ad867dd1352a1660b5296d35fedb77bccd"))
    (origin
      (method git-fetch)
      (uri (git-reference
             (url "https://git.savannah.gnu.org/git/guix/guix-artwork.git")
             (commit commit)))
      (file-name (string-append "guix-artwork-" (string-take commit 7)
                                "-checkout"))
      (sha256
       (base32
        "1k1yif07zf1a4vd6b71x541zv8b64ra941gczp3m0r7mw517qwz0")))))

(define coreutils (specification->package "coreutils"))
(define sed (specification->package "sed"))
(define gawk (specification->package "gawk"))
(define guile-gcrypt (specification->package "guile-gcrypt"))



(define (split-cover)
  "Return the covert artwork split in two files: 'front.pdf' and 'back.pdf'."
  (define build
    (with-imported-modules '((guix build utils))
      #~(begin
          (use-modules (guix build utils))

          (define cover
            #+(file-append %artwork-repository
                           "/reports/guix-hpc-activity-report-2023/printables"
                           "/orbital-spheres-plus-cover-2024.pdf"))

          (setenv "PATH" #+(file-append ghostscript "/bin"))
          (mkdir #$output)

          ;; Get the back and front page.  Note: 842pt = 29.7cm and
          ;; 595pt = 21cm.
          (invoke "gs" "-o" (string-append #$output "/back.pdf")
                  "-sDEVICE=pdfwrite" "-sPAPERSIZE=a5"
                  "-dFIXEDMEDIA" cover)
          ;; XXX: 'cover.pdf' is actually 210x296mm, meaning 1mm too short,
          ;; hence the surprising crop box below.
          (invoke "gs" "-o" (string-append #$output "/front.pdf")
                  "-sDEVICE=pdfwrite" "-sPAPERSIZE=a5"
                  "-c" "[/CropBox [419 0 839 595]" "-c" " /PAGES pdfmark"
                  "-f" cover))))

  (computed-file "split-cover" build))

(define (markdown->latex file)
  "Read Markdown from FILE, process it through Skribilo, and return a
directory containing LaTeX files."
  (define build
    (with-extensions %authoring-packages
      #~(begin
          (use-modules (skribilo reader)
                       (skribilo output)
                       (skribilo engine)
                       (skribilo evaluator)
                       (skribilo ast)
                       (skribilo writer)
                       (skribilo module)
                       (ice-9 match))

          ;;; Update and/or replace the names.
          (define %editors
            ;; TODO: Automatically sort the list
            (list "Konrad Hinsen"
                  "Simon Tournier"))

          (define commonmark-reader
            (make-reader 'commonmark))

          (define latex-engine
            (find-engine 'latex))

          (define output-file
            (string-append #$output "/"
                           #$(string-append
                              (basename (local-file-name file) ".md")
                              ".tex")))

          ;; Guile-Commonmark relies on regexps to install an appropriate
          ;; locale.
          (setenv "GUIX_LOCPATH"
                  (string-append #+(libc-locales-for-target)
                                 "/lib/locale"))
          (setlocale LC_ALL "en_US.UTF-8")

          (markup-writer 'image latex-engine
                         #:options '(#:file #:url #:width #:height #:zoom)
                         #:action (lambda (n e) #t))

          (markup-writer 'url-ref latex-engine
                         #:options '(#:url #:text)
                         #:action (lambda (n e)
                                    (display "\\href{")
                                    (display (markup-option n #:url))
                                    (display "}{")
                                    (output (or (markup-option n #:text)
                                                (markup-option n #:url))
                                            e)
                                    (display "}\\footnote{\\textit{")
                                    (output (markup-option n #:url) e)
                                    (display "}}")))

          (markup-writer 'bold latex-engine
                         #:action (lambda (n e)
                                    (display "\\hl{")
                                    (output (markup-body n) e)
                                    (display "}")))

          (let ((writer (markup-writer-get '&prog-line latex-engine)))
            (markup-writer '&prog-line latex-engine
                           #:before (lambda (n e) #t) ;no line number
                           #:action (writer-action writer)
                           #:after (writer-after writer)))

          (let ((writer (markup-writer-get 'pre latex-engine)))
            (markup-writer 'pre latex-engine
                           #:before "\n\\vspace*{5mm}\n"
                           #:action (writer-action writer)
                           #:after "\n\\vspace*{5mm}\n\\noindent\n"))

          (engine-custom-set! latex-engine 'documentclass
                              "\\documentclass[12pt,a5paper]{article}")
          (engine-custom-set! latex-engine 'maketitle "")
          (engine-custom-set! latex-engine 'hyperref-usepackage "")
          (engine-custom-set! latex-engine 'usepackage "
\\usepackage[twoside, inner=1.5cm, outer=2.5cm, top=1.25cm, bottom=2.5cm]{geometry}
\\usepackage[T1]{fontenc} %improves positioning of umlauts
\\usepackage{xcolor}
\\usepackage{soul}
\\usepackage{microtype}
\\usepackage[all]{nowidow}
\\usepackage{pdfpages}

\\definecolor{guixdark}{HTML}{FFBF2D}
\\definecolor{guixlight}{HTML}{FFD92D}
\\definecolor{guixblue}{HTML}{3EB8D3}

\\sethlcolor{guixlight}

% Underline URL links for PDFs on screen.
\\PassOptionsToPackage{hyphens}{url}\\usepackage[pdfborder={0 0 0}, allbordercolors=guixblue, pdfborderstyle={/S/U/W 1}]{hyperref}

% Allow breaking of long URLs at _.
\\usepackage{xstring}
\\let\\oldtextit\\textit
\\renewcommand{\\textit}[1]{\\oldtextit{\\StrSubstitute[0]{#1}{\\_}{\\_\\allowbreak}}}

\\usepackage{FiraSans}
\\usepackage{GaramondLibre}
\\usepackage{inconsolata}

\\usepackage{titlesec}

\\newcommand{\\sectionnumfont}{%     % define font for chapter number
  \\bfseries
  \\fontsize{72}{72}%                % font size 72pt, baselineskip 72pt
  \\selectfont%                      % activate font
}

% Start each section on a new page.
\\newcommand{\\sectionbreak}{\\clearpage}

% Prevent orphans
\\setnoclub[3]

% Typeset section headers with Fira Sans.
\\titleformat{\\section}[display]
  {\\filleft\\bfseries}
  {\\vspace*{6\\baselineskip}\\filleft\\sectionnumfont\\textcolor{guixdark}{\\thesection}}
  {-1em}
  {\\Huge}
\\titleformat{\\subsection}
  {\\normalfont\\sffamily\\large\\raggedright\\bfseries}
  {\\thesubsection}{1em}{} ")

          (let* ((exp (match (call-with-input-file #$file commonmark-reader)
                        (('document body ...)
                         `(document ,@body
                                    (! "
% Insert blank pages for proper pagination of the booklet.
\\newpage
\\shipout\\null

\\newpage
\\includepdf[fitpaper=true, pages=-]{back.pdf}
")))))
                 (document (eval exp (make-user-module 'skribilo)))
                 (date (match (markup-option document #:date)
                         ((year month rest ...)
                          (let* ((month (string->number month))
                                 (month (cond
                                         ((= month 1) "January")
                                         ((= month 2) "February")
                                         ((= month 3) "March")
                                         (else "Too late!"))))
                            (string-append month " " year)))
                         (_ "Fix Skribilo CommonMark reader :-)")))
                 (authors (string-join
                           (map (lambda (author)
                                  (markup-option author #:name))
                                (markup-option document #:author))
                           ", "))
                 (editors (match %editors
                            ((one) one)
                            ((one two) (string-append one " and " two))
                            ((one two rest ...)
                             (fold (lambda (name res)
                                     (string-append name ", " res))
                                   (string-append two " and " one)
                                   rest)))))
            (engine-custom-set! latex-engine 'postdocument
                                (string-append "
\\includepdf[fitpaper=true, pages=-]{front.pdf}

\\pagenumbering{gobble}
\\clearpage
\\thispagestyle{empty}

\\vspace*{\\fill}
\\noindent
" date ".
\\par
\\vspace{1cm}
\\begin{flushleft}
\\noindent
Edited by " editors ".
\\par
\\vspace{5mm}
\\noindent
Written by " authors ".

\\vspace{1cm}
\\noindent
Published under the terms of the CC-BY-SA 4.0 license and those of the
GNU Free Documentation License (version 1.3 or later, with no Invariant
Sections, no Front-Cover Texts, and no Back-Cover Texts).

\\vspace{1cm}
\\noindent
Cover graphics by Luis Felipe, published under the terms of the
 CC-BY-SA 4.0 license.
\\end{flushleft}

\\newpage

\\pagenumbering{arabic}
\\vspace*{5cm}
"))
            (parameterize ((*current-engine* latex-engine))
              (mkdir #$output)
              (copy-file #+(file-append (split-cover) "/front.pdf")
                         (string-append #$output "/front.pdf"))
              (copy-file #+(file-append (split-cover) "/back.pdf")
                         (string-append #$output "/back.pdf"))
              (with-output-to-file output-file
                (lambda ()
                  (output (evaluate-document document latex-engine)
                          latex-engine))
                #:encoding "UTF-8"))))))

  (computed-file (string-append (basename (local-file-name file) ".md")
                                "-latex")
                 build
                 #:options '(#:env-vars (("COLUMNS" . "250")))))

(define* (latex->pdf directory file #:key
                     (fonts (list texlive-garamond-libre
                                  texlive-xkeyval
                                  texlive-xstring
                                  texlive-fontaxes
                                  texlive-inconsolata
                                  texlive-upquote
                                  texlive-fira)))
  "Build LaTeX source FILE, taken from DIRECTORY, and return the resulting
PDF."
  (define texlive
    (texlive-updmap.cfg (append (list texlive-pdfpages
                                      texlive-scheme-basic
                                      texlive-titlesec
                                      texlive-xcolor
                                      texlive-soul
                                      texlive-eso-pic
                                      texlive-etoolbox
                                      texlive-microtype
                                      texlive-ms
                                      texlive-nowidow
                                      texlive-everyshi
                                      texlive-pdflscape
                                      texlive-pdfjam
                                      texlive-pdfcrop
                                      texlive-pdfbook2
                                      texlive-iftex)
                                fonts)))

  (define tools
    (profile
     (content (packages->manifest
               (list texlive ghostscript coreutils grep sed gawk)))))

  (define build
    (with-extensions (list guile-gcrypt)    ;for (guix profiles) dependencies
      (with-imported-modules (source-module-closure
                              '((guix build utils)
                                (guix profiles)))
        #~(begin
            (use-modules (guix build utils)
                         (guix profiles))

            (define pdf-file
              (string-append #$output "/"
                             #$(basename file ".tex") ".pdf"))

            (define cover
              #+(file-append %artwork-repository
                             "/reports/guix-hpc-activity-report-2023/printables"
                             "/orbital-spheres-plus-cover-2024.pdf"))

            (mkdir #$output)

            (load-profile #$tools)

            (copy-recursively #$directory ".")
            (invoke "pdflatex" #$file)
            (install-file (string-append (basename #$file ".tex") ".pdf")
                          #$output)

            (setenv "PDFJAM_TEMP_DIR" (getcwd))
            (setenv "HOME" (getcwd))              ;placate TeX

            (call-with-output-file ".pdfjam.conf"
              (lambda (port)
                ;; The only true way to get pdfjam to write where we want
                ;; instead of /var/tmp.
                (format port "tempfileDir=~a~%" (getcwd))))

            (with-directory-excursion #$output
              ;; Create an A4 booklet.
              (invoke "pdfbook2" "-p" "a4paper" pdf-file
                      ;; margins:
                      "-o" "0" "-i" "0" "-t" "0" "-b" "0"))

            ;; pdfbook2 internally adds the hard-coded suffix '-book'.
            ;; Guix-HPC publishes the activity reports using the suffix
            ;; '-booklet'.
            (let* ((name (basename #$file ".tex"))
                   (book (string-append #$output "/" name "-book.pdf"))
                   (booklet (string-append #$output "/" name "-booklet.pdf")))
              (rename-file book booklet))))))

  (computed-file (basename file ".tex") build))


(latex->pdf (markdown->latex (local-file "../posts/activity-report-2024.md"))
            "activity-report-2024.tex")
