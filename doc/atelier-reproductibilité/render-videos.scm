;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2021 Inria

;; To build videos of the talks, with openings and closings, run:
;;
;;   guix build -m render-videos.scm
;;
;; The result will a set of post-processed webm videos.

(use-modules (guix) (guix channels) (guix inferior) (guix profiles)
             (guix git-download) (guix modules)
             (gnu packages) (gnu packages tex)
             (srfi srfi-9)
             (ice-9 match))


;;;
;;; Rendering BigBlueButton videos.
;;;

(define bbb-render
  (origin
    (method git-fetch)
    (uri (git-reference (url "https://github.com/plugorgau/bbb-render")
                        (commit "a3c10518aedc1bd9e2b71a4af54903adf1d972e5")))
    (file-name "bbb-render-checkout")
    (sha256
     (base32 "1sf99xp334aa0qgp99byvh8k39kc88al8l2wy77zx7fyvknxjy98"))))

(define rendering-profile
  (profile
   (content (specifications->manifest
             '("gstreamer" "gst-editing-services" "gobject-introspection"
               "gst-plugins-base" "gst-plugins-good"
               "python-wrapper" "python-pygobject" "python-intervaltree")))))

(define* (video-ges-project bbb-data start end
                            #:key (webcam-size 25))
  "Return a GStreamer Editing Services (GES) project for the video,
starting at START seconds and ending at END seconds.  BBB-DATA is the raw
BigBlueButton directory as fetched by bbb-render's 'download.py' script.
WEBCAM-SIZE is the percentage of the screen occupied by the webcam."
  (computed-file "video.ges"
                 (with-extensions (list (specification->package "guile-gcrypt"))
                  (with-imported-modules (source-module-closure
                                          '((guix build utils)
                                            (guix profiles)))
                    #~(begin
                        (use-modules (guix build utils) (guix profiles)
                                     (guix search-paths) (ice-9 match))

                        (define search-paths
                          (profile-search-paths #+rendering-profile))

                        (for-each (match-lambda
                                    ((spec . value)
                                     (setenv
                                      (search-path-specification-variable
                                       spec)
                                      value)))
                                  search-paths)

                        ;; XXX: 'make-xges.py' has '--opening-credits' and
                        ;; '--closing-credits' options, but this lead to
                        ;; blank frames for some reason.  Thus, we resort to
                        ;; ffmpeg concatenation below.
                        (invoke "python"
                                #+(file-append bbb-render "/make-xges.py")
                                #+bbb-data #$output
                                "--start" #$(number->string start)
                                "--end" #$(number->string end)
                                "--webcam-size"
                                #$(number->string webcam-size)))))))

(define (ges->webm ges webm)
  "Render GES, a GStreamer Editing Services (GES) project, as WebM."
  (define gst-editing-services
    (specification->package "gst-editing-services"))

  (computed-file webm
                 (with-extensions (list (specification->package "guile-gcrypt"))
                   (with-imported-modules (source-module-closure
                                           '((guix build utils)
                                             (guix profiles)))
                     #~(begin
                         (use-modules (guix build utils) (guix profiles)
                                      (guix search-paths) (ice-9 match))

                         (define search-paths
                           (profile-search-paths #+rendering-profile))

                         (for-each (match-lambda
                                     ((spec . value)
                                      (setenv
                                       (search-path-specification-variable
                                        spec)
                                       value)))
                                   search-paths)

                         (invoke #+(file-append gst-editing-services
                                                "/bin/ges-launch-1.0")
                                 "--load" #+ges "-o" #$output
                                 "--format"
                                 "video/webm:video/x-vp8:audio/x-vorbis"))))))


;;;
;;; Openings and closings.
;;;

(define channels
  (list (channel
         (name 'guix)
         (url "https://git.savannah.gnu.org/git/guix.git")
         ;; https://data.guix.gnu.org/repository/1/branch/master/package/texlive
         (commit "c81457a5883ea43950eb2ecdcbb58a5b144bcd11"))))

(define inferior
  (inferior-for-channels channels))

(define texlive
  ;; XXX: This is terrible but we end using texlive@2018 (the big package);
  ;; texlive@2019 lacks Fira Sans.
  (car (lookup-inferior-packages inferior "texlive"))
  ;; (specification->package "texlive")
  ;; (texlive-union  (list texlive-base texlive-beamer
  ;;                       texlive-fonts-ec texlive-etoolbox
  ;;                       texlive-generic-ulem texlive-cm-super texlive-latex-capt-of
  ;;                       texlive-latex-wrapfig texlive-latex-geometry
  ;;                       texlive-latex-ms texlive-amsfonts texlive-latex-graphics
  ;;                       texlive-latex-pgf))
  )

(define locales (specification->package "glibc-utf8-locales"))
(define imagemagick (specification->package "imagemagick"))
(define ffmpeg (specification->package "ffmpeg"))

(define* (latex->pdf tex pdf
                     #:key
                     (speaker "SPEAKER") (title "TITLE"))
  (computed-file pdf
                 (with-imported-modules '((guix build utils))
                   #~(begin
                       (use-modules (guix build utils))

                       (setenv "GUIX_LOCPATH"
                               #+(file-append locales "/lib/locale"))
                       (setlocale LC_ALL "en_US.utf8")

                       (copy-file #$tex "source.tex")
                       (make-file-writable "source.tex")
                       (substitute* "source.tex"
                         (("TALK TITLE") #$title)
                         (("SPEAKER") #$speaker))
                       (copy-recursively #$(local-file "images"
                                                       #:recursive? #t)
                                         "images")

                       (let loop ((i 3))
                         (unless (zero? i)
                           (invoke #$(file-append texlive "/bin/pdflatex")
                                   "source.tex")
                           (loop (1- i))))
                       (copy-file "source.pdf" #$output)))))

(define (pdf->bitmap pdf png)
  (computed-file png
                 (with-imported-modules '((guix build utils))
                   #~(begin
                       (use-modules (guix build utils))
                       (invoke #$(file-append imagemagick "/bin/convert")
                               "-density" "300"
                               #$pdf #$output)))))

(define* (image->webm image webm
                      #:key (duration 5))
  (computed-file webm
                 (with-imported-modules '((guix build utils))
                   #~(begin
                       (use-modules (guix build utils))
                       (invoke #$(file-append ffmpeg "/bin/ffmpeg")
                               "-loop" "1" "-i" #$image
                               "-t" #$(number->string duration)
                               "-vcodec" "vp8" "-r" "24"
                               "output.webm")

                       ;; XXX: Add empty audio with a "channel layout" that
                       ;; matches that of VIDEO.  If we produce output
                       ;; without audio, the concatenation will lack audio
                       ;; entirely; if we produce output with a different
                       ;; audio channel, concatenation will have garbled
                       ;; audio.
                       (invoke #$(file-append ffmpeg "/bin/ffmpeg")
                               "-f" "lavfi"
                               "-i" "anullsrc=channel_layout=stereo:sample_rate=48000"
                               "-i" "output.webm"
                               "-c:v" "copy" "-c:a" "libvorbis" "-shortest"
                               #$output)))))

(define (opening title speaker)
  (image->webm
   (pdf->bitmap (latex->pdf (local-file "opening.tex") "opening.pdf"
                            #:title title #:speaker speaker)
                "opening.jpg")
   "opening.webm" #:duration 5))

(define closing.webm
  (image->webm
   (pdf->bitmap (latex->pdf (local-file "closing.tex") "closing.pdf")
                "closing.jpg")
   "closing.webm"
   #:duration 3))

(define (video-with-opening/closing name video opening closing)
  (computed-file name
                 (with-imported-modules '((guix build utils))
                   #~(begin
                       (use-modules (guix build utils)
                                    (ice-9 format))

                       (symlink #+opening "opening.webm")
                       (symlink #+video "video.webm")
                       (symlink #+closing "closing.webm")
                       (call-with-output-file "list.txt"
                         (lambda (port)
                           (format port "~{file ~a~%~}"
                                   '("opening.webm" "video.webm"
                                     "closing.webm"))))

                       (invoke #+(file-append ffmpeg "/bin/ffmpeg")
                               "-f" "concat"
                               "-i" "list.txt" "-c" "copy"
                               "output.webm")
                       (copy-file "output.webm" #$output)))))


;;;
;;; Putting it all together.
;;;

(define-record-type <talk>
  (talk title speaker start end cam-size data)
  talk?
  (title     talk-title)
  (speaker   talk-speaker)
  (start     talk-start)                      ;start time in seconds
  (end       talk-end)                        ;end time
  (cam-size  talk-webcam-size)                ;percentage used for the webcam
  (data      talk-bbb-data))                  ;BigBlueButton data

(define raw-bbb-data/monday
  ;; The raw BigBlueButton data as returned by './download.py URL', where
  ;; 'download.py' is part of bbb-render.
  (local-file "bbb-video-data.monday" "bbb-video-data"
              #:recursive? #t))

(define raw-bbb-data/tuesday
  (local-file "bbb-video-data.tuesday" "bbb-video-data"
              #:recursive? #t))

(define talks
  (list (talk "Concilier le calcul haute performance avec l'utilisation de bibliothèques tierces ?"
              "Emmanuel Agullo"
              (+ (* 1 60) 43) (+ (* 38 60) 59)
              0                                   ;no webcam
              raw-bbb-data/monday)
        (talk "Douze ans de plateforme — des Modules à Guix"
              "François Rué"
              (+ (* 40 60) 15) (+ (* 76 60) 50)
              25
              raw-bbb-data/monday)
        (talk "Génération d'un environnement contrôlé via Debian Snapshot Archive"
              "Arnaud Legrand"
              10 (+ (* 45 60) 31)
              25;%
              raw-bbb-data/tuesday)
        (talk "Guix comme gestionnaire d'environnement logiciel sur un mésocentre HPC"
              "Pierre-Antoine Bouttier"
              (+ (* 46 60) 22) (+ (* 92 60) 09)
              0                                   ;no webcam
              raw-bbb-data/tuesday)
        (talk "Guix et Org mode, deux amis du doctorant sur le chemin vers une thèse reproductible"
              "Marek Felšöci"
              (+ (* 92 60) 29) (+ (* 128 60) 49)
              0                                   ;no webcam
              raw-bbb-data/tuesday)))

(define (canonicalize-string str)
  (string-map (lambda (chr)
                (cond ((eqv? chr #\space) #\-)
                      ((char-set-contains? char-set:ascii chr) chr)
                      (else #\-)))
              (string-downcase str)))

(define (talk->video talk)
  "Given a talk, return a complete video, with opening and closing."
  (define file-name
    (string-append (canonicalize-string (talk-speaker talk))
                   ".webm"))

  (let ((raw (ges->webm (video-ges-project (talk-bbb-data talk)
                                           (talk-start talk)
                                           (talk-end talk)
                                           #:webcam-size
                                           (talk-webcam-size talk))
                        file-name))
        (opening (opening (talk-title talk) (talk-speaker talk))))
    (video-with-opening/closing file-name raw
                                opening closing.webm)))

(define (talk->manifest-entry talk)
  (manifest-entry
    (name (canonicalize-string (talk-speaker talk)))
    (version "0")
    (item (talk->video talk))))

(manifest (map talk->manifest-entry talks))
