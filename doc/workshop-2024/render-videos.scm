;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2021, 2024 Inria

;; To build videos of the talks, with openings and closings, run:
;;
;;   guix build -m render-videos.scm
;;
;; The result will a set of post-processed webm videos.

(use-modules (guix) (guix channels) (guix inferior) (guix profiles)
             (guix git-download) (guix modules)
             (gnu packages) (gnu packages tex)
             (srfi srfi-9)
             (ice-9 match))


;;;
;;; Rendering BigBlueButton videos.
;;;

(define bbb-render
  (origin
    (method git-fetch)
    (uri (git-reference (url "https://github.com/plugorgau/bbb-render")
                        (commit "f93f900aaa4cd469d3e1479e1027687c41d1c877")))
    (file-name "bbb-render-checkout")
    (sha256
     (base32 "16ffrhniggmy47rld599ncbf2s4l7gyjgx9pj6jij7dbl8y0j2lb"))))

(define rendering-profile
  (profile
   (content (specifications->manifest
             '("gstreamer" "gst-editing-services" "gobject-introspection"
               "gst-plugins-base" "gst-plugins-good" "gst-plugins-bad"
               "python-wrapper" "python-pygobject" "python-intervaltree")))))

(define* (video-ges-project bbb-data start end
                            #:key (webcam-size 25))
  "Return a GStreamer Editing Services (GES) project for the video,
starting at START seconds and ending at END seconds.  BBB-DATA is the raw
BigBlueButton directory as fetched by bbb-render's 'download.py' script.
WEBCAM-SIZE is the percentage of the screen occupied by the webcam."
  (computed-file "video.ges"
                 (with-extensions (list (specification->package "guile-gcrypt"))
                  (with-imported-modules (source-module-closure
                                          '((guix build utils)
                                            (guix profiles)))
                    #~(begin
                        (use-modules (guix build utils) (guix profiles)
                                     (guix search-paths) (ice-9 match))

                        (load-profile #+rendering-profile)

                        ;; XXX: 'make-xges.py' has '--opening-credits' and
                        ;; '--closing-credits' options, but this lead to
                        ;; blank frames for some reason.  Thus, we resort to
                        ;; ffmpeg concatenation below.
                        (invoke "python"
                                #+(file-append bbb-render "/make-xges.py")
                                #+bbb-data #$output
                                "--start" #$(number->string start)
                                "--end" #$(number->string end)
                                "--webcam-size"
                                #$(number->string webcam-size)))))))

(define (ges->webm ges webm)
  "Render GES, a GStreamer Editing Services (GES) project, as WebM."
  (define gst-editing-services
    (specification->package "gst-editing-services"))

  (computed-file webm
                 (with-extensions (list (specification->package "guile-gcrypt"))
                   (with-imported-modules (source-module-closure
                                           '((guix build utils)
                                             (guix profiles)))
                     #~(begin
                         (use-modules (guix build utils) (guix profiles)
                                      (guix search-paths) (ice-9 match))

                         (load-profile #+rendering-profile)

                         (invoke #+(file-append gst-editing-services
                                                "/bin/ges-launch-1.0")
                                 "--load" #+ges "-o" #$output
                                 "--format"
                                 "video/webm:video/x-vp8:audio/x-vorbis"))))))


;;;
;;; Openings and closings.
;;;

(define texlive
  (profile
   (content
    (packages->manifest

     (list texlive-scheme-basic texlive-beamer
           texlive-ec texlive-etoolbox
           texlive-ulem  texlive-capt-of
           texlive-wrapfig texlive-geometry
           texlive-ms texlive-graphics
           texlive-pgf texlive-translator
           texlive-xkeyval texlive-mweights texlive-fontaxes
           texlive-textpos texlive-fancyvrb texlive-xifthen
           texlive-ifmtarg texlive-upquote
           texlive-babel-french

           ;; Additional fonts.
           texlive-cm-super texlive-amsfonts
           texlive-fira texlive-inconsolata)))))

(define locales (specification->package "glibc-locales"))
(define imagemagick (specification->package "imagemagick"))
(define ffmpeg (specification->package "ffmpeg"))

(define* (latex->pdf tex pdf
                     #:key
                     (speaker "SPEAKER") (title "TITLE"))
  (computed-file pdf
                 (with-imported-modules (source-module-closure
                                         '((guix build utils)
                                           (guix profiles)))
                   #~(begin
                       (use-modules (guix build utils)
                                    (guix profiles))

                       (setlocale LC_ALL "C.UTF-8")

                       (copy-file #$tex "source.tex")
                       (make-file-writable "source.tex")
                       (substitute* "source.tex"
                         (("TALK TITLE") #$title)
                         (("SPEAKER") #$speaker))
                       (copy-recursively #$(local-file "images"
                                                       #:recursive? #t)
                                         "images")

                       (setenv "HOME" (getcwd))
                       (load-profile #+texlive)

                       (let loop ((i 3))
                         (unless (zero? i)
                           (invoke "pdflatex" "source.tex")
                           (loop (1- i))))
                       (copy-file "source.pdf" #$output)))))

(define (pdf->bitmap pdf png)
  (computed-file png
                 (with-imported-modules '((guix build utils))
                   #~(begin
                       (use-modules (guix build utils))
                       (invoke #$(file-append imagemagick "/bin/convert")
                               "-density" "300"
                               #$pdf #$output)))))

(define* (image->webm image webm
                      #:key (duration 5))
  (computed-file webm
                 (with-imported-modules '((guix build utils))
                   #~(begin
                       (use-modules (guix build utils))
                       (invoke #$(file-append ffmpeg "/bin/ffmpeg")
                               "-loop" "1" "-i" #$image
                               "-t" #$(number->string duration)
                               "-vcodec" "vp8" "-r" "24"
                               "output.webm")

                       ;; XXX: Add empty audio with a "channel layout" that
                       ;; matches that of VIDEO.  If we produce output
                       ;; without audio, the concatenation will lack audio
                       ;; entirely; if we produce output with a different
                       ;; audio channel, concatenation will have garbled
                       ;; audio.
                       (invoke #$(file-append ffmpeg "/bin/ffmpeg")
                               "-f" "lavfi"
                               "-i" "anullsrc=channel_layout=stereo:sample_rate=48000"
                               "-i" "output.webm"
                               "-c:v" "copy" "-c:a" "libvorbis" "-shortest"
                               #$output)))))

(define (opening title speaker)
  (image->webm
   (pdf->bitmap (latex->pdf (local-file "opening.tex") "opening.pdf"
                            #:title title #:speaker speaker)
                "opening.jpg")
   "opening.webm" #:duration 5))

(define closing.webm
  (image->webm
   (pdf->bitmap (latex->pdf (local-file "closing.tex") "closing.pdf")
                "closing.jpg")
   "closing.webm"
   #:duration 3))

(define (video-with-opening/closing name video opening closing)
  (computed-file name
                 (with-imported-modules '((guix build utils))
                   #~(begin
                       (use-modules (guix build utils)
                                    (ice-9 format))

                       (symlink #+opening "opening.webm")
                       (symlink #+video "video.webm")
                       (symlink #+closing "closing.webm")
                       (call-with-output-file "list.txt"
                         (lambda (port)
                           (format port "~{file ~a~%~}"
                                   '("opening.webm" "video.webm"
                                     "closing.webm"))))

                       (invoke #+(file-append ffmpeg "/bin/ffmpeg")
                               "-f" "concat"
                               "-i" "list.txt"
                               ;; "-c" "copy"

                               ;; Normalize audio.  See
                               ;; <https://ffmpeg.org/ffmpeg-all.html#speechnorm>.
                               "-filter:a" "speechnorm=e=25:r=0.0001:l=1"

                               "output.webm")
                       (copy-file "output.webm" #$output)))))


;;;
;;; Putting it all together.
;;;

(define-record-type <talk>
  (talk title speaker start end cam-size data)
  talk?
  (title     talk-title)
  (speaker   talk-speaker)
  (start     talk-start)                      ;start time in seconds
  (end       talk-end)                        ;end time
  (cam-size  talk-webcam-size)                ;percentage used for the webcam
  (data      talk-bbb-data))                  ;BigBlueButton data

(define raw-bbb-data
  ;; The raw BigBlueButton data as returned by './download.py URL', where
  ;; 'download.py' is part of bbb-render.
  (local-file "bbb-video-data" "bbb-video-data"
              #:recursive? #t))

(define-syntax t
  (syntax-rules (:)
    ((_ minutes : seconds)
     (+ (* minutes 60) seconds))
    ((_ hours : minutes : seconds)
     (+ (* hours 3600) (* minutes 60) seconds))))

(define talks
  (list (talk "Autonomie, flexibilité et reproductibilité : le pari de Guix-HPC"
              "Ludovic Courtès"
              (t 17 : 54) (t 52 : 25)
              0
              raw-bbb-data)
        (talk "Utilisation de Guix au mésocentre GLiCID"
              "Yann Dupont"
              (t 56 : 56) (t 1 : 50 : 24)
              0
              raw-bbb-data)
        (talk "Retour d'expérience sur l'utilisation de Guix dans le cadre
d'un programme de recherche"
              "Benjamin Arrondeau"
              (t 1 : 51 : 20) (t 2 : 26 : 44)
              0
              raw-bbb-data)
        (talk "Utilisation de Guix pour la gestion d'environnements
reproductibles, flexibles et collaboratifs dans le cadre d'une thèse"
              "Antoine Gicquel"
              (t 2 : 27 : 57) (t 3 : 13 : 09)
              0
              raw-bbb-data)
        (talk "Déploiement logiciel sur les supercalculateurs nationaux
avec Guix : un retour d'expérience"
              "Romain Garbage"
              (t 3 : 14 : 32) (t 4 : 00 : 23)
              0
              raw-bbb-data)
        (talk "Table ronde : déploiement de Guix dans les mésocentres"
              "Valéry Ozenne, Yann Dupont, Pierre-Antoine Bouttier, Philippe Virouleau, Emmanuel Agullo"
              (t 4 : 02 : 27) (t 5 : 13 : 25)
              100
              raw-bbb-data)))

(define (canonicalize-string str)
  (string-map (lambda (chr)
                (cond ((memv chr '(#\space #\, #\/)) #\-)
                      ((char-set-contains? char-set:ascii chr) chr)
                      (else #\-)))
              (string-downcase str)))

(define (talk->video talk)
  "Given a talk, return a complete video, with opening and closing."
  (define file-name
    (string-append (canonicalize-string (talk-speaker talk))
                   ".webm"))

  (let ((raw (ges->webm (video-ges-project (talk-bbb-data talk)
                                           (talk-start talk)
                                           (talk-end talk)
                                           #:webcam-size
                                           (talk-webcam-size talk))
                        file-name))
        (opening (opening (talk-title talk) (talk-speaker talk))))
    (video-with-opening/closing file-name raw
                                opening closing.webm)))

(define (talk->manifest-entry talk)
  (manifest-entry
    (name (canonicalize-string (talk-speaker talk)))
    (version "0")
    (item (talk->video talk))))

(manifest (map talk->manifest-entry talks))
