;;; This module is part of Guix-HPC and is licensed under the same terms,
;;; those of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2017, 2020-2024 Inria
;;; Copyright © 2017 Ludovic Courtès
;;; Copyright © 2015 David Thompson <davet@gnu.org>
;;; Copyright © 2016 Christopher Allan Webber <cwebber@dustycloud.org>

(define-module (guix-hpc)
  #:use-module (haunt post)
  #:use-module (haunt page)
  #:use-module (haunt site)
  #:use-module (haunt html)
  #:use-module (haunt utils)
  #:use-module (haunt reader)
  #:use-module (haunt reader commonmark)
  #:autoload   (haunt artifact) (serialized-artifact)
  #:use-module (syntax-highlight)
  #:use-module (syntax-highlight scheme)
  #:use-module (syntax-highlight lexers)
  #:use-module (sxml simple)
  #:use-module ((events program)
                #:select (talk?
                          talk-video
                          talk->video-page
                          talk-id))
  #:use-module ((events workshop-2023 program)
                #:prefix workshop-2023:)
  #:use-module (ice-9 match)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-26)
  #:export (base-url
            image-url
            css-url
            post-url

            change-image-to-video
            full-width-images
            syntax-highlight
            base-layout

            static-pages
            atom-feed

            video-pages)
  #:declarative? #f)

(define (base-url . location)
  (string-concatenate (cons "" location)))

(define (image-url location)
  (base-url "/static/images" location))

(define (css-url location)
  (base-url "/static/css" location))

(define (post-url post site)
  "Return the URL of POST, a Haunt blog post, for SITE."
  (let ((date (post-date post)))
    (base-url "/blog/"
              (number->string (date-year date))
              "/"
              (string-pad (number->string (date-month date))
                          2 #\0)

              ;; There's an implicit "/index.html" here.
              "/" (site-post-slug site post))))


;;;
;;; Syntax highlighting (stolen from Guix's web site.)
;;;

(define %default-special-prefixes
  '("define" "syntax"))

(define lex-scheme/guix
  ;; Specialized lexer for the Scheme we use in Guix.
  ;; TODO: Add #~, #$, etc.
  (make-scheme-lexer (cons* "with-imported-modules"
                            "gexp" "ungexp"
                            "ungexp-native" "ungexp-splicing"
                            "ungexp-native-splicing"
                            "mlet" "mlet*"
                            "match"
                            %default-special-symbols)
                     %default-special-prefixes))

(define (syntax-highlight sxml)
  "Recurse over SXML and syntax-highlight code snippets."
  (match sxml
    (('code ('@ ('class "language-scheme")) code-snippet)
     `(code ,(highlights->sxml
              (highlight lex-scheme/guix code-snippet))))
    ((tag ('@ attributes ...) body ...)
     `(,tag (@ ,@attributes) ,@(map syntax-highlight body)))
    ((tag body ...)
     `(,tag ,@(map syntax-highlight body)))
    ((? string? str)
     str)))


(define %default-footer
  `(div (@ (id "collaboration"))
        (div (@ (id "collaboration-inner"))
             (div (@ (class "members"))
                  (ul
                   (li (img (@ (alt "MDC")
                               (src ,(image-url "/mdc.png")))))
                   (li (img (@ (alt "Inria")
                               (src ,(image-url "/inria.png")))))
                   (li (img (@ (alt "UBC")
                               (src ,(image-url "/ubc.png")))))
                   (li (img (@ (alt "UTHSC")
                               (src ,(image-url "/uthsc.png"))))))))))

(define (event-footer event)
  ;; Footer for events.
  (match event
    ("2023/workshop"
     `(div (@ (id "sponsors"))
           (div (@ (id "sponsors-inner"))
                (div (@ (class "sponsored-by")) "Sponsors")
                (div (@ (class "members"))
                     (ul
                      (li (img (@ (alt "Institut de science des données")
                                  (src ,(image-url "/isdm.png"))))
                          (img (@ (alt "Institut Agro")
                                  (src ,(image-url "/institut-agro.png"))))
                          (img (@ (alt "Euro-CC2")
                                  (src ,(image-url "/eurocc2.jpg"))))
                          (img (@ (alt "Inria Academy")
                                  (src ,(image-url
                                         "/inria-academy.png"))))))))))
    ("2024/workshop"
     `(div (@ (id "sponsors"))
           (div (@ (id "sponsors-inner"))
                (div (@ (class "sponsored-by")) "Sponsors")
                (div (@ (class "members"))
                     (ul
                      (li (img (@ (alt "Inria")
                                  (src ,(image-url "/inria.png"))))))))))))

(define (event-menu event)
  ;; Top navigation menu for EVENT.
  (match event
    ("2023/workshop"
     (let ((workshop-url (lambda (path)
                           (base-url "/events/2023/workshop"
                                     path))))
       `(ul
         (li (a (@ (href ,(workshop-url "/")))
                "Workshop"))
         (li (a (@ (href ,(workshop-url "/venue")))
                "Venue"))
         (li (a (@ (href ,(workshop-url "/program")))
                "Program"))
         (li (a (@ (href ,(workshop-url "/speakers")))
                "Speakers")))))
    ("2024/workshop"
     (let ((workshop-url (lambda (path)
                           (base-url "/events/2024/workshop"
                                     path))))
       `(ul
         (li (a (@ (href ,(workshop-url "/")))
                "Workshop"))
         (li (a (@ (href ,(workshop-url "/program")))
                "Program"))
         (li (a (@ (href ,(workshop-url "/speakers")))
                "Speakers")))))))

(define* (base-layout body #:key (title "Guix-HPC") (meta '())
                      (posts '()) site)
  (define (post->brief post)
    `(li (@ (class "news-brief"))
         (a (@ (href ,(post-url post site)))
            ,(post-ref post 'title))))

  `((doctype "html")
    (html (@ (lang "en"))
          (head
           (meta (@ (http-equiv "Content-Type")
                    (content "text/html; charset=utf-8")))
           (link (@ (rel "icon")
                    (type "image/x-icon")
                    (href ,(image-url "/favicon.png"))))
           (link (@ (rel "stylesheet")
                    (href ,(css-url "/main.css"))
                    (type "text/css")
                    (media "screen")))
           ,@(if (assoc-ref meta 'event-page)
                 `((link (@ (rel "stylesheet")
                            (href ,(css-url "/events.css"))
                            (type "text/css")
                            (media "screen"))))
                 '())
           (title ,title))
          (body
           (div (@ (id "header")
                   ,@(if (assoc-ref meta 'frontpage)
                         '((class "frontpage"))
                         '()))
                (div (@ (id "header-inner")
                        (class "width-control"))
                     (a (@ (href ,(base-url "/")))
                        (img (@ (class "logo")
                                (src ,(image-url (if (assoc-ref meta 'frontpage)
                                                     "/logo.png"
                                                     "/logo-small.png"))))))
                     (div (@ (class "baseline"))
                          "Reproducible software deployment for high-performance computing.")))
           (div (@ (id "menubar")
                   (class "width-control"))
                ,(if (assoc-ref meta 'event-page)
                     (event-menu (assoc-ref meta 'event-page))
                     `(ul
                       (li (a (@ (href ,(base-url "/about")))
                              "About"))
                       (li (a (@ (href ,(base-url "/browse")))
                              "Packages"))
                       (li (a (@ (href ,(base-url "/channels")))
                              "Channels"))
                       (li (a (@ (href ,(base-url "/events")))
                              "Events"))
                       (li (a (@ (href ,(base-url "/blog")))
                              "Blog"))
                       (li (a (@ (href ,(base-url "/blog/feed.xml")))
                              (img (@ (alt "Atom feed")
                                      (src ,(image-url "/feed.png")))))))))

           (div (@ (id "content")
                   (class "width-control"))
                (div (@ (id "content-inner"))

                     ,@(if (assoc-ref meta 'frontpage)
                           `((div (@ (class "latest-news"))
                                  "LATEST ARTICLES"
                                  (ul ,@(map post->brief
                                             (take (posts/reverse-chronological posts) 3))
                                      (li (a (@ (href "/blog")) "More…")))))
                           '())

                     (article ,body)))

           ,(if (assoc-ref meta 'event-page)
                (event-footer (assoc-ref meta 'event-page))
                %default-footer)

           (div (@ (id "footer-box")
                   (class "width-control"))
                (p (a (@ (href "https://gitlab.inria.fr/guix-hpc/website"))
                      "Source of this site")))))))


;;;
;;; Static pages.
;;;

(define %cwd
  (and=> (assq-ref (current-source-location) 'filename)
         dirname))

(define (full-width-images sxml)
  "Add the 'full-width' class attribute to all 'img' tags of SXML so that
they get properly displayed in blog articles, except for images whose URL
starts with \"inline+\"."
  (let loop ((sxml sxml))
    (match sxml
      (('img ('@ attributes ...) rest ...)
       (let ((src (and=> (assoc-ref attributes 'src) car)))
         (if (and src (string-prefix? "inline+" src))
             `(img (@ (src ,(string-drop src (string-length "inline+")))
                      ,@(delete 'src attributes))
                   ,@rest)
             `(img (@ (class "full-width") ,@attributes)
                   ,@rest))))
      (((? symbol? tag) ('@ attributes ...) rest ...)
       `(,tag (@ ,@attributes) ,@(map loop rest)))
      (((? symbol? tag) rest ...)
       `(,tag ,@(map loop rest)))
      ((lst ...)
       (map loop lst))
      (x x))))

(define (change-image-to-video sxml)
  "Replace <img> tags in SXML that refer to WebM videos with proper <video>
tags.  This hack allows one to refer to a video from a Markdown document."
  (match sxml
    (('img ('@ attributes ...) body ...)
     (let ((src (match (assoc 'src attributes)
                  ((_ url) url)))
           (alt (match (assoc 'alt attributes)
                  ((_ text) text))))
       (if (string-suffix? ".webm" src)
           `(video (@ (src ,src)
                      (poster ,(string-append src ".poster.png"))
                      (controls "controls"))
                   (p (a (@ (href ,src) (class "link-subtle"))
                         "Download video.")))
           sxml)))
    ((tag ('@ attributes ...) body ...)
     `(,tag (@ ,@attributes) ,@(map change-image-to-video body)))
    ((tag body ...)
     `(,tag ,@(map change-image-to-video body)))
    ((? string? str)
     str)))

(define (decorate-page site posts meta body)
  (base-layout `(div (@ (class "post"))
                     (div (@ (class "post-body"))
                          ,(full-width-images
                            (change-image-to-video (syntax-highlight body)))))
               #:title (string-append "Guix-HPC — " (assoc-ref meta 'title))
               #:meta meta
               #:posts posts
               #:site site))

(define read-markdown
  (reader-proc commonmark-reader))

(define (read-markdown-page file posts site)
  "Read the CommonMark page from FILE.  Return its final SXML
representation."
  (let-values (((meta body)
                (read-markdown (string-append %cwd "/" file))))
    (decorate-page site posts meta body)))

(define (read-sxml-page file posts site)
  (define read-sxml
    (reader-proc sxml-reader))

  (let-values (((meta body)
                (read-sxml (string-append %cwd "/" file))))
    (decorate-page site posts meta body)))

(define (static-pages)
  (define (markdown-page html md)
    (lambda (site posts)
      (serialized-artifact html (read-markdown-page md posts site)
                           sxml->html)))

  (define channels
    (include "channels.scm"))

  (list (markdown-page "about/index.html" "about.md")
        ;; Channel page commented out: it's superseded by the same-named
        ;; page served by hpcguix-web 0.4.0.
        ;;
        ;; (lambda (site posts)
        ;;   (make-page "channels/index.html"
        ;;              (channel-registry-page
        ;;               (filter free-software-channel? channels))
        ;;              sxml->html))
        (lambda (site posts)
          (make-page "channels/non-free/index.html"
                     (channel-registry-page
                      (remove free-software-channel? channels)
                      #:title "Guix-HPC — Channel registry — non-free
software packages"
                      #:intro
                      '(div
                        (p "This page lists "
                            (a (@ (href
                                   "https://guix.gnu.org/manual/en/html_node/Channels.html"))
                               "channels")
                            " that provide packages for scientific and "
                            "high-performance computing (HPC).")
                        (div (@ (class "warning"))
                             (p "WARNING")
                             (p "The channels on this page provide "
                                (em "non-free software") "—software that
denies users " (a (@ (href "https://gnu.tools/en/documents/free-software/"))
                  "essential freedoms") ".  "
                  "From a scientific viewpoint, non-free software packages are black boxes;
relying on them is akin to relying on magic potions in chemistry.  It goes
against the principles of reproducible research and “open science”—it goes
against the principles of science.  "
                  (a (@ (href "https://hal.inria.fr/hal-01161771/en"))
                     "Read more about our views in this paper") ".")
                             (p "This page is provided for your convenience
but the Guix-HPC project recommends against the use of non-free software.
Check out " (a (@ (href "/channels")) "the wealth of channels providing free
software packages") "."))))
                     sxml->html))
        (markdown-page "events/index.html" "events/index.md")
        (markdown-page
         "events/2021/atelier-reproductibilité-environnements/index.html"
         "events/atelier-repro-francophone-2021.md")
        (lambda (site posts)
          (make-page "events/2023/workshop/index.html"
                     (read-sxml-page "events/workshop-2023.sxml" posts site)
                     sxml->html))
        (lambda (site posts)
          (make-page "events/2023/workshop/program/index.html"
                     (read-sxml-page "events/workshop-2023/program.sxml" posts site)
                     sxml->html))
        (lambda (site posts)
          (make-page "events/2023/workshop/speakers/index.html"
                     (read-sxml-page "events/workshop-2023/speakers.sxml"
                                     posts site)
                     sxml->html))
        (lambda (site posts)
          (make-page "events/2023/workshop/venue/index.html"
                     (read-sxml-page "events/workshop-2023/venue.sxml"
                                     posts site)
                     sxml->html))

        (lambda (site posts)
          (make-page "events/2024/workshop/index.html"
                     (read-sxml-page "events/workshop-2024.sxml" posts site)
                     sxml->html))
        (lambda (site posts)
          (make-page "events/2024/workshop/program/index.html"
                     (read-sxml-page "events/workshop-2024/program.sxml" posts site)
                     sxml->html))
        (lambda (site posts)
          (make-page "events/2024/workshop/speakers/index.html"
                     (read-sxml-page "events/workshop-2024/speakers.sxml"
                                     posts site)
                     sxml->html))

        (markdown-page "events/2021/user-developer-meetup/index.html"
                       "events/user-developer-meetup-2021.md")
        (markdown-page "events/2021/café-guix/index.html"
                       "events/café-guix-2021.md")
        (markdown-page "events/2022/café-guix/index.html"
                       "events/café-guix-2022.md")
        (markdown-page "events/2024/café-guix/index.html"
                       "events/café-guix-2023-2024.md")
        (markdown-page "events/2024-2025/café-guix/index.html"
                       "events/café-guix-2024-2025.md")
        (markdown-page "index.html" "getting-started.md")))


;;;
;;; Channel descriptions.
;;;

;; Description of a channel shown in the registry.
(define-record-type <channel-description>
  (%channel-description name url home-page logo-url
                        ci-badge ci-url
                        substitute-url substitute-key
                        synopsis introduction license policy)
  channel-description?
  (name         channel-description-name)
  (url          channel-description-url)
  (home-page    channel-description-home-page)
  (logo-url     channel-description-logo-url)
  (ci-badge     channel-description-ci-badge)
  (ci-url       channel-description-ci-url)
  (substitute-url channel-description-substitute-url)
  (substitute-key channel-description-substitute-key)
  (synopsis     channel-description-synopsis)
  (introduction channel-description-introduction) ;sexp | #f
  (license      channel-description-license)      ;string
  (policy       channel-description-policy))      ;<channel-policy>

(define* (channel-description name url
                              #:key (home-page url)
                              (logo-url "/static/images/favicon.png")
                              (ci-badge #f) (ci-url #f)
                              substitute-url substitute-key
                              (synopsis "") (introduction #f)
                              (license "GPLv3+")
                              (policy (channel-policy)))
  (%channel-description name url home-page logo-url ci-badge ci-url
                        substitute-url substitute-key
                        synopsis introduction license policy))

;; Policy of the channel.
(define-record-type <channel-policy>
  (%channel-policy free-software-only? build-from-source? unbundle?)
  channel-policy?
  (free-software-only? channel-policy-free-software-only?)
  (build-from-source?  channel-policy-build-from-source?)
  (unbundle?           channel-policy-unbundle?))

(define* (channel-policy #:key (free-software-only? #t)
                         (build-from-source? #t) (unbundle? #t))
  (%channel-policy free-software-only? build-from-source? unbundle?))

(define free-software-channel?
  ;; Whether the given channel provides exclusively free software.
  (compose channel-policy-free-software-only?
           channel-description-policy))

(define (channel-description->sxml channel)
  "Return SXML describing CHANNEL."
  (define (object->string* obj)
    (call-with-output-string
      (lambda (port)
        (pretty-print obj port))))

  `(li (@ (class "card"))
       (div (@ (class "card-header"))
            (a (@ (href ,(channel-description-home-page channel)))
               (img (@ (class "project-logo")
                       (src ,(channel-description-logo-url channel))
                       (alt ,(string-append
                              "Logo for the "
                              (symbol->string
                               (channel-description-name channel))
                              " channel")))))
            (a (@ (href ,(channel-description-home-page channel)))
               ,(symbol->string (channel-description-name channel))))
       (div (@ (class "card-body"))
            ,(channel-description-synopsis channel)

            ,@(if (channel-description-ci-url channel)
                  `((a (@ (class "ci-status")
                          (href ,(channel-description-ci-url channel)))
                       ,(if (channel-description-ci-badge channel)
                            `(img (@ (class "ci-badge")
                                     (src ,(channel-description-ci-badge
                                            channel))
                                     (alt "Continuous integration badge.")))
                            "continuous integration")))
                  '())

            (details
             (summary "channel configuration...")
             (div (@ (class "channel-data"))
                  "To use the " (code ,(channel-description-name channel))
                  " channel, add these lines to "
                  (a (@ (href
                         "https://guix.gnu.org/manual/en/html_node/Specifying-Additional-Channels.html"))
                     (code "~/.config/guix/channels.scm"))
                  ":"
                  (pre
                   (code
                    ,(object->string*
                      `(channel (name ',(channel-description-name channel))
                                (url ,(channel-description-url channel))
                                ,@(match (channel-description-introduction
                                          channel)
                                    (#f '())
                                    (sexp `((introduction ,sexp))))))))))

            ,@(match (channel-description-substitute-url channel)
                (#f '())
                (url
                 `((details
                    (summary "substitute configuration...")
                    (div (@ (class "channel-data"))
                         "To obtain "
                         (a (@ (href
                                "https://guix.gnu.org/manual/en/html_node/Substitutes.html"))
                            "substitutes")
                         " (pre-built binaries) for packages in this channel, add "
                         (code ,url) " and authorize this key:"
                         (pre (code ,(channel-description-substitute-key channel)))
                         (a (@ (href
                                "https://guix.gnu.org/manual/en/html_node/Getting-Substitutes-from-Other-Servers.html"))
                            "More info.")))))))))

(define %channel-registry-intro
  '(p "This page lists "
      (a (@ (href
             "https://guix.gnu.org/manual/en/html_node/Channels.html"))
         "channels")
      " that provide packages for scientific and "
      "high-performance computing (HPC)."))

(define* (channel-registry-page channels
                                #:key (intro %channel-registry-intro)
                                (title "Guix-HPC — Channel registry"))
  "Return SXML for the channel registry listing each of CHANNELS, a list of
channel descriptions."
  (base-layout `(div (@ (class "channel-registry"))
                     ,intro

                     (ul (@ (class "projects"))
                         ,@(map channel-description->sxml channels))

                     (p "Want to have your channel listed here?  Please "
                        "email "
                        (a (@ (href
                               "https://lists.gnu.org/mailman/listinfo/guix-science"))
                           "guix-science@gnu.org")
                        " details "
                        (a (@ (href
                               "https://gitlab.inria.fr/guix-hpc/website/-/blob/master/channels.scm"))
                           "about your channel") "."))
               #:title title))


;;;
;;; Atom feed (stolen from Haunt and adjusted).
;;;

;;; We cannot use Haunt's 'atom-feed' because of the non-default post URLs
;;; that we use.  Thus the code below is mostly duplicated from (haunt
;;; builder atom), with the exception of the URLs.

(define (sxml->xml* sxml port)
  "Write SXML to PORT, preceded by an <?xml> tag."
  (display "<?xml version=\"1.0\" encoding=\"utf-8\"?>" port)
  (sxml->xml sxml port))

(define (date->string* date)
  "Convert date to ISO-8601 formatted string."
  (date->string date "~4"))

(define* (post->atom-entry site post #:key (blog-prefix ""))
  "Convert POST into an Atom <entry> XML node."
  `(entry
    (title ,(post-ref post 'title))
    (author
     (name ,(post-ref post 'author))
     ,(let ((email (post-ref post 'email)))
        (if email `(email ,email) '())))
    (updated ,(date->string* (post-date post)))
    (link (@ (href ,(string-append blog-prefix (post-url post site)))
             (rel "alternate")))
    (summary (@ (type "html"))
             ,(sxml->html-string (post-sxml post)))))

(define* (atom-feed #:key
                    (file-name "feed.xml")
                    (subtitle "Recent Posts")
                    (filter posts/reverse-chronological)
                    (max-entries 20)
                    (blog-prefix ""))
  "Return a builder procedure that renders a list of posts as an Atom
feed.  All arguments are optional:

FILE-NAME: The page file name
SUBTITLE: The feed subtitle
FILTER: The procedure called to manipulate the posts list before rendering
MAX-ENTRIES: The maximum number of posts to render in the feed"
  (lambda (site posts)
    (make-page file-name
               `(feed (@ (xmlns "http://www.w3.org/2005/Atom"))
                      (title ,(site-title site))
                      (subtitle ,subtitle)
                      (updated ,(date->string* (current-date)))
                      (link (@ (href ,(string-append blog-prefix "/" file-name))
                               (rel "self")))
                      (link (@ (href ,(site-domain site))))
                      ,@(map (cut post->atom-entry site <>
                                  #:blog-prefix blog-prefix)
                             (take-up-to max-entries (filter posts))))
               sxml->xml*)))


;;;
;;; Video pages.
;;;

(define* (video-pages #:optional (talks workshop-2023:talks)
                      #:key
                      (event "2023/workshop")
                      (directory "/events/2023/workshop/video")
                      (slides-directory "/static/doc/workshop-2023")
                      (video-directory "/static/videos/workshop-2023")
                      (title-prefix "2023 Workshop Videos")
                      (footer ""))
  "Return video pages, one for each element of TALKS."
  (define talk-with-video? talk-video)

  (let loop ((talks talks)
             (previous #f)
             (pages '()))
    (match talks
      (()
       (reverse pages))
      (((? talk-with-video? talk) . rest)
       (let ((next (find talk-with-video? rest)))
         (loop rest talk
               (cons (lambda (site posts)
                       (serialized-artifact
                        (string-append directory "/" (talk-id talk)
                                       "/index.html")
                        (let* ((page (talk->video-page talk
                                                       #:event event
                                                       #:slides-directory
                                                       slides-directory
                                                       #:video-directory
                                                       video-directory
                                                       #:video-page-directory
                                                       directory
                                                       #:previous previous
                                                       #:next next
                                                       #:footer footer))
                               (body (assq-ref page 'content))
                               (meta (fold alist-delete page '(content))))
                          (base-layout `(div (@ (class "post"))
                                             (div (@ (class "post-body"))
                                                  ,body))
                                       #:title
                                       (string-append "Guix-HPC — "
                                                      title-prefix
                                                      " — "
                                                      (assoc-ref meta 'title))
                                       #:meta meta
                                       #:posts posts
                                       #:site site))
                        sxml->html))
                     pages))))
      ((_ . rest)
       (loop rest previous pages)))))
