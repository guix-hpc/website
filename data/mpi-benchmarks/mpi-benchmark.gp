# Gnuplot file to produce a plot for "IMB-MPI1 PingPong" from Intel MPI Benchmark.
# This reads "mpi-benchmark.data", produced by 'plot.scm'.

set terminal svg size 800,800 font "Fira Sans,16"
set output 'mpi-benchmark.svg'

set title "Intel MPI Benchmarks, PingPong (Adastra)"
set grid y
set grid x
set style textbox opaque
set key bottom right
set xtics nomirror rotate by -45 scale 0
set logscale x 2

set xlabel 'Message Size (Bytes)'
set ylabel 'Bandwidth (MB/s)'

plot "mpi-benchmark.data" using 1:2 with linespoints lw 2 lc 3 title 'Cray MPICH', \
     "mpi-benchmark.data" using 1:3 with linespoints lw 2 lc 4 title 'Open MPI (Guix)', \
     "mpi-benchmark.data" using 1:4 with linespoints lw 2 lc 4 title 'MPICH (Guix)'
