;; SPDX-License-Identifier: GPL-3-or-later

(use-modules (srfi srfi-1)
             (ice-9 match)
             (ice-9 rdelim))

(define (parse-intel-mpi-benchmark-output port)
  "Parse from @var{port} the output of @command{IMB-MPI1 PingPong} and
return an alist that contains the performance figures."
  (let loop ((result '())
             (prologue? #t))
    (match (read-line port)
      ((? eof-object?)
       (reverse result))
      (line
       ;; We expect a heading like this:
       ;; #---------------------------------------------------
       ;; # Benchmarking PingPong 
       ;; # #processes = 2 
       ;; #---------------------------------------------------
       (if (string-prefix? "# Benchmarking PingPong" line)
           (begin
             (let liip ((n 3))
               (unless (zero? n)
                 (read-line port)
                 (liip (- n 1))))
             (loop result #f))
           (if prologue?
               (loop result prologue?)
               (match (string-tokenize line)
                 (((= string->number message-size) (= string->number iterations)
                   (= string->number duration) (= string->number bandwidth))
                  (loop (cons `((message-size . ,message-size)
                                (iterations . ,iterations)
                                (duration . ,duration)
                                (bandwidth . ,bandwidth))
                              result)
                        #f))
                 (()
                  (reverse result)))))))))

(define (data-points data)
  "Return a list of pairs (data points) from @var{data}, an alist as produced
by @code{parse-intel-mpi-benchmark-output}."
  (fold-right (lambda (data lst)
                (if (< (assoc-ref data 'message-size) 4096)
                    lst
                    (cons (cons (assoc-ref data 'message-size)
                                (assoc-ref data 'bandwidth))
                          lst)))
              '()
              data))

;; XXX: Attempt to use Guile-Charting.
;;
;; (define (plot cray-mpich guix-openmpi)
;;   (make-scatter-plot
;;    "Intel MPI Benchmark Ping-Pong Performance"
;;    `(("Cray MPICH" ,@(data-points cray-mpich))
;;      ("Open MPI (Guix)" ,@(data-points guix-openmpi)))
;;    #:chart-height 400
;;    #:chart-width 600
;;    #:min-x 4096
;;    #:max-x 4194304
;;    #:log-x-base 2
;;    ;;#:x-ticks '(4096.0 8192.0 16384.0 32768.0 65536.0 131072.0 262144.0 524288.0 1048576.0 2097152.0)
;;    #:x-axis-label "Message size (bytes)"
;;    #:y-axis-label "Bandwidth (MB/s)"
;;    #:write-to-png "/tmp/graph.png"))

(define (assert c)
  (unless c
    (error "assertion failed")))

(define (output-gnuplot-data cray-mpich guix-openmpi guix-mpich port)
  "Write to @var{port} tabulated data for Gnuplot based on the given
benchmark data."
  (map (match-lambda*
         (((x1 . y1) (x2 . y2) (x3 . y3))
          (assert (= (pk 'x1 x1) (pk 'x2 x2) (pk 'x3 x3)))
          (format port "~a ~a ~a ~a~%" x1 y1 y2 y3)))
       (data-points cray-mpich)
       (data-points guix-openmpi)
       (data-points guix-mpich)))


;; Write to "mpi-benchmark.data" raw benchmark data for Gnuplot.
(let ((cray-mpich (call-with-input-file "libcxi-benchmark-with-cray-modules.txt"
                    parse-intel-mpi-benchmark-output))
      (guix-openmpi (call-with-input-file "libcxi-benchmark-with-guix-openmpi.txt"
                      parse-intel-mpi-benchmark-output))
      (guix-mpich (call-with-input-file "libcxi-benchmark-with-guix-mpich-ofi.txt"
                    parse-intel-mpi-benchmark-output)))

  ;; (plot cray-mpich guix-openmpi)
  (call-with-output-file "mpi-benchmark.data"
    (lambda (port)
      (output-gnuplot-data cray-mpich guix-openmpi guix-mpich port))))
