title: Café Guix (francophone)
author: Guix Hackers
date: 2021-10-26 13:00
tags: community
---

# Café Guix

![Image of a hot beverage and the words Café Guix.](/static/images/café-guix.png)

> _This page is about monthly hacking sessions for French-speaking
> scientists and HPC practitioners, covering a range of topics from user
> interface matters to deep packaging dives.  Read below for past and
> upcoming sessions._

Le Café Guix est un temps d’échange mensuel et informel autour du gestionnaire
d’environnement logiciel [GNU Guix](https://guix.gnu.org).  Étudiant·e·s,
chercheuses et chercheurs, administrateur·ice système, IT support de laboratoire
ou de Centre de Calcul, tout le monde est bienvenu·e dans ce rendez-vous
mensuel d’une heure où l’on discutera de questionnements apportés par chacun
sur Guix et sa pratique au sens large.

La participation aux Cafés Guix est libre, aucune obligation d’assiduité n’est
imposée.  En fonction des sujets abordés, il est fort possible que vous soyez
parfois peu concernés. Si vous voulez discuter d’un sujet spécifique
(par ex. interface utilisateur·ice, empaquetage, installation système, problème sur
un paquet spécifique), n’hésitez pas à nous en faire part en amont afin que
l’on puisse répondre aux envies de tout le monde.

Après la trève estivale, les cafés Guix sont de retour !
Ils auront lieu de façon générale les derniers mardis de chaque mois de 13h à 14h en visio sur ce lien :

> [▶ BigBlueButton](https://meet.univ-grenoble-alpes.fr/b/cel-dyj-m93-arv)
 — entrez votre nom et rejoignez-nous. N'hésitez pas à rejoindre [le groupe Mattermost
dédié](https://mattermost.univ-nantes.fr/signup_user_complete/?id=njdxbdazafddtq6wsm6cgrr95r)
pour prolonger la discussion entre deux sessions !

Nous avons choisi de labelliser les sessions par des petits logos qui indiquent le niveau de difficulté du thème présenté :


![Description des trois niveaux de difficulté.](/static/images/Niveaux.png)

Ces niveaux sont bien sûr donnés à titre indicatif.

## Programme des sessions 2022–2023

- 05/07/23 ![Niveau 2](inline+/static/images/logo-guix-verysmall.png)![Niveau 2](inline+/static/images/logo-guix-verysmall.png) : Création de machines virtuelles 

## Sessions 2022–2023

## Dernière session (05/07/2023, 13h00–14h00)
 ![Niveau 2](inline+/static/images/logo-guix-verysmall.png)![Niveau 2](inline+/static/images/logo-guix-verysmall.png)
 **Gérer une infrastructure virtuelle avec Guix**


 [📺](https://videos.univ-grenoble-alpes.fr/video/27361-cafe_guix_gerer_infrastructure_virtuelle_avec_guixmp4/)
 
Bien connu comme un gestionnaire de paquets logiciels particulièrement adapté au support d'expériences reproductibles, les fonctionnalités de Guix ne s’arrêtent pas là. Nous proposons d’explorer ce midi une partie des commandes « guix system », qui permettent de générer des systèmes d’exploitation complets et d’y injecter des caractéristiques intéressantes, telles que la cohérence et la reproductibilité. Nous commencerons par écrire des machines virtuelles minimales, pour progresser vers des déploiements plus complexes, puis explorer des déploiements utilisés en production sur le mésocentre GLiCID. Nous finirons par le dessert et essaierons de générer des VM pour des architectures matérielles exotiques.


## Douzième session (30 mai 2023, 13h00–14h00)
 ![Niveau 2](inline+/static/images/logo-guix-verysmall.png)![Niveau 2](inline+/static/images/logo-guix-verysmall.png)
 **Transformations de paquets pour utilisateurices HPC exigeant·es**

[📂](/static/doc/café-guix/Courtes_cafe-guix-20230530.md)  [📺](https://videos.univ-grenoble-alpes.fr/video/27180-cafe_guix_ludovic_courtes_30052023mp4/)

Les personnes qui travaillent en calcul intensif (HPC) ont la réputation
d’être exigeantes dès qu’il s’agit de flexibilité sur la manière de
construire les logiciels : on veut pouvoir combiner des versions
choisies, activer ou non certaines options de compilation, ajouter ou
non certaines dépendances optionnelles, etc.  Spack est connu pour
répondre à ces besoins en ligne de commande.

Dans cet esprit, mais dans un cadre où on a reproductibilité et
traçabilité par défaut, les options de transformations de paquets de
Guix permettent d’ajuster les logiciels à ses besoins depuis la ligne de
commande.  Nous verrons les principales transformations, du changement
de dépendances avec `--with-input` à l’optimisation pour une
micro-architecture avec `--tune`, en passant par le choix de révisions
spécifiques avec `--with-commit`.  Et bien sûr, ce sera l’occasion de
répondre à vos interrogations en la matière !


## Onzième session (09 mai 2023, 13h00–14h00)
session spéciale Thé Guix : **Guix et Nix sur un cluster HPC : mise en oeuvre.**

[📂](/static/doc/café-guix/Bzeznik_Nix_Guix_sur_cluster_HPC.pdf)  [📺](https://videos.univ-grenoble-alpes.fr/video/26922-the_guix_b_bzeznik_nix_guix_cluster_hpc_09052023mp4/) 
 
 Les systèmes de paquets Guix et Nix sont très intéressants pour déployer
facilement des applications en mode "user-level" et mettre en place des
environnements développement, en prenant en compte la reproductibilité.
Ce sont des outils de choix pour une infrastructure HPC, qui viennent
avantageusement concurrencer les plus classiques "modules
d'environnement" ou autres installations locales qui sont souvent trop
dépendantes des librairies systèmes.
Leur mise en place dans un environnement HPC est possible quelle que
soit la distribution Linux des noeuds de calcul, et est assez simple.
Nous discuterons des opérations minimales nécessaires afin de rendre
opérationnels Nix et Guix sur un cluster HPC et verrons également
quelques aspects particuliers concernant leur gestion et optimisation.


## Dixième session (07 avril 2023, 13h00–14h00)
 ![Niveau 2](inline+/static/images/logo-guix-verysmall.png)![Niveau 2](inline+/static/images/logo-guix-verysmall.png)
 **VSCode comme outil d’édition et son plugin guile, illustration avec la construction d'un paquet simple et détail des phases de build-system**

[📂](https://bouttiep.gricad-pages.univ-grenoble-alpes.fr/slides-v2/cafe_guix_20230407.html)  [📺](http://videos.univ-grenoble-alpes.fr/video/26660-cafe_guix_vscode_comme_outil_deditionmp4/) 
 
 Dans ce café Guix, un ingénieur support d’un centre de calcul (GRICAD) présentera comment il développe et met à disposition des paquets Guix pour les utilisateurs des supercalculateurs. De la configuration de VSCode (peut-être pas optimale) à la création d’un channel (à la configuration pas forcément complète) en passant par l’environnement de tests, le workflow et les pratiques concrètes (pas forcément bonnes) de la création de paquets seront ainsi présentées.


## Neuvième session (06 mars 2023, 13h00–14h00)
![Niveau 1](inline+/static/images/logo-guix-verysmall.png)
**Guix et le long terme : difficultés et contre-mesures**

[📂](/static/doc/café-guix/tournier-20230306.pdf)  [📺](http://videos.univ-grenoble-alpes.fr/video/26399-cafe_guix_guix_et_le_long_terme_difficultes_et_contre-mesures_vfinalemp4/)

Dans un contexte de recherche reproductible, un des enjeux est la capacité de
« capturer l’environnement computationnel » pour le redéployer ou l’inspecter.
Et il y a ainsi une dimension sous-jacente : le temps qui passe.  Des serveurs
ne sont plus atteignables, des sources ne sont plus disponibles, etc.  Dans ce
Café Guix, nous présenterons la problématique, la solution d’archivage
[Software Heritage](https://www.softwareheritage.org/) et comment Guix se
positionne.  Notre objectif est d’introduire la question « comment refaire
plus tard et là-bas ce qui a été fait aujourd’hui et ici ? », et d’amorcer une
discussion autour d’une autre question corollaire : comment citer un logiciel
?

## Huitième session (21 février 2023, 13h00–14h00)
![Niveau 2](inline+/static/images/logo-guix-verysmall.png)
![Niveau 2](inline+/static/images/logo-guix-verysmall.png)
![Niveau 2](inline+/static/images/logo-guix-verysmall.png)
**Quand empaqueter devient compliqué**

 [📺](http://videos.univ-grenoble-alpes.fr/video/26395-cafe_guix_quand_empaqueter_devient_complique_vfinalemp4/)
 
Pour donner une idée de ce qu’est un paquet, on commence généralement
par montrer [un cas simple](https://guix.gnu.org/manual/fr/html_node/Definition-des-paquets.html),
immédiatement compréhensible et rassurant.  Dans la réalité, il n’est pas rare que les choses se compliquent.

Dans cette session, nous verrons comment adapter une définition de paquet pour spécifier des options de configuration ou des drapeaux de
 compilation, changer les [phases de construction](https://guix.gnu.org/manual/fr/html_node/Phases-de-construction.html),
 manipuler des fichiers source ou installés, faire référence aux
 dépendances du paquet.

Nous retrouverons des notions abordées dans l’introduction au langage
 Scheme de la session du 29 novembre 2022, pour votre plus grand
 bonheur.  Bonus : vous êtes invité·es à venir avec vos propres soucis
 d’empaquetage pour en discuter !


## Septième session (29 novembre 2022, 13h00–14h00)
![Niveau 2](inline+/static/images/logo-guix-verysmall.png)
![Niveau 2](inline+/static/images/logo-guix-verysmall.png)
**Introduction à Scheme/Guile : déconstruisons un paquet**

[📂](/static/doc/café-guix/tournier-20221129.pdf)  [📺](http://videos.univ-grenoble-alpes.fr/video/25728-cafe-guix-introduction-a-schemeguile-deconstruisons-un-paquet/)

Les parenthèses, c'est « baroque » !  Et il y en a beaucoup avec Guix, de la
déclaration dans les fichiers `manifest` à la définition d'un paquet en
passant par Guix lui-même.  Grâce au langage Scheme
([Guile](https://www.gnu.org/software/guile/)), il y a une continuité allant
de l'implémentation des commandes Guix à la configuration déclarative dans un
langage dédié (*domain-specific language–DSL*).  Dans ce Café Guix, nous
proposons une introduction à quelques concepts de Guile pour mieux appréhender
les parenthèses et peut-être mieux apprécier le « néo-baroque ».

## Sixième session (25 octobre 2022, 13h00–14h00)

![Niveau débutant·e.](inline+/static/images/logo-guix-verysmall.png)
**Installer et utiliser facilement et de façon reproductible ses logiciels avec Guix**

[📂](https://gricad_formations.gricad-pages.univ-grenoble-alpes.fr/cafe-guix-20221025/)  [📺](http://videos.univ-grenoble-alpes.fr/video/25727-cafe-guix-installer-et-utiliser-facilement-et-de-facon-reproductible-ses-logiciels-avec-guix/)

Si le monde des environnements logiciels vous paraît difficile d’accès
mais que vous voulez mener vos expérimentations numériques dans un cadre
le plus reproductible possible, cette session est pour vous. Nous
découvrirons le gestionnaire d’environnements logiciels Guix, par
rapport à ce que vous pouvez connaître, puis nous verrons, très
concrètement, comment l’utiliser en quelques commandes, se comptant sur
les doigts d’une main.

### Cinquième session (7 juillet 2022)

[📂](/static/doc/café-guix/tournier-20220707.pdf)

Dans ce dernier Café Guix avant les vacances estivales nous parlerons des
[canaux](https://guix.gnu.org/manual/devel/fr/html_node/Canaux.html) pour
étendre la collection de paquets disponible.  Nous commencerons par un rappel
sur étendre localement la collection de paquets via `--load-path` puis comment
transformer ces définitions de paquet en canal partageable.  Pour finir, nous
amorcerons une discussion sur la maintenance de [canaux
tiers](https://hpc.guix.info/channels/).

Ce prochain Café Guix aura lieu le **7 juillet, de 13h30 à 14h30** [à cette
adresse.](https://meet.univ-grenoble-alpes.fr/b/pie-kls-hpu-opc)

### Quatrième session (15 avril 2022)

**Guix et le long terme : difficultés et contre-mesures**

[📂](/static/doc/café-guix/tournier-20220415.pdf)

> _En complément, nous recommandons ce
> [billet](https://simon.tournier.info/posts/2022-04-15-cafe-guix-long-term.html)
> (en anglais) qui donne des éléments sur les enjeux de la préservation de
> Guix sur le long terme._

Dans un contexte de recherche reproductible, un des enjeux est la capacité de
« capturer l’environnement computationnel » pour le redéployer ou l’inspecter.
Et il y a ainsi une dimension sous-jacente : le temps.  Dans ce quatrième Café
Guix de l’année, nous présenterons la problématique, la solution d’archivage
[Software Heritage](https://www.softwareheritage.org/) et comment Guix se
positionne.  Notre objectif est d’introduire la question « comment refaire
plus tard et là-bas ce qui a été fait aujourd’hui et ici ? », et d’amorcer une
discussion autour d’une autre question : comment peut-on utiliser Guix en
pratique pour y répondre ?

Ce prochain Café Guix aura lieu le **15 avril, de 13h à 14h** [à cette
adresse.](https://meet.univ-grenoble-alpes.fr/b/pie-kls-hpu-opc)

### Troisième session (11 mars 2022)

[📂](/static/doc/café-guix/swartvagher-20220311.pdf)

Pour ce troisième Café Guix de l'année, Philippe Swartvagher (doctorant à Inria
Bordeaux) nous présentera comment Guix peut aider dans la réalisation
d'expériences reproductibles, sur des machines HPC, et avec des piles
logicielles plutôt complexes. Seront évoqués : pourquoi utiliser Guix ; comment
se servir de Guix pour faire des expériences reproductibles (à présenter dans
un papier, par exemple) ; et enfin comment assurer la pérennité de ses scripts
et logiciels.

La présentation permettra à celles et ceux qui n'ont pas encore fait
d'expériences reproductibles avec Guix d'avoir un aperçu des différentes étapes
et pratiques possibles pour y parvenir.

Le prochain Café Guix aura lieu le **11 mars, de 13h à 14h** [à cette
adresse.](https://meet.univ-grenoble-alpes.fr/b/pie-kls-hpu-opc)


### Deuxième session (11 février 2022)

Lors des 3 derniers (et premiers, aussi) Cafés Guix, bon nombre de questions ont été posées sans que nous ayons forcément le temps d’y répondre exhaustivement. Ainsi, pour le prochain Café Guix, nous vous proposons une session **Questions/Réponses**. 

**N’hésitez surtout pas à envoyer vos questions [à cette adresse](mailto:pierre-antoine.bouttier@univ-grenoble-alpes.fr) ou à les rajouter [dans ce pad](https://mypads.framapad.org/mypads/?/mypads/group/misc-ycrzy75p/pad/view/cafe-guix-11-02-2022-6ds0y79l)** afin que nous préparions au mieux la session et que nous y répondions le plus précisément possible. **Ne vous modérez pas**, il n’y pas de questions stupides. Le périmètre peut être très large : *question générale sur les gestionnaires d’environnement logiciel, comment fait-on ceci, j’ai un problème avec ce paquet précis, comment Guix interagit avec les variables d’environnement, etc.* Si l’afflux de questions est trop important, un deuxième café de ce type sera programmé. 

Le prochain Café Guix aura lieu le **11 février, de 13h à 14h** [à cette adresse.](https://meet.univ-grenoble-alpes.fr/b/pie-kls-hpu-opc)

### Première session de l’année (7 janvier 2022)

Nous parlerons cette fois ci de création d'environnements de
développement.  Nous verrons comment utiliser [`guix
shell`](https://guix.gnu.org/manual/devel/fr/html_node/Invoquer-guix-shell.html),
la nouvelle commande qui [remplace `guix
environment`](https://guix.gnu.org/fr/blog/2021/from-guix-environment-to-guix-shell/)
(laquelle est toujours disponible mais désuète), pour obtenir des
environnements à la manière de `module load` ou de VirtualEnv.  Si vous
n’avez pas encore franchi le pas ou si vous venez de commencer avec
Guix, cette session est pour vous !

Ce prochain Café Guix aura lieu **vendredi 7 janvier 2022, de 13h à 14h** à cette
[instance
BigBlueButton](https://meet.univ-grenoble-alpes.fr/b/pie-kls-hpu-opc) — entrez
votre nom et rejoignez-nous.

## Continuer la discussion

[Rejoignez le groupe Mattermost
dédié](https://mattermost.univ-nantes.fr/signup_user_complete/?id=njdxbdazafddtq6wsm6cgrr95r)
pour prolonger la discussion entre deux sessions !

## [Sessions précédentes…](/events/2021/café-guix/)

## Organisation

 - Céline Acary-Robert (Université Grenoble-Alpes)
 - Pierre-Antoine Bouttier (Université Grenoble-Alpes)
 - Ludovic Courtès (Inria)
 - Yann Dupont (Université Nantes)
 - Jean-François Guillaume (Université Nantes)
 - Simon Tournier (Université Paris)
