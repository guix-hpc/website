;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2022, 2023 Inria

;; To build the name tags, run:
;;
;;   guix build -f build.scm
;;
;; The result will be a directory containing one PDF file.  This assumes that
;; a file named 'registered.csv', obtained from sciencesconf.org, is
;; available in the current directory.

(use-modules (guix) (gnu)
             (srfi srfi-1)
             (ice-9 rdelim)
             (ice-9 match))


(define ghostscript (specification->package "ghostscript"))
(define lout (specification->package "lout"))
(define font-fira-sans (specification->package "font-fira-sans"))
(define coreutils (specification->package "coreutils"))
(define ttf2pt1 (specification->package "ttf2pt1"))

(define (truetype->type1 fonts)
  "Return a directory containing Type 1 .afm and .pfa files for FONTS."
  (define build
    (with-imported-modules '((guix build utils))
      #~(begin
          (use-modules (guix build utils)
                       (srfi srfi-1))

          (define ttf-files
            (append-map (lambda (font)
                          (find-files font "\\.ttf$"))
                        '#$fonts))

          (define directory
            (string-append #$output "/share/fonts/type1"))

          (mkdir-p directory)
          (for-each (lambda (ttf)
                      (let ((base (string-append directory "/"
                                                 (basename ttf ".ttf") )))
                        (invoke #$(file-append ttf2pt1 "/bin/ttf2pt1")
                                "-e" ttf base)))
                    ttf-files))))

  (computed-file "type1-fonts" build))

(define* (lout->pdf directory file #:key
                    (paper-size "A4")
                    (fonts (list font-fira-sans)))
  "Build Lout source FILE, taken from DIRECTORY, and return the resulting
PDF."
  (define font-directory
    (truetype->type1 fonts))

  (define build
    (with-imported-modules '((guix build utils))
      #~(begin
          (use-modules (guix build utils))

          (define ps-file
            (string-append #$output "/"
                           #$(basename file ".lout") ".ps"))

          (define pdf-file
            (string-append #$output "/"
                           #$(basename file ".lout") ".pdf"))

          (mkdir #$output)
          (copy-recursively #$directory ".")
          (invoke #$(file-append lout "/bin/lout") "-a" "-r3"
                  "-I."
                  "-F" #$(file-append font-directory "/share/fonts/type1")
                  "-s" #$file "-o" ps-file)

          (setenv "PATH" (string-join '(#$ghostscript #$coreutils)
                                      "/bin:" 'suffix))
          (setenv "GS_FONTPATH"
                  (string-append #$font-directory "/share/fonts/type1"))
          (invoke #$(file-append ghostscript "/bin/ps2pdf")
                  "-dPDFSETTINGS=/prepress"
                  #$(string-append "-sPAPERSIZE="
                                   (string-downcase paper-size))
                  ps-file pdf-file))))

  (computed-file (basename file ".lout") build))

(define %organizers
  ;; Very Important People.
  '("Acary-Robert"
    "Bouttier"
    "Courtès"
    "Dehne Garcia"
    "Tournier"))

(define (name-tag-page name-file)
  "Return the Lout source of a name tag page"
  (define build
    (with-imported-modules '((guix build utils))
      #~(begin
          (use-modules (guix build utils)
                       (srfi srfi-1)
                       (ice-9 match))

          (define (organizer? name)
            (find (lambda (organizer)
                    (string-contains-ci name organizer))
                  '#$%organizers))

          (define (string->lout str)
            (list->string
             (string-fold-right (lambda (chr lst)
                                  (match chr
                                    (#\“ (cons* #\` #\` lst))
                                    (#\” (cons* #\' #\' lst))
                                    (#\  (cons #\~ lst))
                                    (#\’ (cons #\' lst))
                                    (#\& (cons* #\" #\& #\" lst))
                                    (#\/ (cons* #\" #\/ #\" lst))
                                    (#\š (append
                                          (string->list
                                           "{ { { Times Base } @Font @Char \"caron\" } |0.5ro s }")
                                          lst))
                                    (#\č (append
                                          (string->list
                                           "{ { { Times Base } @Font @Char \"caron\" } |0.5ro c }")
                                          lst))
                                    (#\Ż (append  ;😱
                                          (string->list
                                           "{ @Sup{ { Times Base } @Font @Char \"dotaccent\" } |0.5ro Z }")
                                          lst))
                                    (chr (cons chr lst))))
                                '()
                                str)))

          (define names+affiliations
            (with-fluids ((%default-port-encoding "UTF-8"))
              (call-with-input-file #$name-file read)))

          (mkdir #$output)
          (for-each (lambda (file)
                      (copy-file file
                                 (string-append
                                  #$output "/"
                                  (strip-store-file-name file))))
                    '(#$(local-file "../../../doc/fira-fonts.ld")
                      #$(local-file "doc-style.lout")
                      #$(local-file "workshop-logo.eps")))

          ;; Lout only understands Latin-1...
          (with-fluids ((%default-port-encoding "ISO-8859-1"))
            (call-with-output-file (string-append #$output "/name-tag-page.lout")
              (lambda (port)
                (display "
@Include { doc-style.lout }
@Database @FontDef { fira-fonts }

def @Logo {
  2.4c @Wide @Scale @IncludeGraphic { \"workshop-logo.eps\" }
}

def @OrganizerTag {
  red @Color @Box paint { red } { white @Color 0.7f @Font { organizer } }
}

def @NameTag named @Affiliation {} named @Organizer { No } right name {
  8.7c @Wide 5.4c @High @Box @HExpand @VExpand {
    //0ct
    { @Organizer @Case {
         \"Yes\" @Yield { @Logo @Right @OrganizerTag }
         else @Yield @Logo
      }
    }
    //2.5ct
    { @Center 1.1fx @Break { name }
      //1.1cx
      @Center { gray @Color 0.6f @Font { @Affiliation } } }
  }
}

@Document
   @InitialFont { FiraSans Bold 20p }
   @InitialBreak { cragged nohyphen 1.3fx }
//

@Text @Begin
0c @Break { "
                         port)
                (for-each (match-lambda
                            ((name . affiliation)
                             (display (string-append
                                       "@NameTag @Affiliation { "
                                       (string->lout affiliation)
                                       "} "
                                       " @Organizer { "
                                       (if (organizer? name)
                                           "Yes"
                                           "No")
                                       " }"
                                       " { "
                                       (string->lout (string-capitalize name))
                                       " } &0ce \n")
                                      port)))
                          names+affiliations)

                ;; Add a bunch of empty tags.
                (display
                 (string-concatenate
                  (make-list 20 "@NameTag {} &0ce \n"))
                 port)

                (display " }\n@End @Text\n" port)))))))

  (computed-file "name-tag-page" build))

(define (sciencesconf-csv->name-file csv-file)
  (define build
    #~(begin
        (use-modules (ice-9 rdelim)
                     (ice-9 match)
                     (ice-9 pretty-print)
                     (srfi srfi-1))

        (define (string->object str)
          (call-with-input-string str read))

        (define (input-line->names line)
          (match line
            ((uid casid user-name last-name first-name email
                  validity civility middle-name url language-id phone date
                  billing-address billing-zip-code billing-city
                  billing-country arrival-date departure-date
                  wednesday-lunch? thursday-lunch?
                  thursday-dinner? friday-lunch?
                  thursday-tutorial-session
                  friday-tutorial-session
                  affiliation affiliation-address
                  affiliation-country
                  laboratory status)
             (cons (string-append (string->object first-name)
                                  " " (string->object last-name))
                   (string->object affiliation)))
            ((or #f ())
             #f)))

        (define not-semicolon
          (char-set-complement (char-set #\;)))

        (setvbuf (current-output-port) 'line)
        (setvbuf (current-error-port) 'line)
        (fluid-set! %default-port-encoding "UTF-8")

        (let ((lines
               (call-with-input-file #$csv-file
                 (lambda (port)
                   (let loop ((lines '()))
                     (match (read-delimited "\r\n" port)
                       ((? eof-object?)
                        (cdr (reverse lines)))
                       (#f
                        (loop lines))
                       (line
                        (match (input-line->names
                                (string-tokenize line not-semicolon))
                          (#f (loop lines))
                          (name (loop (cons name lines)))))))))))
          (call-with-output-file #$output
            (lambda (port)
              (pretty-print (sort lines
                                  (match-lambda*
                                    (((name1 . _) (name2 . _))
                                     (string<? name1 name2))))
                            port))))))

  (computed-file "participant-names.scm" build))

(let ((names (sciencesconf-csv->name-file (local-file "registered.csv"))))
  (lout->pdf (name-tag-page names)
             "name-tag-page.lout"))
