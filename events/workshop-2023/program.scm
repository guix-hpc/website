;;; This module is part of the Ten Years of Guix web site and is licensed
;;; under the same terms, those of the GNU GPL version 3 or (at your option)
;;; any later version.
;;;
;;; Copyright © 2022-2024 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2023 Simon Tournier <simon.tournier@inserm.fr>

(define-module (events workshop-2023 program)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 match)
  #:use-module (events program)
  #:export (wednesday-talk?
            thursday-talk?
            friday-talk?

            talks))

(define talks
  (sort
   (list (talk "Caring for your environment(s)"
               (list (speaker "Konrad Hinsen"
                              "Centre de Biophysique Moléculaire, CNRS; Synchrotron SOLEIL; France"
                              #:biography
                              "Biophysicist at CNRS.  Scientific computing since 1988, irreproducibility
victim since 1995, tendency: ever increasing.  Hoping that we will reach peak
irreproducibility soon.  Counting on Guix."))
               "Programmers mentally divide code into three layers: their own code, the
libraries and tools they interact with, and the environment.  Unfortunately,
the environment is the layer they care about least.  My mission is to
convince you that the environment is interesting and worth caring about.  Did
you know that the environment metaphor is very inaccurate? It's really the
foundation supporting your code.  And then, there are additional environments
you should be aware of: the social environments of developers and users,
society at large, the physical environment of our computing systems.  Are you
ready to become an environmentalist?"
               45
               (date "2023-11-08 13:30")
               "konrad.pdf"
               #:video "01-hinsen.webm")

         (talk "Good practices for reproducibility"
               (list (speaker "Miguel Colom-Barco"
                              "Centre Borelli, ENS Paris-Saclay, France"
                              #:biography
                              '((a (@ (href
                                       "http://mcolom.perso.math.cnrs.fr/"))
                                   "Miguel Colom")
                                " is a senior researcher (PhD, HDR) at Centre Borelli, ENS
Paris-Saclay.  He works on open science, image processing, computer vision,
and general AI.  He's one of the founding editor of the Image Processing On
Line (IPOL) journal and he designed the current demo system along with a team
of motivated engineers.  He's a member of the Task Force on Infrastructures
for Quality Research Software of EOSC, of the IPOL Editorial Board, and the
Board Team of TC18 IAPR Discrete Geometry and Mathematical Morphology on
reproducibility matters.  He teaches two courses at the “reproducibility
track” of the M2 MVA master at ENS PS.")))
               '((p "The lack of reproducibility of much scientific research pointed out in 2009
by D. Donoho and many other researchers has consequences not only on the
credibility of the results, but also it has a very direct and negative impact
in aspects such as public health, safety, or security. There's a consensus on
the benefits of reproducibility, but only a minority of researchers are fully
committed. We'll review some of the reasons why, and think about how
reproducible research could be encouraged and rewarded.")
                 (p "\
From a practical point of view, we'll discuss good practices both to write and
review reproducible scientific articles. To this purpose we'll review good
practices related to the execution environments of the software, the
versioning of the code, the FAIR principles for data, formats, standards, and
the quality of the source code itself"))
               45
               (date "2023-11-08 14:15")
               "miguel.pdf"
               #:video "02-colom-barco.webm")

         (talk "Reproducible computational environments with Binder"
               (list (speaker "Sarah Gibson"
                              "2i2c, United Kingdom"
                              #:biography
                              "Sarah Gibson is an Open Source Infrastructure Engineer at 2i2c, an open
source contributor, and advocate for open science.  She holds more than two
years of experience as a Research Engineer at a national institute for data
science and artificial intelligence, as well as holding a core contributor
role in the open source projects JupyterHub, Binder, and The Turing Way.
Sarah is passionate about working with domain experts to leverage cloud
computing in order to accelerate cutting-edge, data-intensive research and
disseminating the results in an open, reproducible and reusable manner.  Sarah
holds a Fellowship with the Software Sustainability Institute and advocates
for best software practices in research.  She is a member of the mybinder.org
operating team and maintains infrastructure supporting a global community in
sharing reproducible computational environments.  She has also mentored
projects through cohorts of the OLS and Outreachy programmes, imparting lived
experience of her skills participating and leading in open science projects."))
               "Reproducible research is necessary to ensure that scientific work can be
trusted.  Funders and publishers are beginning to require that publications
include access to the underlying data and the analysis code.  The goal is to
ensure that all results can be independently verified and built upon in future
work.  This is sometimes easier said than done!  Sharing these research
outputs means understanding data management, library sciences, software
development, and continuous integration techniques: skills that are not widely
taught or expected of academic researchers.  A particularly steep barrier to
working with codebases is setting up computational environments, and getting
the combination of package versions just right can influence the
reproducibility of code: from outright failures, to subtle changes in
generated outputs.  There are many tools available to manage your
computational environment; but in this talk, we’ll explore Project Binder and
its subproject repo2docker, which aims to automate reproducibility best
practices across a number of ecosystems.  Binder can build portable
computational environments, when requested, with all the information encoded
in a single, clickable URL, which greases the wheels of collaborative research
while reducing the toil involved.  We will discuss how these concepts can
apply to the HPC community."
               45
               (date "2023-11-08 15:00")
               "sarah.pdf"
               #:video "03-gibson.webm")

         (talk "What is Guix?"
               (list (speaker "Josselin Poiret"
                              "Équipe Gallinette ; Nantes Université,
Inria, France"
                              #:biography
                              "Mathematics and computer science researcher by day, I've always loved
tinkering with systems and found Guix to be both a wonderful tool and also
project to work on!  I'm interested in general in proof assistants, typed
languages, correct-by-construction code and strong guarantees, but also
synthetic mathematics and higher category theory."))
               '((p "Navigating the jungle of reproducible environments can be pretty tough,
as there is a myriad of problems to consider: does this
language-specific package manager build packages reproducibly?  How does
it integrate its external dependencies?  Is the compiler bootstrapped?
Will all the metadata it's using disappear in X years?  Will my
distributed artifacts work without external assumptions?  Do I have tools
and guarantees to examine all of this?")
                 (p "\
With these interrogations in mind, I will give an overview of Guix, the
swiss army knife of environments, and describe how it can help you
achieve reproducibility.  I will also be comparing it with some other
practices I've seen and try to dismiss some misconceptions about them,
highlighting Guix's strengths."))
               45
               (date "2023-11-08 16:15")
               "josselin.pdf"
               #:video "04-poiret.webm")

         (talk "Everyone can learn how to Guix"
               (list (speaker "Nicolas Vallet"
                              "University Hospital of Tours, Hematology and
Cell Therapy department, Inserm U1069, LNOx group, France"
                              #:biography
                              "Nicolas Vallet obtained a MD in clinical
Hematology at the university of Tours, France in 2019.  He next joined
Institut de recherche Saint Louis in Paris for a PhD, from 2019 to 2022,
where he studied mechanisms of relapse after allogeneic hematopoietic stem
cell transplant.  His worked relied on mass cytometry and single cell RNAseq
experiments.  He is now assistant professor at the university hospital of
Tours in the hematology and cell therapy department."))
               "In the biomedical environment, reproducibility is mainly taught in the
setting of bench experiments. Data analyses description usually focus on basic
measures such as mentioning the software version used, sharing the raw data,
and occasionally providing a partial script. This presentation aims to
showcase how we went from version labels to Guix and how it shaped our
workflows to analyze various types of data, encompassing omics and targeted
measurements.  We will provide insights into how we effectively reported its
utilization in our published manuscripts."
               45
               (date "2023-11-08 17:00")
               "nicolas.pdf"
               #:video "05-vallet.webm")


         (talk "Software Heritage: Archive your source code for a consistent and durable referencing"
               (list (speaker "Benoît Chauvet"
                              "Software Heritage, France"
                              #:biography
                              "Benoit Chauvet is the engineering manager of the Software Heritage
technical team.  He has an IT engineering and training/consulting background.
His role is to coordinate the team planning and organize the Software
Heritage annual roadmap.  He is interested in free and Open Source software,
and eager to get the best from the combination of the research environment
and engineering best practices."))
               "Software Heritage, the world largest source code archive, is designed to
collect, preserve and share source code for the long term.  After an overview
of the Software Heritage mission and infrastructure, we'll discover how
Software Heritage created a unique and powerful system of source code
referencing, using the SWHID standard.  Then a demo/tutorial will show you how
to archive and reference your own source code using Software Heritage tools
and infrastructure."
               60
               (date "2023-11-09 09:00")
	       "benoit.pdf"
               #:video "06-chauvet.webm")

         (talk "How to get started with Gitlab, an essential tool for research reproducibility?"
               (list (speaker "Alizia Tarayoun"
                              "Université Grenoble-Alpes, France"
                              #:biography
                              "Alizia Tarayoun is a research engineer at University Grenoble Alpes (France)
since 2019.  In 2018 she obtained a PhD degree in Geosciences from University
Montpellier (France).  She is now a member of the SEISCOPE consortium which is
focused on the high resolution seismic imaging by full waveform inversion. Her
research interests focus especially on high-performance computing, system
architectures for Exascale computing and full waveform inversion.  Her role is
to manage the computation codes developed where she notably switched from the
version control system Subversion to Git and the gricad-gitlab forge.  She has
used the later daily since then and has been training PhD students in its use
for several years."))
               "This tutorial, intended for a novice audience, aims you to get started with
the gitlab forge and a versioning system (git).  We will see what is a forge
and why it is today an essential tool for research reproducibility.  At the
end of this tutorial you will know how to use the main tools offered by the
forge as well as the basic git commands to manage one or several project(s).
Small demonstrations will be carried out from a simple one to illustrate the
git commands to a more advanced example working with several branches on a
code development project."
               75
               (date "2023-11-09 10:15")
	       "alizia.pdf"
               #:type 'tutorial
               #:note "session 1: amphitheater")

         (talk "Reproducible Jupyter Notebook and BinderHub"
               (list (speaker "Pierre-Antoine Bouttier"
                              "GRICAD, France"
                              #:biography
                              '("Trained as an applied mathematician, I'm now working at "
                                (a (@ (href
                                       "https://gricad.univ-grenoble-alpes.fr/"))
                                   "GRICAD") ", trying to provide Grenoble's
research communities with the most relevant infrastructures and services
possible to meet the needs of scientific computing and research data
management, with a strong focus on reproducibility. ")))
               '((p "Jupyter notebooks are excellent pedagogical and methodological tools for
explaining concepts and reasoning involving the processing of digital data,
making it possible to include formatted text, multimedia elements and software
code in a single interface.")
                 (p "\
However, setting up a notebook execution environment can sometimes be tedious
and time-consuming. In this tutorial, we'll look at how notebooks can be put
to good use and how a GitLab project combined with technologies such as
JupyterLite or BinderHub can greatly simplify the provision of Jupyter
notebooks."))
               75
               (date "2023-11-09 11:30")
	       "pierre-antoine.pdf"
               #:type 'tutorial
               #:note "session 1: amphitheater")

         (talk "Advanced Gitlab – How to setup Continuous Integration in your Gitlab projects, another step towards software reproducibility"
               (list (speaker "Franck Pérignon"
                              "Jean Kuntzmann Laboratory, CNRS, France"
                              #:biography
                              '("Franck Pérignon is a CNRS research engineer in scientific computing, at the
Jean Kuntzmann Laboratory in Grenoble.  Working for a long time with
modeling and simulation tools, he has an extensive experience in software
engineering.  Moreover, since 2017, he is in charge of the GitLab "
                                (a (@ (href
                                       "https://gricad-gitlab.univ-grenoble-alpes.fr"))
                                   "platform for the research and teaching community")
                                " in Grenoble.")))
               "In software engineering, Continuous Integration (CI) is a practice which
consists in systematically checking the impact of any source code modification
on operation, performance, etc. via an automatic execution chain.  Combined
with Docker or equivalent tools, Gitlab offers very practical and powerful
procedures and tools to implement continuous integration in your projects.  In
this tutorial, we will describe the CI setup in a software project and show
how it someway helps to ensure software reproducibility."
               75
               (date "2023-11-09 10:15")
	       "franck.pdf"
               #:type 'tutorial
               #:note "session 2: classroom")

         (talk "How to use Org mode and Guix to build a reproducible experimental study"
               (list (speaker "Marek Felšöci"
                              "Inria, France"
                              #:biography
                              "Marek is a post-doc research fellow in computer science at Inria (French
National Institute for Research in Computer science and Automation).
Everyday user and occasional contributor to GNU Guix since the beginning of
his PhD, he has dedicated a substantial part of his thesis to the
reproducibility in computer science and especially in the domain of HPC.
Whereas the main subject of his thesis was the design of fast solvers for
coupled sparse and dense linear systems arising from aeroacoustic numerical
modelizations, his current research interest resides in automatic
parallelization of C/C++ source code through task insertion."))
               '((p "In computer science in general and in HPC in particular, reproducibility of a
research study has always been a complex matter. On the one hand, rebuilding
exactly the same software environment on various computing platforms and over
extended periods of time may be long, tedious and sometimes virtually impossible
to be done manually. On the other hand, while the experimental method is usually
explained in research studies, the instructions required to reproduce the latter
from A to Z should also be provided in a comprehensive manner.")
                 (p "\
In this tutorial, following a brief presentation of the context and motivations,
we will introduce the principles of literate programming with "
                    (a (@ (href "https://orgmode.org")) "Org mode")
                    " and learn
how we can take advantage of it in the association with Guix to build a
reproducible experimental study. Assuming the knowledge of some basics of Guix
(searching for and installing packages, spawning simple environments with the
" (tt "guix shell") " command), we will use it during the hands-on session to manage the
software environment of an example study thanks to the " (tt "guix time-machine") "
command and a more advanced usage of the " (tt "guix shell") " command including
manifests and package transformations. Then, we will rely on the literate
programming paradigm and use Org mode to not only write the study itself, but
also to describe all the elements and instructions allowing for reproducing it.
This includes the experiments, source code and procedures involved in the
construction of experimental software environments, execution of benchmarks,
gathering and post-processing of results and production of the final
publication(s).")
                 (p "\
For the hands-on session, the participants will need to bring a personal
computer on which they have installed GNU Guix 1.4.0 beforehand (see "
                    (a (@ (href
                           "https://guix.gnu.org/manual/en/html_node/Binary-Installation.html"))
                       "instructions")
                    ").  To store the
software environment and the experimental study, the participants should have
around 20 GiB of free space on the " (tt "/") " (root) partition."))
               75
               (date "2023-11-09 11:30")
               "https://gitlab.inria.fr/tutorial-guix-hpc-workshop/"
               #:type 'tutorial
               #:note "session 2: classroom")

         (talk "A decade of Guix — a subjective retrospective "
               (list (speaker "Ricardo Wurmus"
                              "Max Delbrück Center, Germany"
                              #:biography
                              '("Ricardo is a free software hacker and GNU/Linux system administrator.
  Working at the Scientific Bioinformatics Platform at the Max Delbrueck
Center for Molecular Medicine in Berlin he packages and manages scientific
software on high performance computing systems and supports the development of
bioinformatics tools and platforms.  As a long term contributor and former
co-maintainer of GNU Guix he has extensive experience in building software
reproducibly and making stubborn applications work.")))
               '((p "Almost a decade ago a Berlin research institute looked for help in compiling
scientific software for bioinformatics researchers.  A systems engineer from
Shanghai with a yearning for a simpler life answered the call.  Little did he
know that his long repressed infatuation with a quaint programming language
would soon resurface, sparking his fantastically deterministic journey away
from the traditions of the sysadmin tribe and just beyond the brink of the
cutting edge.")
                 (p "\
This is the story of a quest for predictability, reproducibility, and
stability through unpredictable means, ad-hoc hacks, and an embrace of the
quirky.  Based on actual events.")
                 (p "\
Keywords: research and HPC; Guix project infrastructure; round PiGx and square
holes; Guix Workflow Language; the unreasonable allure of simple
abstractions."))
               45
               (date "2023-11-09 14:00")
               "ricardo.pdf"
               #:video "07-wurmus.webm")

         (talk "Why we deploy and develop using Guix and why you should too — an experience
report from GeneNetwork"
               (list (speaker "Arun Isaac"
                              "Department of Genetics, Evolution & \
Environment, University College London, United Kingdom"
                              #:biography
                              "Arun Isaac is a research fellow at the Department of Genetics, Evolution &
Environment in University College London. He is an avid and experienced
lisper/schemer.  He loves writing domain specific languages and exploring new
computing paradigms.  He contributes regularly to the GNU Guix functional
package manager, and helps run and maintain genenetwork.org."))
               '((p "Scientific software is increasingly complex, but is developed on a shoestring
budget. Maintaining a reproducible development environment for all developers
and running robust deployments is challenging, to say the least.  Wouldn't it
be nice to have a tool that does it all and does so correctly?")
                 (p "\
The traditional Unix way to deploy complex web applications and provision
servers is to manually mutate configuration files on the server. Such an
approach is brittle, time consuming and hard to migrate to new machines.
Tools as varied as Ansible and Docker have been developed to ease this
process, but these tools are still mutation based and their abstractions leak
in unexpected ways. Guix, with its \"functional\" package deployment provides
the watertight abstractions necessary to express complex deployments with
precision.")
                 (p "\
In this talk, I will present how we deploy development and production
environments using Guix at genenetwork.org.  I will show how we use Guix
channels to distribute our own packages and services; and how we run
continuous integration and deployment (CI/CD) using Guix. I will explain how
this enables us to further software quality in science, and will hopefully be
able to convince you to use more Guix in your team."))
               45
               (date "2023-11-09 14:45")
               "arun.pdf"
               #:video "08-arun.webm")

         (talk "PsychNotebook is intended as…"
               (list (speaker "Lars-Dominik Braun"
                              #f
                              #:biography
                              "Educated as a computer scientist, employed as an engineer, but in reality I
am just an artist, easily amazed by every aspect of software and computers.
In my three and a half years at Leibniz Institute for Psychology I built a
platform based on GNU Guix, which sought to improve access to reproducible
research for scientists and students without formal computer science
education.  Recently I returned to embedded software development."))
               '((p "PsychNotebook was a web platform for students and scientists providing access
to shareable and reproducible programming environments including RStudio and
JupyterLab. It was developed and operated by Leibniz Institute for Psychology
between 2019 and 2023.")
                 (p "\
In this talk I will review why PsychNotebook was built, which components we
used and built ourselves, why we chose them and how they interacted with each
other as well as how the platform was kept running. I will also discuss why,
ultimately, the service was shut down and what can be learned from its
technical and organizational design."))
               45
               (date "2023-11-09 15:30")
	       "lars.pdf"
               #:video "09-braun.webm")

         (talk "Reproducible virtual machine management with Guix"
               (list (speaker "Yann Dupont"
                              "GLiCID, France"
                              #:biography
                              "Yann Dupont is currently working on the architectural, system and network
aspects of GliCID, the HPC center for education and research in the Pays de
Loire region of France.  Before entering the HPC world in 2016, he was in
charge of the infrastructure department at the University of Nantes, where he
was involved in Linux, networking, storage and even programming."))
               '((p "GLiCID is the HPC center for research in the French region
of Pays de la Loire, merging the various pre-existing HPC centers
in the region.")
                 (p "The installation of new machines in June 2023 has led to the launch
of a brand new common system infrastructure (identity management,
slurm services, databases, etc.), independent (as far as possible)
of the solutions provided by the manufacturers.")
                 (p "Installed on 2 remotes machine rooms, the infrastructure has to be
highly available, implying complex deployment. However, the team
wanted to guarantee simple, predictable redeployment of the
infrastructure in the event of problems.")
                 (p "Guix, offered as standard on all our clusters, has a proven track
record of reproducibility, which is also a desirable feature for our
infrastructure. We have therefore tried to build it with Guix.  We'll be
reporting on the impact of these choices (both positive and negative), and
why a 100% rate has not yet been achieved."))
               45
               (date "2023-11-09 16:30")
               "yann.pdf"
               #:video "10-dupont.webm")

         (talk "Reconciling high-performance computing with the use of third-party libraries?"
               (list (speaker "Emmanuel Agullo"
                              "Inria, France"
                              #:biography
                              '("Inria researcher in the "
                                (a (@ (href
                                       "https://www.inria.fr/fr/concace"))
                                   "concace")
                                " team in Bordeaux.  Seeking to combine high-level expression of numerical
algorithms (especially numerical linear algebra) and high-performance on
supercomputers.  Trying to ensure a robust deployment of the resulting software
stack with " (a (@ (href
                    "https://gitlab.inria.fr/guix-hpc/guix-hpc"))
                "guix-hpc") " channel.")))
               '((p "High-performance computing (HPC) often requires the use of multiple software
packages and that they are optimised on the target machine on which these
computations are running. The optimisation constraints are such that it is
widely accepted that the deployment of this software can only be done manually
or by relying on the work of the administrators of the target
machine (typically via a load module). However, the complexity of the
dependencies often results in strong constraints on the exact functionality,
version and build processes of the requested software. As a result, many HPC
codes choose to provide some functionality themselves, which in principle
could be provided by third party libraries, contrary to the canons of software
engineering.")
                 (p "\
In this talk, we will first review our quest (CMake, Spack, and now Guix) for
an environment to reliably deploy HPC software in a portable,
high-performance, and reproducible way, so that the use of third-party
libraries is no longer a concern. We will then present our experience of
deploying such a complex software stack on several machines in the French and
European ecosystem. We show that we have been able to ensure a robust
deployment while achieving top performance, not only on machines with Guix
available but also on supercomputers where Guix is not (yet!) available."))
               45
               (date "2023-11-09 17:15")
               "emmanuel.pdf"
               #:video "11-agullo.webm")

         (talk "How to get started using Guix"
               (list (speaker "Ludovic Courtès"
                              "Inria, France"
                              #:biography
                              '("Research software engineer at Inria in Bordeaux, France, I have been working
with kind yet demanding HPC practitioners who want it all—performance!
flexibility! reproducibility!  I founded Guix in 2012, soon joined by a team
of intrepid developers, and Guix has proved to be a great tool to try and
satisfy those HPC needs.  I am member of the Software chapter of the
French " (i "Comité pour la science ouverte") " (Open Science Committee).")))
               '((p "Curious about Guix but haven’t yet had a chance to give
it a try?  Coming from " (tt "apt")", Spack, CONDA, pip?
Wondering whether it meets your specific needs?")
                 (p "This tutorial aims to get you started with Guix.  We will start with the
main commands to manage software with Guix, including on-demand environments
with " (tt "guix shell")".  We will discuss the technicalities and gotchas
one needs to be aware of—from environment variables to pre-built binaries to
containerization.  We will introduce " (it "channels")", how they let you extend the package
collection of Guix, and how they let you pin your complete software
environment so you can reproduce it elsewhere and at different points in time."))
               90
               (date "2023-11-10 09:00")
               "https://gitlab.inria.fr/guix-hpc/workshop-2023/-/blob/main/getting-started/hands-on.org"
               #:type 'tutorial
               #:note "session 3: amphitheater")

         (talk "How to get started writing Guix packages"
               (list (speaker "Andreas Enge"
                              "Inria, France"
                              #:biography
                              '("Andreas Enge is a researcher at Inria Bordeaux, working on computational
number theory and its applications to cryptology.  He is interested in
combinations of algebraic and analytic methods, or symbolic-numerical
computations guided by complexity theory.  This led him to be one of the main
authors of GNU MPC for arbitrary precision complex floating point arithmetic
with correct rounding.  Andreas programmes mainly in C, which he tweaks to
look like Pascal.")))
               '((p "Guix is based on a full-blown programming language, Scheme.  But after
overcoming our first cultural shock of meeting parentheses in all the wrong
places, we will see that defining a first package is actually a piece of cake
by just using one of the many existing package importers.  It turns out that
simple packages are declared by a simple text file in an intuitive format.
Mechanisms in Guix make it possible to enrich the set of existing packages
with our own creations.  Finally we will see how to give back to the Guix
community by submitting our recipe for inclusion into the official git
repository.")
                 (p "The aim of this workshop is to let you go home with your own œuvre, so please
bring an idea for a (simple) piece of software to package.  Depending on time
and the problems we encounter, we may discuss more advanced strategies of
packaging that require a bit of Scheme code"))
               90
               (date "2023-11-10 10:45")
               "https://gitlab.inria.fr/guix-hpc/workshop-2023/-/tree/main/packaging"
               #:type 'tutorial
               #:note "session 3: amphitheater")

         (talk "How to install and manage Guix on a cluster"
               (list (speaker "Bruno Bzeznik"
                              "GRICAD, France"
                              #:biography
                              "I'm a senior Unix system administrator, used to distributed systems just
before the 2000s and specialized into High Performance Computing and Data
Analysis infrastructures since 2006. I'm currently in charge of the HPC
clusters and storage facilities of the GRICAD computing center in Grenoble."))
               "Guix is a useful environment to empower the users of an HPC cluster with the
applications installations and customization. We'll see how to set-up Guix as
a software environment for an HPC cluster and important things to know about
the administration of this service."
               90
               (date "2023-11-10 10:45")
	       "bruno.pdf"
               #:type 'tutorial
               #:note "session 4: classroom")

         (talk "How to make advanced packages"
               (list (speaker "Simon Tournier"
                              "Université Paris-Cité, Institut de Recherche
Saint Louis, Inserm US53, CNRS 2030, France"
                              #:biography
                              '("I am Research Engineer at the Université Paris Cité; member of the core
facilities team Inserm US53, CNRS 2030; team which supports biologists from
generating data as genomic, flow cytometry or microscopy imaging to helping
them with numerical processing.  Unable, from laptop to HPC cluster, to deal
with the whole complexity of all the software involved
in " (i "bioinformatics") " using classical package managers – especially when
trying to reproduce the computational environment between computers or over
the time – so I have been interested in Guix. Now, I am daily working with it!")))
               '((p "This tutorial is dedicated to review what can be
done when that’s not enough to list dependencies and/or declare a build system
.  The aim is to introduce various mechanisms for adapting the base Guix
recipe.  The prerequisite is the reading of the section "
                    (a (@ (href
                           "https://guix.gnu.org/manual/devel/en/guix.html#Defining-Packages"))
                       (i "“Defining Packages”"))
                    " from the manual and the goal of this tutorial is to provide some
ingredients for making it sound.  We propose to first introduce a Scheme/Guile
Swiss-knife toolbox, then to cover how to modify upstream source
code (field " (tt "origin") ") and how to customize the build system
parameters or phases
(field " (tt "arguments") ").  If time allows, we will introduce the meaning of
cryptic symbols as the sequence " (tt "#~(#$(%") ".")
                 (p "\
Do not forget that packaging is a craft, so there is no magic but only
practise."))
               90
               (date "2023-11-10 09:00")
               "https://archive.softwareheritage.org/swh:1:dir:179f060aabd198e758b52058b417fee1a7b8962c;origin=https://gitlab.com/zimoun/advanced-packages-2023;visit=swh:1:snp:909e690430f6d869f78c6b56abc0e8cbb895ea8d;anchor=swh:1:rev:e2b1a61486e8b7d75200231cf645b5442373f048"
               #:type 'tutorial
               #:note "session 4: classroom"))
   talk-earlier?))


;;;
;;; Helper procedures.
;;;

(define (talk-day-predicate day)
  (lambda (talk)
    (let ((date (talk-date talk)))
      (and (= (date-day date) day)
           (= (date-month date) 11)
           (= (date-year date) 2023)))))

(define wednesday-talk? (talk-day-predicate 08))
(define thursday-talk? (talk-day-predicate 09))
(define friday-talk? (talk-day-predicate 10))
