title: Guix-HPC events
author: Ludovic Courtès
date: 2021-04-09 14:00
tags: community
---

# Guix-HPC Events

This page lists past and upcoming events organized by the [Guix-HPC
collective](/about).

## Upcoming Events

  - [☕ Café Guix](/events/2024-2025/café-guix) (_monthly French-speaking
    hacking sessions for scientists and HPC practitioners_)

## Past Events

  - [Mini Workshop on Guix in High-Performance
    Computing](/events/2024/workshop), 7 November 2024
  - [☕ Café Guix in 2023/2024](/events/2024/café-guix) (_monthly French-speaking
    hacking sessions for scientists and HPC practitioners_)
  - [First Workshop on Reproducible Software Environments for Research
    and High-Performance Computing](/events/2023/workshop), 8–10
    November 2023
  - [☕ Café Guix in 2022/2023](/events/2022/café-guix) (_monthly French-speaking
    hacking sessions for scientists and HPC practitioners_)	
  - [Second reproducible research
    hackathon](/blog/2023/05/reproducible-research-hackathon-let-redo),
    27 June 2023 (on-line)
  - [🎂 Ten Years of Guix](https://10years.guix.gnu.org), 16–18
    September 2022 (Paris, France); [videos
    available](https://10years.guix.gnu.org/program)!
  - [☕ Café Guix in 2021](/events/2021/café-guix) (_a new monthly event
    for French-speaking scientists and HPC practitioners_)
  - [User & Developer Meetup](/events/2021/user-developer-meetup), 27
    September 2021
  - [Atelier reproductibilité des environnements
    logiciels](/events/2021/atelier-reproductibilit%C3%A9-environnements/)
    (_French-speaking workshop on reproducible software environments_),
    17–18 May 2021
  - [First reproducible research
    hackathon](/blog/2020/07/reproducible-research-hackathon-experience-report/),
    10 July 2020
