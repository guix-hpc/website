title: Atelier reproductibilité des environnements logiciels (francophone)
author: Ludovic Courtès, Konrad Hinsen, Simon Tournier
date: 2021-04-09 14:00
tags: community
---

# Atelier reproductibilité des environnements logiciels

> _This page is about a workshop on the reproducibility of software
> environments for French-speaking scientists, engineers, and system
> administrators.  The workshop took place on-line on **May
> 17–18th, 2021** with up to 80 participants._

Nous avons organisé un atelier _reproductibilité des environnements
logiciels_, en ligne, les matinées des **17 et 18 mai 2021**, qui a
accueilli jusqu'à 80 personnes issues de la recherche, de l'enseignement
supérieur et de l'industrie.

Ces matinées sont **ouvertes à toute personne** s'intéressant aux questions de
[recherche
reproductible](https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/about).
Guix esquisse une solution de l'ordinateur portable aux machines de calculs
mutualisées en passant par la génération de conteneurs.  Ces matinées veulent
refléter cela : quels outils pour appliquer une méthodologie reproductible du
chercheur à l'administrateur système.

L'objectif de ces matinées est d'échanger ou découvrir ce qu'il est déjà
possible de faire concrètement et discuter ce qu’il manquerait à Guix pour
tendre vers une recherche reproductible.

# Programme

## lundi 17 mai

 - 08:50–09:00 [accueil](https://webconf.gricad.cloud.math.cnrs.fr/b/lud-2wr-qmd)
 - 09:00–09:30 **Pourquoi la science a besoin d'environnements logiciels reproductibles**
 - 09:30–10:00 **Utilisation de Guix au mésocentre CCIPL (Pays de Loire)**
 - 10:00–10:30 pause
 - 11:00–11:30 **Concilier le calcul haute performance avec l'utilisation de
   bibliothèques tierces ?**
 - 11:30–12:00 **Douze ans de plateforme — des Modules à Guix**
 - 12:00–12:30 **discussion**

## mardi 18 mai

 - 08:50–09:00 [accueil](https://webconf.gricad.cloud.math.cnrs.fr/b/lud-2wr-qmd)
 - 09:00–09:30 **Génération d'un environnement contrôlé via Debian Snapshot Archive**
 - 09:30–10:00 **Guix comme gestionnaire d'environnement logiciel sur un
   mésocentre HPC : retour d'expérience**
 - 10:00–10:30 pause
 - 11:00–11:30 **Guix et Org mode, deux amis du doctorant sur le chemin vers une thèse reproductible**
 - 11:30–12:30 **table ronde / discussion**

### Pourquoi la science a besoin d'environnements logiciels reproductibles ([PDF](/static/doc/atelier-reproductibilité-2021/konrad-hinsen-pourquoi.pdf))
Présenté par *Konrad Hinsen* (CNRS).

> *Vidéo indisponible, nos excuses !*

La réproductibilité des études scientifiques est un sujet de vifs débats ces
dernières année. On entend même parler d'une crise. Je vais résumer les trois
catégories de (non-)reproductibilité et aller dans les détails de celle dont
la cause est la (non-)reproductibilité des environnements logiciels.

### Utilisation de Guix au mésocentre CCIPL (Pays de la Loire) ([PDF](/static/doc/atelier-reproductibilité-2021/yann-dupont-ccipl.pdf))
Présenté par *Yann Dupont* (Univ. Nantes).

> *Vidéo indisponible, nos excuses !*

Initialement destiné à étoffer l’offre des logiciels disponibles au
mésocentre, Guix est pleinement utilisable au
[CCIPL](https://ccipl.univ-nantes.fr/) depuis fin 2018, mais son usage
nécessite une démarche volontaire de la part de l’usager ; notre souhait est
d’éviter à tout prix les perturbations d’un existant bien établi.

Néanmoins, les usages de Guix se sont multipliés au fil du temps. Nous
disposons de notre propre
[canal](https://guix.gnu.org/manual/fr/guix.fr.html#Canaux) pour enrichir le
panel de logiciels avec des paquets ad-hoc, par exemple des versions d’openMPI
optimisées pour RoCE et destinées à des versions spécifiques de Fortran.

Certains de ces logiciels sont directement mis à disposition de tous nos
usagers, de façon transparente (ils n’ont pas conscience d’utiliser des
paquets Guix), grâce à un profil global dédié rendu disponible à tous (via
l’utilisation de `/usr/local/bin`).

S’il n’est pas actuellement question de remplacer les traditionnels *modules*
pour des raisons historiques, Guix apporte une seconde source bienvenue dans
ce domaine.

Enfin, une partie de l’infrastructure de notre cluster est en train de migrer
vers des machines virtuelles construites par Guix. Ce processus est au long
cours, mais l’objectif est, à terme, d’avoir la possibilité de redéployer
complètement le cluster de façon reproductible.

La présentation fera le point sur les raisons de ces choix, de la modeste
avancée de ces différentes utilisations et les freins à une adoption plus
rapide dans le cadre d’un mésocentre pluridisciplinaire destiné à tous types
de populations.

### Concilier le calcul haute performance avec l'utilisation de bibliothèques tierces ? ([PDF](/static/doc/atelier-reproductibilité-2021/emmanuel-agullo-spack-guix.pdf))
Présenté par *Emmanuel Agullo* (Inria).

![(video)](/static/videos/atelier-reproductibilité-2021/emmanuel-agullo.webm)

Le calcul haute performance requiert souvent de se reposer sur de
multiples logiciels et que ceux-ci soient optimisés sur la machine cible
où tournent ces calculs. Les contraintes d'optimisation sont telles
qu'il est communément admis que le déploiement de ces logiciels ne peut
se faire que manuellement ou en se reposant sur le travail des
administrateurs de la machine cible (_via_ typiquement un `module
load`). Toutefois, la quantité de travail que cela représente lorsqu'on
doit le faire soi-même ou la non disponibilité des fonctionnalités
précisément requises lorsqu'on s'appuie sur les modules fournis par les
administrateurs, fait que bien des codes choisissent de fournir eux-même
certaines fonctionnalités qui pourraient être obtenues via des
bibliothèques tierces, à l'opposé des canons du génie logiciel.

Dans cette présentation, nous reviendrons sur une quête
([CMake](https://cmake.org/), [Spack](https://spack.io/),
et désormais Guix) d'un environnement permettant de déployer de manière
fiable, portable, performante et reproductible des logiciels de calculs
haute performance afin de ne plus craindre le recours à des
bibliothèques tierces.

### Douze ans de plateforme — des Modules à Guix… ([PDF](/static/doc/atelier-reproductibilité-2021/françois-rué-plafrim.pdf))
Présenté par *François Rué* (Inria).

![(video)](/static/videos/atelier-reproductibilité-2021/françois-rué.webm)

… ou comment nous simplifier le boulot.

Douze ans de gestion d'une [plateforme expérimentale](https://www.plafrim.fr/)
à répondre à des besoins très ciblés sur des moyens de Calcul très
hétérogènes.  Pour ce faire, l'équipe technique associée moyens de Calcul
accompagne les utilisateurs par la mise à disposition d'environnements de
calcul dédiés.  Seulement, plus la plateforme se renforce, plus le maintien,
le suivi des environnements et l'accompagnement des utilisateurs devient
complet et complexe. Depuis 3 ans, l'offre de Guix sur les moyens de Calculs a
permis de repenser ce rapport aux environnements, tant du point du maintien,
du suivi et de l'accompagnement.  C'est l'objet de cette présentation.

### Génération d'un environnement contrôlé via Debian Snapshot Archive ([PDF](/static/doc/atelier-reproductibilité-2021/arnaud-legrand-debian-docker.pdf))
Présenté par *Arnaud Legrand* (CNRS/Univ. Grenoble).

![(video)](/static/videos/atelier-reproductibilité-2021/arnaud-legrand.webm)

À l'occasion du [Ten Years Reproducibility
Challenge](https://rescience.github.io/ten-years/), je me suis essayé à la
reconstruction d'un environnement logiciel similaire à celui que j'avais
utilisé en 2009 pour un article. Les dépendances logicielles du code que je
souhaitais exécuter étant très difficiles à recompiler dans un environnement
actuel, je vous expliquerai comment, à l'aide de la [Debian Snapshot
Archive](https://snapshot.debian.org/) et de
[Debuerreotype](https://github.com/debuerreotype/debuerreotype), j'ai pu
réaliser une recette Docker pérenne correspondant exactement à ce que je
souhaitais.

### Guix comme gestionnaire d'environnement logiciel sur un mésocentre HPC : retour d'expérience ([PDF](/static/doc/atelier-reproductibilité-2021/pierre-antoine-bouttier-guix-nix.pdf))
Présenté par *Pierre-Antoine Bouttier* (CNRS).

![(video)](/static/videos/atelier-reproductibilité-2021/pierre-antoine-bouttier.webm)

Depuis plusieurs années, [GRICAD](https://gricad.univ-grenoble-alpes.fr/) a
fait le choix de passer de la commande *module* à **Nix** et **Guix** pour
fournir aux utilisateurs de ses supercalculateurs les environnements logiciels
dont ils avaient besoin. Nous présenterons l'historique et les raisons de ce
choix puis nous exposerons quelques cas d'usages. Enfin, nous nous attarderons
sur les grands points forts de Guix ainsi que les améliorations possibles que
nous entrevoyons.

### Guix et Org mode, deux amis du doctorant sur le chemin vers une thèse reproductible ([PDF](/static/doc/atelier-reproductibilité-2021/marek-felšöci-org-guix.pdf))
Présenté par *Marek Felšöci* (Inria).

![(video)](/static/videos/atelier-reproductibilité-2021/marek-felšöci.webm)

J'ai commencé par utiliser Guix pour faciliter le déploiement de notre
pile logicielle sur différentes plateformes de calcul intensif et
favoriser la reproducibilité des expérimentations menées dans le cadre
de ma thèse. Depuis, l'ensemble de mon environement logiciel est géré
par Guix, du développement et la compilation par l'exécution des tests
de performances jusqu'au post-traitement et publication de résultats. Le
tout documenté et organisé via [Org mode](https://orgmode.org/).

Au moyen de cette présentation, je résumerai le fonctionnement de cet
environnement.


### Table ronde

Voici les trois thématiques dont nous aimerions parler :

- mise à disposition et maintenance des paquets scientifique ; exemples :
  TensorFlow, variantes MPI, etc.
- fonctionalités manquantes ? exemples : conteneurs, transformations de
  paquets, utilisation non-root, etc.
- mise à disposition sur les centres de calculs : centres locaux, régionaux,
  Grid'5000, et après ?

# Modalités / Inscription

En raison des conditions sanitaires, ces rencontres se feront intégralement à
distance par le biais d'outils de webconférence et de discussion instantanée.

[L'inscription se fait en
ligne](https://deploie-repro.sciencesconf.org/registration) ; elle est gratuite et
libre.  Elle nous permet de mieux connaître le
public.  Si vous préférez, vous pouvez simplement envoyer un email à l'adresse
privée `guix-hpc@gnu.org`.  Et si vous êtes intéressé·e, mais dans l'impossibilité
d'assister à l'atelier, n'hésitez pas à envoyer un courriel.

**Organisateurs**

 - Ludovic Courtès (Inria)
 - Konrad Hinsen (CNRS)
 - Simon Tournier (Univ. de Paris)
