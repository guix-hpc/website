title: User & Developer Meetup
author: Simon Tournier, Ludovic Courtès
date: 2021-04-09 14:00
tags: community
---

> This event is over, but you can [read the
> minutes](https://lists.gnu.org/archive/html/guix-science/2021-09/msg00024.html).

Users and developers of GNU Guix in a scientific context, for
reproducible research or high-performance computing (HPC), will meet
on-line, on **Monday 27th September, 9:00–10:30 AM CEST**.

> The meeting will take place in this [BigBlueButton chat
> room](https://qlf-bbb.inria.fr/cou-srm-ted-9y1).  Enter your name or
> nickname and join.

This will be an informal meeting to collectively **map out actions for
the coming year**—define what each of us would like to work on, what we
would like to see happening, and how we can coordinate.  If you use Guix
in an HPC or scientific context, if you package software or develop Guix
itself and related tools, you’re welcome to join and share your ideas!

Topics include:

  - organizing training sessions and workshops;
  - funding opportunities;
  - long-term archival using Software Heritage and Disarchive;
  - citing software using Software Heritage IDs and Guix;
  - “converting” reproducible/active papers to use Guix;
  - packaging machine learning frameworks;
  - programming with GPUs;
  - Julia packaging and importer;
  - CPU micro-architecture support with function multi-versioning;
  - relocatable pack execution on clusters that lack user namespaces.

More details can be found [in this notepad](https://mensuel.framapad.org/p/guix-hpc-meetup-2021-9pw7?lang=en)
and you are free to add your ideas.  We will arrange to split time evenly
among those items so we can browse all the suggestions.

> This event is subject to the [code of conduct of the GNU Guix
> project](https://git.savannah.gnu.org/cgit/guix.git/tree/CODE-OF-CONDUCT).

For any question, please email
[`guix-science@gnu.org`](https://guix.gnu.org/en/contact) or join the
`#guix-hpc` IRC channel on `irc.libera.chat`.

# Organizers

 - Simon Tournier (Université de Paris)
 - Ludovic Courtès (Inria)
