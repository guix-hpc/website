;;; It's as if it were -*- Scheme -*-.
;;;
;;; This file is part of the Guix-HPC web site and is licensed under the same
;;; terms, those of the GNU GPL version 3 or (at your option) any later
;;; version.
;;;
;;; Copyright © 2023, 2024 Ludovic Courtès <ludo@gnu.org>

(use-modules (events workshop-2024 program)
             (events program)
             (srfi srfi-1))

(let ((speakers (delete-duplicates (append-map talk-speakers talks))))
  `((title . "Orateur·rices — Atelier Guix-HPC 2024")
    (event-page . "2024/workshop")
    (content
     (p "Les " ,(number->string (length speakers))
        " orateur·rices qui contribuent au "
        (a (@ (href "/events/2024/workshop/program"))
           "programme")
        " de cette journée.")

     ,(map speaker->sxml
           (sort speakers
                 (lambda (s1 s2)
                   (string<? (speaker-name s1)
                             (speaker-name s2))))))))
