;;; This module is part of the Ten Years of Guix web site and is licensed
;;; under the same terms, those of the GNU GPL version 3 or (at your option)
;;; any later version.
;;;
;;; Copyright © 2022-2024 Ludovic Courtès <ludo@gnu.org>

(define-module (events workshop-2024 program)
  #:use-module (events program)
  #:export (talks
            bigbluebutton-url))

(define yann-dupont
  (speaker "Yann Dupont"
           "GLiCID"
           #:biography
           '(("Yann Dupont is currently working on the architectural, system and network
aspects of " (a (@ (href "https://glicid.fr/")) "GliCID") ", the HPC center
for education and research in the Pays de Loire region of France.  Before
entering the HPC world in 2016, he was in charge of the infrastructure
department at the University of Nantes, where he was involved in Linux,
networking, storage and even programming."))))

(define pierre-antoine-bouttier
  (speaker "Pierre-Antoine Bouttier"
           "GRICAD"
           #:biography
           '("Trained as an applied mathematician, I'm now working at "
             (a (@ (href
                    "https://gricad.univ-grenoble-alpes.fr/"))
                "GRICAD") ", trying to provide Grenoble's
research communities with the most relevant infrastructures and services
possible to meet the needs of scientific computing and research data
management, with a strong focus on reproducibility.")))

(define philippe-virouleau
  (speaker "Philippe Virouleau"
           "Inria"
           #:biography
           '((p "Research software engineer at Inria in Bordeaux, France, I
worked on a variety of topics ranging from web development to compiler
backends, with a significant focus on HPC during my PhD in 2015.  I'm
currently a developper in the Inria team in charge of administering Inria
clusters (now called \"Abaca\").")
             (p "In addition to using Guix on my free time, part of my job also involves
interactions with Guix as it is available for Abaca and Grid5000 users."))))


(define talks
  (sort
   (list (talk "Autonomie, flexibilité et reproductibilité : le pari de Guix-HPC"
               (list (speaker "Ludovic Courtès"
                              "Inria"
                              #:biography
                              '("Research software engineer at Inria in Bordeaux, France, I have been working
with kind yet demanding HPC practitioners who want it all—performance!
flexibility! reproducibility!  I founded Guix in 2012, soon joined by a team
of intrepid developers, and Guix has proved to be a great tool to try and
satisfy those HPC needs.  I am member of the Software chapter of the
French " (i "Comité pour la science ouverte") " (Open Science Committee).")))
               '((p "Le déploiement de logiciels scientifiques en calcul intensif (HPC) est
trop souvent synonyme de souffrance : piles logicielles complexes,
autonomie et flexibilité limitées en tant qu’utilisateurice des moyens
de calcul, et un travail à refaire sur chaque " (i "cluster") ".  Peut-on aller
au-delà des modules ?")
                 (p "Cet exposé revient sur le point de départ de Guix en HPC : donner plus
d’autonomie aux utilisateurices des moyens de calcul et permettre la
mise en commun de nos expertises métier.  Nous discuterons de
l’articulation avec les enjeux de « science ouverte » et de
reproductibilité et des risques associés — quid de la performance ?
Depuis 2017, Guix-HPC suit ce chemin singulier dans le paysage de la
recherche avec des avancées notamment techniques qui portent leurs
fruits."))
               30
               (date "2024-11-07 09:30")
               "ludovic.pdf"
               #:video "ludovic.webm")

         (talk "Utilisation de Guix au mésocentre GliCID (Nantes)"
               (list yann-dupont)
               '((p (a (@ (href "https://glicid.fr/")) "GliCID") " — le
mésocentre unique de la région pays de la Loire — issu de plusieurs centres
de calculs disséminés sur le territoire, a mis à disposition mi-2023 son
premier cluster de calcul.")
                 (p "Disposant de nœuds de calculs et de frontales fonctionnant sous une
distribution Linux très commune, avec des outils habituels tels que les
modules, ce cluster est volontairement proche des précédents : Les usagers
des anciens centres ne sont pas perdus.")
                 (p "Mais en y regardant de plus près, tout n’est pas si
habituel.  Guix y tient une place importante.")
                 (p "Guix est immédiatement utilisable par tous les utilisateurs, sans
restriction, apportant ses avantages : un large choix de logiciels
supplémentaires, la garantie d’une exécution correcte et reproductible sur
l’ensemble des machines (le cluster dispose de plusieurs tranches
hétérogènes). Le tout en totale autonomie pour l’usager : le profil adéquat
doit juste être chargé (évitant de perturber des environnements historiques
parfois fragiles, tels que ceux générés par les modules).")
                 (p "GliCID dispose de son propre canal pour enrichir le panel de logiciels avec
des paquets ad-hoc, par exemple des versions d’Open MPI optimisées pour le
réseau RoCE et destinées à des versions spécifiques de Fortran.")
                 (p "Certains logiciels très communs ou personnalisés pour le cluster sont
directement mis à disposition de tous nos usagers, de façon transparente (ils
n’ont pas conscience d’utiliser des paquets Guix), grâce à un profil global
dédié rendu disponible à tous (via l’utilisation de" (tt "/usr/local/bin")
").")
                 (p "Cette même méthode permet la mise à disposition d’un
binaire global " (tt "guix") " disposant de tous les canaux supplémentaires et
mis à jour toutes les nuits.")
                 (p "Un tout nouvel utilisateur n’a plus à lancer le fameux
premier " (tt "guix pull") ",
pour bénéficier de la logithéque complète et à jour. Cette étape, longue et
normalement nécessaire lors de la toute première utilisation, est souvent mal
comprise. Nous l’avons identifié comme une barrière importante à la
démocratisation de l’outil, en particulier lors de formations.")
                 (p "Nous sommes allés plus loin dans l’utilisation de
Guix. Si la partie visible (nœuds de calcul, frontales principales) reste
basée sur une distribution Linux habituelle, ce n’est plus le cas de la plus
grosse partie de l’infrastructure de nos clusters, qui est elle générée par
Guix System (contrôleurs slurm, gestion d’identités, de haute disponibilité,
bases de données, monitoring, serveurs WWW, bastions SSH, frontales et nœuds
de debug…) et déployée sur des machines virtuelles gérées par Proxmox.")
                 (p "La stabilité et facilité de redéploiement de l’ensemble
sont remarquables.")
                 (p "À l’avenir, certains services ou serveurs complexes restent à convertir sous
Guix, tout comme certaines bibliothèques scientifiques complexes ou
historiques, demandées par nos utilisateurs."))
               45
               (date "2024-11-07 10:00")
               "yann.pdf"
               #:video "yann.webm")

         (talk "Retour d'expérience sur l'utilisation de Guix dans le cadre d'un programme de
recherche"
               (list (speaker "Benjamin Arrondeau"
                              "CNRS"
                              #:biography
                              '((p "Titulaire d'un doctorat en
mécanique des fluides numériques (CFD), j'ai d'abord été une gros utilisateur
des mésocentres avant de passer de l'autre côté de la barrière en tant
qu'ingénieur support HPC au sein de Gricad à Grenoble.  Je travaille
actuellement sur des problématiques de conteneurisation/packaging de codes et
workflows dans le cadre du " (a (@ (href "https://www.pepr-diadem.fr/")) "PEPR DIADEM") "."))))
               '((p "Dans cette présentation, on abordera différents aspects
gravitant autour de Guix : la définition de paquets, la création d'un canal,
l'intégration continue et le passage des paquets Guix aux images Apptainer.
Cela se fera au travers un exemple concret : le projet DIAMOND du "
                    (a (@ (href "https://www.pepr-diadem.fr/")) "PEPR DIADEM")
                    " qui vise à mettre en place une plateforme numérique pour accélérer le
développement et la découverte de nouveaux matériaux.  Une des problématiques
de ce projet est de fournir aux chercheurs une liste de codes exhaustive et
prête à l'emploi sur la plupart des infrastructures de calcul."))
               45
               (date "2024-11-07 11:00")
               "benjamin.pdf"
               #:video "benjamin.webm")

         (talk "Utilisation de Guix pour la gestion d'environnements
reproductibles, flexibles et collaboratifs dans le cadre d'une thèse"
               (list (speaker "Antoine Gicquel"
                              "Inria"
                              #:biography
                              '((p "Je suis Antoine Gicquel, doctorant en première année au centre
Inria de l'Université de Bordeaux.  J'ai effectué mes études à Rennes en
mathématiques appliquées, avec une spécialisation en calcul scientifique.
J'ai ensuite intégré l'équipe " (a (@ (href
                                       "https://www.inria.fr/fr/concace"))
                                   "CONCACE") " pour entamer une thèse portant sur la
composabilité des méthodes hiérarchiques pour l'accélération des produits
matrice-vecteur.  J'ai découvert Guix lors d'un stage précédent ma thèse et
j'ai choisi de l'adopter dans mon workflow de recherche en raison de ses
avantages en termes de reproductibilité, de gestion des environnements et de
flexibilité."))))
               '((p "Dans cette présentation, je montrerai comment la
commande " (tt "guix shell") " facilite le développement dans des environnements isolés et
cohérents entre les collaborateurs, notamment sur des projets comportant de
nombreuses dépendances.  Nous verrons également comment la combinaison avec
la commande " (tt "guix time-machine") " garantit la reproductibilité des expérimentations.  Nous
aborderons la flexibilité apportée par les options de transformation de Guix,
qui permettent d'adapter les environnements à différents cas d'utilisation.")
                 (p "Cette présentation des fonctionnalités de Guix se fera à
travers des exemples concrets issus de ma thèse : le développement d'un
solveur dans le cadre de l'ANR TensorVim et l'encadrement d'un stage sur
l'utilisation de Kokkos."))
               45
               (date "2024-11-07 11:45")
               "antoine.pdf"
               #:video "antoine.webm")

         (talk "Déploiement logiciel sur les supercalculateurs nationaux
avec Guix : un retour d'expérience"
               (list (speaker "Romain Garbage"
                              "Inria"
                              #:biography
                              '((p "Ingénieur de recherche en CDD depuis le
1er décembre 2023 au sein du " (a (@ (href "https://sed.bordeaux.inria.fr"))
                                  "SED de l'INRIA de Bordeaux") ", Romain GARBAGE
travaille sur des problématiques de déploiement logiciel en HPC avec Guix
dans le cadre du "
                                   (a (@ (href "https://numpex.fr")) "PEPR NumPEx") "."))))
               '((p "Comment utiliser Guix pour générer et déployer un
environnement reproductible sur des machines où l'outil n'est pas
disponible ?")
                 (p "Cette présentation est un retour d'expérience issu de
différents cas de déploiement logiciel sur certains
supercalulateurs nationaux français : Jean-Zay (IDRIS) et Adastra
(CINES).  Elle abordera la génération d'archives embarquant une pile
logicielle personnalisée à travers l'utilisation de la commande " (tt "guix
pack") " et le déploiement de ces archives sur ces machines."))
               45
               (date "2024-11-07 14:00")
               "romain.pdf"
               #:video "romain.webm")

         (talk "Table ronde : déploiement de Guix dans les mésocentres"
               (list pierre-antoine-bouttier
                     yann-dupont
                     philippe-virouleau)
               '((p "Voici les thématiques dont nous aimerions parler :"
                    (ul
                     (li "Guix n'est pas installé dans mon mésocentre :
pourquoi ?")
                     (li "J'ai essayé d'installer Guix dans mon mésocentre :
freins, difficultés, ...")
                     (li "Guix est fonctionnel dans mon mésocentre : comment
l'utilise-t-on ?"))))
               60
               (date "2024-11-07 14:45")
               #:type "table ronde"
               #:video "table-ronde.webm"))
   talk-earlier?))

(define bigbluebutton-url
  "https://webinaire.numerique.gouv.fr/meeting/signin/invite/43958/creator/22917/hash/416152c08d81e7d25b1a9fec676fbe16a6191422")
