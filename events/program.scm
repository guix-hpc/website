;;; This module is part of the Ten Years of Guix web site and is licensed
;;; under the same terms, those of the GNU GPL version 3 or (at your option)
;;; any later version.
;;;
;;; Copyright © 2022-2024 Ludovic Courtès <ludo@gnu.org>
;;; Copyright © 2023 Simon Tournier <simon.tournier@inserm.fr>

(define-module (events program)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 match)
  #:export (speaker
            speaker?
            speaker-name
            speaker-title
            speaker-affiliation
            speaker-biography
            speaker->sxml

            date

            talk
            talk?
            talk-title
            talk-speakers
            talk-synopsis
            talk-duration
            talk-date
            talk-end-date
            talk-slides
            talk-video

            talk-id
            speakers->sxml
            talk->sxml
            talk->video-page
            morning-talk?
            afternoon-talk?
            full-length-talk?
            lightning-talk?
            talk-earlier?
            talk-range-predicate
            conjoin

            talk->sxml
            speaker->sxml))

(define-record-type <speaker>
  (make-speaker name title affiliation biography)
  speaker?
  (name        speaker-name)
  (title       speaker-title)
  (affiliation speaker-affiliation)
  (biography   speaker-biography))

(define* (speaker name #:optional affiliation
                  #:key title biography)
  (make-speaker name title affiliation biography))

(define-record-type <talk>
  (make-talk title type speakers synopsis duration date note slides video)
  talk?
  (title       talk-title)
  (type        talk-type)                         ;symbol
  (speakers    talk-speakers)
  (synopsis    talk-synopsis)
  (duration    talk-duration)                     ;minutes
  (date        talk-date)                         ;start time
  (note        talk-note)
  (slides      talk-slides)
  (video       talk-video))

(define* (talk title speakers synopsis duration date
               #:optional slides #:key note video (type 'talk))
  (make-talk title type speakers synopsis duration date
             note slides video))

(define (date str)
  ;; XXX: Force CET (offset: +3600).
  (string->date str "~Y-~m-~d ~H:~M"))

(define (talk-end-date talk)
  "Return the end date of TALK."
  (let* ((start    (talk-date talk))
         (duration (make-time time-duration 0
                              (* 60 (talk-duration talk)))))
    (time-utc->date
     (add-duration (date->time-utc start) duration))))

(define (talk-earlier? t1 t2)
  (time<? (date->time-utc (talk-date t1))
          (date->time-utc (talk-date t2))))


(define (lightning-talk? talk)
  (<= (talk-duration talk) 10))
(define full-length-talk? (negate lightning-talk?))

(define (talk-range-predicate start end)
  (match start
    ((start-hour start-minute)
     (match end
       ((end-hour end-minute)
        (lambda (talk)
          (let ((date (talk-date talk)))
            (and (or (> (date-hour date) start-hour)
                     (and (= (date-hour date) start-hour)
                          (>= (date-minute date) start-minute)))
                 (or (< (date-hour date) end-hour)
                     (and (= (date-hour date) end-hour)
                          (<= (date-minute date) end-minute)))))))))))

(define morning-talk? (talk-range-predicate '(08 30) '(12 30)))
(define afternoon-talk? (negate morning-talk?))

(define (conjoin . procs)
  (let ((procs (reverse procs)))
    (lambda (arg)
      (every (lambda (proc)
               (proc arg))
             procs))))

(define (join lst delimiter)
  ;; Like 'string-join' but for arbitrary elements, typically sxml.
  (let loop ((lst lst)
             (result '()))
    (match lst
      (()
       '())
      ((one)
       (reverse (cons one result)))
      ((head . tail)
       (loop tail (cons* delimiter head result))))))


;;;
;;; Rendering.
;;;

(define (speakers->sxml speakers event-root)
  (define (link speaker body)
    (if (speaker-biography speaker)
        `(a (@ (href ,(string-append event-root "/speakers/#"
                                     (speaker-id speaker))))
            ,body)
        body))

  (match speakers
    ((speaker)                                    ;single speaker
     (link speaker
           (if (speaker-affiliation speaker)
               (string-append (speaker-name speaker) " ("
                              (speaker-affiliation speaker) ")")
               (speaker-name speaker))))
    ((_ ...)
     (let ((affiliations (delete-duplicates
                          (filter-map speaker-affiliation speakers))))
       `(span ,@(join (map (lambda (speaker)
                             (link speaker
                                   (speaker-name speaker)))
                           speakers) ", ")
              ,(match affiliations
                 (() "")
                 (_ (string-append " ("
                                   (string-join affiliations ", ")
                                   ")"))))))))

(define (string->anchor-id str)
  (list->string
   (string-fold-right (lambda (chr lst)
                        (if (char-set-contains? char-set:letter+digit chr)
                            (cons chr lst)
                            (match lst
                              ((#\- _ ...) lst)   ;don't repeat dashes
                              (_ (cons #\- lst)))))
                      '()
                      str)))
(define talk-id
  (compose string-downcase string->anchor-id talk-title))

(define* (talk->sxml talk event-root
                     #:key
                     (slides-directory "/static/doc/workshop-2023"))
  `(li (@ (class ,(if (lightning-talk? talk)
                      "lightning talk"
                      "talk"))
          (id ,(talk-id talk)))
       (div (@ (class "talk-time"))
            ,(date->string (talk-date talk) "~H:~M")
            "–"
            ,(let* ((start (talk-date talk))
                    (end   (talk-end-date talk)))
               (date->string end "~H:~M")))
       ,(match (talk-type talk)
          ('tutorial
           `(div (@ (class "talk-badge")) "tutorial"))
          ((? string? type)
           `(div (@ (class "talk-badge")) ,type))
          (_
           ""))
       ,(if (talk-note talk)
            `(div (@ (class "talk-badge")) ,(talk-note talk))
            "")
       (div (@ (class "talk-title"))
            (a (@ (href ,(string-append "#" (talk-id talk))))
               ,(talk-title talk)))
       (div (@ (class "talk-speakers"))
            ,(speakers->sxml (talk-speakers talk) event-root))
       ,(match (talk-slides talk)
          (#f "")
          (slides
           (if (string-prefix? "https://" slides)
               `(a (@ (href ,slides)
                      (class "talk-slides"))
                   "📁")
               `(a (@ (href ,(string-append slides-directory "/" slides))
                      (class "talk-slides"))
                   "📁"))))
       ,(match (talk-video talk)
          (#f "")
          (video
           `(a (@ (href ,(string-append event-root "/video/"
                                        (talk-id talk)))
                  (class "talk-slides"))
               "📺")))
       ,@(match (talk-synopsis talk)
           ((or "" #f) '())
           (_ `((details
                 (summary "Read more...")
                 (div (@ (class "talk-synopsis"))
                      ,@(match (talk-synopsis talk)
                          ((lst ...) lst)
                          ((? string? synopsis) (list synopsis))))))))))

(define speaker-id
  (compose string->anchor-id speaker-name))

(define (speaker->sxml speaker)
  `(div
    (div (@ (class "talk-title")
            (id ,(speaker-id speaker)))
         (a (@ (href ,(string-append "#" (speaker-id speaker))))
            ,(speaker-name speaker)))
    ,(match (speaker-affiliation speaker)
       (#f "")
       (affiliation `(div (@ (class "talk-speakers")) ,affiliation)))
    (p ,(or (speaker-biography speaker) ""))))

(define* (talk->video-page talk #:key
                           previous next
                           (event "2023/workshop")
                           (program-url "/events/2023/workshop/program")
                           (slides-directory "/static/doc/workshop-2023")
                           (video-directory
                            "/static/videos/workshop-2023")
                           (video-page-directory
                            "/events/2023/workshop/video")
                           (video-poster
                            "/static/images/workshop-2023-video-poster.png")
                           (footer ""))
  "Return a video page (content and metadata as an alist) for TALK."
  `((title . ,(string-append (talk-title talk) " — "
                             (string-join (map speaker-name
                                               (talk-speakers talk))
                                          ", ")))
    (event-page . ,event)
    (content
     (div (@ (class "talk-time"))
          ,(date->string (talk-date talk) "~e ~B ~Y, ~H:~M"))
     ,@(if previous
           `((div (@ (class "talk-time"))
                  (a (@ (class "talk-slides")
                        (href ,(string-append video-page-directory "/"
                                              (talk-id previous))))
                     "⏪")))
           '())
     ,@(if next
           `((div (@ (class "talk-time"))
                  (a (@ (class "talk-slides")
                        (href ,(string-append video-page-directory "/"
                                              (talk-id next))))
                     "⏩")))
           '())

     (div (@ (class "talk-title"))
          (a (@ (href ,(string-append program-url "/#" (talk-id talk))))
             ,(talk-title talk)))
     (div (@ (class "talk-speakers"))
          ,(speakers->sxml (talk-speakers talk)
                           (dirname program-url)))
     ,(match (talk-slides talk)
        (#f "")
        (slides
         `(div (a (@ (href ,(string-append slides-directory "/" slides))
                     (class "talk-slides"))
                  "📁"))))

     (video (@ (class "full-width")
               (controls #t)
               (width "800")
               (height "450")
               (poster ,video-poster))
            (source
             (@ (src ,(string-append video-directory "/"
                                     (talk-video talk))))))

     (div (@ (class "talk-synopsis"))
          ,@(match (talk-synopsis talk)
              ((or "" #f) '())
              ((lst ...) lst)
              ((? string? synopsis) (list synopsis))))

     ,footer)))
