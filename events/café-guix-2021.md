title: Café Guix (francophone)
author: Guix Hackers
date: 2021-10-26 13:00
tags: community
---

# Café Guix

![Image of a hot beverage and the words Café Guix.](/static/images/café-guix.png)

> _This page is about monthly hacking sessions for French-speaking
> scientists, covering a range of topics from user interface matters to
> deep packaging dives.  Read below for past and upcoming sessions._

Le Café Guix est un lieu et un temps d’échange informel autour du gestionnaire
d’environnement logiciel [GNU Guix](https://guix.gnu.org).  Étudiant·e·s,
chercheuses et chercheurs, administrateur·ice système, IT support de laboratoire
ou de Centre de Calcul, tout le monde est bienvenu·e dans ce rendez-vous
mensuel d’une heure où l’on discutera de questionnements apportés par chacun
sur Guix et sa pratique au sens large.

La participation aux Cafés Guix est libre, aucune obligation d’assiduité n’est
imposée.  En fonction des sujets abordés, il est fort possible que vous soyez
parfois peu concernés. Si vous voulez discuter d’un sujet spécifique
(par ex. interface utilisateur·ice, empaquetage, installation système, problème sur
un paquet spécifique), n’hésitez pas à nous en faire part en amont afin que
l’on puisse répondre aux envies de tout le monde.

> ▶ [**La suite en 2022 !**](/events/2022/café-guix/)

## Deuxième session : empaquetage, import et contribution (26 novembre 2021)

Ce deuxième Café Guix parlera de… paquets.  Nous commencerons par utiliser
[`guix
import`](https://guix.gnu.org/manual/devel/en/guix.html#Invoking-guix-import),
pour mettre le résultat dans un dossier utilisable avec l'option `--load-path`
et nous expliquerons ce qu'il faut ajouter et pourquoi.  Puis nous
présenterons quelques rudiments de
[Guile](https://www.gnu.org/software/guile/) pour avoir des paquets publiables
directement dans Guix.  Idéalement, si le temps le permet, nous
[soumettrons](https://guix.gnu.org/manual/devel/en/guix.html#Contributing) un
paquet.

## Première session : empaquetage (26 octobre 2021)

Le premier Café Guix parlera d'empaquetage à partir d’un exemple
de paquet « amateur » d’un logiciel scientifique.  La session assumera la
connaissance d'une création de paquet « basique », par exemple *via* [`guix
import`](https://guix.gnu.org/manual/fr/html_node/Invoquer-guix-import.html#Invoquer-guix-import)
et discutera entre autres le champ
[`arguments`](https://guix.gnu.org/manual/fr/html_node/reference-de-package.html#r_00e9f_00e9rence-de-package) et
la modification des
[phases](https://guix.gnu.org/manual/fr/html_node/Phases-de-construction.html).

## Continuer la discussion

[Rejoignez le groupe Mattermost
dédié](https://mattermost.univ-nantes.fr/signup_user_complete/?id=njdxbdazafddtq6wsm6cgrr95r)
pour prolonger la discussion entre deux sessions !

## Organisateurs

 - Pierre-Antoine Bouttier (Univ. Grenoble-Alpes)
 - Ludovic Courtès (Inria)
 - Yann Dupont (Univ. Nantes)
 - Jean-François Guillaume (Univ. Nantes)
 - Simon Tournier (Univ. Paris)
