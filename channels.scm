;;; This is the database of Guix channels listed at
;;; <https://hpc.guix.info/channels>.

(let ((%inria-key "\
(public-key
 (ecc
  (curve Ed25519)
  (q #89FBA276A976A8DE2A69774771A92C8C879E0F24614AAAAE23119608707B3F06#)))"))

  (list (channel-description
         'guix-hpc
         "https://gitlab.inria.fr/guix-hpc/guix-hpc"
         #:synopsis '("High-performance computing packages at "
                      (a (@ (href "https://www.inria.fr/en")) "Inria"))
         #:logo-url
         "https://gitlab.inria.fr/uploads/-/system/project/avatar/2095/guix-hpc-square.jpg?width=64"
         #:ci-badge "https://guix.bordeaux.inria.fr/jobset/guix-hpc/badge.svg"
         #:ci-url "https://guix.bordeaux.inria.fr/jobset/guix-hpc"
         #:substitute-url "https://guix.bordeaux.inria.fr"
         #:substitute-key %inria-key)

        (channel-description
         'guix-past
         "https://gitlab.inria.fr/guix-hpc/guix-past"
         #:synopsis "Providing old package versions"
         #:logo-url
         "https://gitlab.inria.fr/uploads/-/system/project/avatar/19816/guix-bw.png?width=64"
         #:ci-badge "https://guix.bordeaux.inria.fr/jobset/guix-past/badge.svg"
         #:ci-url "https://guix.bordeaux.inria.fr/jobset/guix-past"
         #:introduction '(make-channel-introduction
                          "0c119db2ea86a389769f4d2b9c6f5c41c027e336"
                          (openpgp-fingerprint
                           "3CE4 6455 8A84 FDC6 9DB4  0CFB 090B 1199 3D9A EBB5"))
         #:substitute-url "https://guix.bordeaux.inria.fr"
         #:substitute-key %inria-key)

        (channel-description
         'guix-science
         "https://github.com/guix-science/guix-science"
         #:synopsis "General scientific and statistics packages"
         #:introduction '(make-channel-introduction
                          "b1fe5aaff3ab48e798a4cce02f0212bc91f423dc"
                          (openpgp-fingerprint
                           "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))
         #:ci-badge "https://guix.bordeaux.inria.fr/jobset/guix-science/badge.svg"
         #:ci-url "https://guix.bordeaux.inria.fr/jobset/guix-science"
         #:substitute-url "https://guix.bordeaux.inria.fr"
         #:substitute-key %inria-key)

        (channel-description
         'guix-cran
         "https://github.com/guix-science/guix-cran"
         #:synopsis '("Automated import of "
                      (a (@ (href "https://cran.r-project.org/")) "CRAN")
                      " packages")
         #:logo-url
         "https://github.com/guix-science/guix-cran-scripts/raw/master/logo.png"
         #:ci-badge "https://guix.bordeaux.inria.fr/jobset/guix-cran/badge.svg"
         #:ci-url "https://guix.bordeaux.inria.fr/jobset/guix-cran"
         #:substitute-url "https://guix.bordeaux.inria.fr"
         #:substitute-key %inria-key)

        (channel-description
         'guix-bioc
         "https://github.com/guix-science/guix-bioc"
         #:synopsis '("Automated import of "
                      (a (@ (href "https://bioconductor.org/")) "Bioconductor")
                      " packages")
         #:logo-url
         "https://github.com/guix-science/guix-cran-scripts/raw/master/logo.png"
         #:ci-badge "https://guix.bordeaux.inria.fr/jobset/guix-bioc/badge.svg"
         #:ci-url "https://guix.bordeaux.inria.fr/jobset/guix-bioc"
         #:substitute-url "https://guix.bordeaux.inria.fr"
         #:substitute-key %inria-key)

        (channel-description
         'guix-bioinformatics
         "https://gitlab.com/genenetwork/guix-bioinformatics"
         #:synopsis '("Bioinformatics and HPC packages used by "
                      (a (@ (href "https://genenetwork.org/environments/"))
                         "Genenetwork")
                      " at the University of Tennessee"))

        (channel-description
         'guix-genomics
         "https://github.com/ekg/guix-genomics"
         #:synopsis '("Packages for bioinformatics and genomics used at "
                      (a (@ (href "https://genenetwork.org/facilities/"))
                         "UTHSC") "."))

        (channel-description
         'guix-zpid
         "https://github.com/leibniz-psychology/guix-zpid"
         #:synopsis '("Packages in use at the "
                      (a (@ (href "https://leibniz-psychology.org/en/"))
                         "Leibniz Institute for Psychology Information")
                      "."))

        ;; Channels that provide non-free software.

        (channel-description
         'guix-hpc-non-free
         "https://gitlab.inria.fr/guix-hpc/guix-hpc-non-free"
         #:synopsis '("Non-free software commonly used in HPC, such as MKL
and CUDA, as well as free packages that depend on it.")
         #:policy (channel-policy
                   #:free-software-only? #f
                   #:build-from-source? #f)
         #:ci-badge "https://guix.bordeaux.inria.fr/jobset/guix-hpc-non-free/badge.svg"
         #:ci-url "https://guix.bordeaux.inria.fr/jobset/guix-hpc-non-free"
         #:substitute-url "https://guix.bordeaux.inria.fr"
         #:substitute-key %inria-key)

        (channel-description
         'guix-science-nonfree
         "https://github.com/guix-science/guix-science-nonfree/"
         #:synopsis '("Non-free scientific software, notably bioinformatics and
statistics.")
         #:policy (channel-policy
                   #:free-software-only? #f
                   #:build-from-source? #f)
         #:introduction '(make-channel-introduction
                          "58661b110325fd5d9b40e6f0177cc486a615817e"
                          (openpgp-fingerprint
                           "CA4F 8CF4 37D7 478F DA05  5FD4 4213 7701 1A37 8446"))
         #:ci-badge "https://guix.bordeaux.inria.fr/jobset/guix-science-nonfree/badge.svg"
         #:ci-url "https://guix.bordeaux.inria.fr/jobset/guix-science-nonfree"
         #:substitute-url "https://guix.bordeaux.inria.fr"
         #:substitute-key %inria-key)))
