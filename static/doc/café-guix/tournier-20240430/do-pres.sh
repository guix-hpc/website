#!/usr/bin/env bash

guix time-machine -C channels.scm   \
       -- shell -C -m manifest.scm  \
       -- rubber --pdf tournier-20240430.tex
