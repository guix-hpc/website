\documentclass[aspectratio=169,ignorenonframetext,presentation]{beamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
%\usepackage[french]{babel}
\usepackage{fancyvrb}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing,positioning, arrows.meta}


\usetheme{Madrid}
\useoutertheme[subsection=true]{smoothbars}

\makeatletter
\usecolortheme{whale}
\usecolortheme{orchid}
\setbeamerfont{block title}{size={}}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{itemize items}[triangle]
\makeatletter
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
    \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,left]{author in head/foot}%
      \usebeamerfont{author in head/foot}\quad S. Tournier
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.7\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
      \usebeamerfont{title in head/foot}\insertshorttitle
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
      \insertframenumber{} / \inserttotalframenumber\hspace*{2ex}
    \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatother

\title[Guix: a short introduction to G-expression]
{Introduction to G-expression}
\subtitle{\emph{(how to beat around the bush)}}
\author{Simon Tournier}
\date{April, 30th, 2024}
\institute{Inserm US53 - UAR CNRS 2030 \\ \texttt{simon.tournier@inserm.fr}}
\hypersetup{
  pdfauthor={Simon Tournier},
  pdftitle={Guix: Introduction to G-expression},
  pdfkeywords={GNU Guix, reproducible, software deployment, package management},
  pdfsubject={short introduction of Guix G-expression},
  pdfcreator={with love},
  pdflang={English}}


\newcommand{\violet}{\textcolor{violet}}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\magenta}{\textcolor{magenta}}
\newcommand{\cyan}{\textcolor{cyan}}
\newcommand{\green}{\textcolor{green}}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\newcommand{\mauve}{\textcolor{mauve}}

\input{listings-scheme}

\newcommand{\someguile}{\textcolor{someguile}}
\newcommand{\somestring}{\textcolor{somestring}}
\newcommand{\somevariable}{\textcolor{somevariable}}

\newcommand{\smiley}[1]{${\textsf{#1~}}^{\textrm{:-)}}$}

\newcommand{\hrefsf}[2]{\href{#1}{\textsf{#2}}}
\newcommand{\code}[1]{\texttt{#1}}

\begin{document}

Après la lecture de la section [« Définition des
paquets »](https://guix.gnu.org/manual/devel/fr/guix.fr.html#D_00e9finition-des-paquets),
et après quelques itérations test/erreur, nous devons ici ou là mettre des
symboles comme \texttt{\#\~{}(\#\${}(\%}.  Ce Café Guix essaiera de les rendre moins
cryptiques.  Nous introduirons quelques ingrédients de la boite à outils
Scheme/Guile (quote, quasiquote, unquote) ce qui nous mènera aux
G-expressions.


\begin{frame}[plain, fragile, noframenumbering]{}
  \vspace{5mm}
  \titlepage

  \vfill{}
  \vspace{-12mm}
  \begin{center}
    \includegraphics[width=0.2\paperwidth]{static/Guix-white}

    \vspace{-10mm}
    \href{https://hpc.guix.info}{\texttt{https://hpc.guix.info}}
  \end{center}
  \vfill{}
  \normalsize
  \begin{minipage}{0.3\paperwidth}
    \vspace{-7mm}
    \begin{center}
      \includegraphics[width=0.2\paperwidth]{static/cafe-guix}
    \end{center}
  \end{minipage}
  \begin{flushright}
    \begin{minipage}{0.2\paperwidth}
      \vspace{-20mm}
      \includegraphics[width=0.2\paperwidth]{static/u-paris}
    \end{minipage}
  \end{flushright}
\end{frame}



\begin{frame}<presentation>[plain, noframenumbering]{Too long; don't read}
  \begin{block}{}
    Guix is implemented in Scheme/Guile language.
  \end{block}

  \vfill
  \begin{exampleblock}{}
    \quad\quad\quad%
    Guix provides a Domain-Specific Language (DSL),
    \\\quad\\
    \quad\quad\quad\quad%
    This DSL helps in defining \texttt{package}s.
  \end{exampleblock}

  \vfill
  \begin{alertblock}{}
    \quad\quad\quad\quad\quad\quad\textbf{%
      ``G-expression'' is another embedded DSL and adapted to build \emph{thing}.
    }
    \\\quad\\
    \quad\quad\quad\quad\quad\quad\quad%
    This DSL helps in describing a sequence of actions;\\
    \hfill to be performed to produce item in the store.
  \end{alertblock}
\end{frame}


\begin{frame}<presentation>[plain, noframenumbering]{Too short; more read}
  \vspace{-0.25cm}
  \begin{block}{}
    \begin{itemize}
    \item Code Staging in GNU Guix,
      \small{\emph{in GPCE'17, 16th ACM SIGPLAN International Conference}}

      \href{https://doi.org/10.48550/arXiv.1709.00833}%
      {https://doi.org/10.48550/arXiv.1709.00833}
    \end{itemize}
  \end{block}

  \vfill
  \begin{alertblock}{}
    \begin{itemize}
    \item Guix manual: read twice;
      1, 2, 3 and 4 (``anti-order'') then 4, 3, 2 and 1 (``natural'' order)
      \vfill
      \begin{enumerate}
      \item section G-Expressions

        \hfill
        \href{https://guix.gnu.org/manual/devel/en/guix.html\#G\_002dExpressions}%
        {https://guix.gnu.org/manual/devel/en/guix.html\#G\_002dExpressions}
      \item section The Store Monad

        \hfill
        \href{https://guix.gnu.org/manual/devel/en/guix.html\#The-Store-Monad}%
        {https://guix.gnu.org/manual/devel/en/guix.html\#The-Store-Monad}
      \item section Derivations

        \hfill
        \href{https://guix.gnu.org/manual/devel/en/guix.html\#Derivations}%
        {https://guix.gnu.org/manual/devel/en/guix.html\#Derivations}
      \item section The Store

        \hfill
        \href{https://guix.gnu.org/manual/devel/en/guix.html\#The-Store}%
        {https://guix.gnu.org/manual/devel/en/guix.html\#The-Store}
      \end{enumerate}
    \end{itemize}
  \end{alertblock}

  \vfill
  \begin{exampleblock}{}
    \begin{itemize}
    \item source code
      \begin{itemize}
      \item other packages
      \item the build system you know the most
        \hfill e.g., \texttt{(guix build-system julia)}
      \item the hard way: module \texttt{(guix gexp)}
      \end{itemize}
    \end{itemize}
  \end{exampleblock}
\end{frame}


\begin{frame}<presentation>[plain, noframenumbering]{From my experience}
  \begin{block}{}
    \begin{center}
      the main difficulties when speaking about \emph{G-expression} are from:
    \end{center}
  \end{block}

  \vfill
  \begin{exampleblock}{}
    \begin{enumerate}
    \item Missing knowledge about what is Guile-specific language
      \hfill
      (\texttt{lambda}, \texttt{let}, conventions, etc.)

    \item Missing knowledge about Scheme concepts
      \hfill
      (``\emph{quotation}'')
    \end{enumerate}
  \end{exampleblock}

  \vfill
  \begin{alertblock}{}
    \begin{center}
      here the aim is thus to introduce some
    \end{center}
  \end{alertblock}

\end{frame}


\begin{frame}<presentation>[plain, noframenumbering]{Plan for today}
  \begin{center}
    \textbf{A pedestrian journey toward G-expressions and Schemey-way}
  \end{center}

  \vfill
  \begin{center}
    \begin{minipage}{0.7\textwidth}
      \begin{itemize}
      \item The aim is to provide some ``helpers'',
      \item For easing the reading of Guix manual and source code.
      \end{itemize}
    \end{minipage}
  \end{center}

  \vfill
  \setcounter{tocdepth}{2}
  \tableofcontents
\end{frame}


\section{Concretely}


\begin{frame}<presentation>[fragile, label=def-pkg]%
  {Defining Packages: key points}
  \begin{description}
  \item[define-module] Create a Guile module
    \begin{description}
    \item[\#:use-module] List the modules required for Guile \emph{compiling} the
      recipe
    \end{description}
  \item[define-public] Define and export
    \begin{description}
    \item[package] Object representing a package (Scheme record)
      \begin{description}
      \item[name] The string we prefer
      \item[version] A string that makes sense
      \item[source] Define where to fetch the source code
      \item[build-system] Define how to build
      \item[arguments] The arguments for the build system
      \item[inputs] List the other package dependencies
      \end{description}
    \end{description}
  \end{description}
  \begin{block}{}
    \begin{center}
      all sounds clear\dots{}
    \end{center}
  \end{block}
\end{frame}


\begin{frame}<presentation>[fragile, label=intro-scheme]{Examples of packages}
\begin{verbatim}
$ guix edit gsl
$ guix edit r-torch
\end{verbatim}

  \vfill
  \begin{center}
    What does it mean?
  \end{center}
  \vfill

  \begin{description}
  \item[keyword] \code{define-public}, \code{let}, \code{lambda}
  \item[record] \code{package}
  \item[convention] \code{\%something}, \code{something?}, \code{something*}
  \item[symbol] quote (\code{'}), backtick (\code{\`{}}), comma (\code{,}),
    comma at (\code{,@}), underscore (\code{\_})\\
    G-expressions: \code{\#\~{}} or \code{\#\$}
  \end{description}
\end{frame}


\begin{frame}<presentation>[fragile, label=pkg-repl]{Package from \texttt{guix repl}}

  \begin{block}{}
    \begin{center}
      Recommendation for the file \texttt{\~{}/.guile}
    \end{center}
\begin{verbatim}
(use-modules (ice-9 readline)       ;; package guile-readline, guile?
             (ice-9 format)
             (ice-9 pretty-print))
(activate-readline)
\end{verbatim}
  \end{block}

  \begin{enumerate}
  \item Type \texttt{r-torch} then \texttt{,q}
  \item Type \texttt{(use-modules (gnu packages cran))} (or \texttt{,use(gnu
      packages maths)}) and again \texttt{r-torch}
  \item Try \texttt{(package-name r-torch)} then \texttt{,use(guix packages)} (or
    \texttt{,use(guix)}) and repeat
  \end{enumerate}

  \begin{center}
    \textbf{Two names: the Scheme variable and the string.}
  \end{center}
\end{frame}


\begin{frame}<presentation>[fragile]{Package from \texttt{guix repl} II}
  \vspace{-.25cm}
  \begin{enumerate}
  \item How to display the version?
  \item Try \texttt{(package-inputs r-torch)}
  \item About the arguments?
  \end{enumerate}

  \begin{block}{}
\begin{verbatim}
scheme@(guix-user)> ,pp (package-arguments r-torch)
$3 = (#:phases
 #<gexp (modify-phases %standard-phases (add-after (quote install) (quote link-libraries)
(lambda* (#:key inputs #:allow-other-keys) (let ((deps (string-append #<gexp-output out> "/site-library/torch/deps")) (site-packages (string-append "/lib/python" #<gexp-input "3.10":out> "/site-packages"))) (mkdir-p deps) (symlink (search-input-file inputs (string-append site-packages "/torch/lib/libtorch.so")) (string-append deps "/libtorch.so")) (symlink (search-input-file inputs "/lib/liblantern.so")
(string-append deps "/liblantern.so")))))) gnu/packages/cran.scm:30475:8 769ee1ddede0>)

scheme@(guix-user)> ,pp (package-arguments gsl)
$4 = (#:configure-flags
 (list "--disable-static")
 #:phases
 (modify-phases %standard-phases))
\end{verbatim}
  \end{block}
\end{frame}



\section{Scheme/Guile Swiss-knife toolbox}


\begin{frame}<presentation>[fragile, label=first-thing-first]{First things
    first
    \hfill
  'S' is before 'G'}
  \begin{center}
    S-expression: atom or expression of the form \code{(x y …)}

    \footnotesize{%
      \emph{%
        S-exp: opening-parenthesis something … closing-parentheis
      }}

  \end{center}

  \begin{description}
  \item[atom:] +, *, \code{list}, etc.
  \item[expression:] \code{(list 'one 2 "three")}
  \end{description}

  \visible<2->{
  \begin{block}{}
  Call/evaluation with parenthesis

  \hfill e.g., apply the atom \code{list} to the rest

  \hfill \code{(list 'one 2 "three")} returns the list composed by the
  elements \code{(one 2 "three")}
  \end{block}
  }

  \vfill

  \visible<3->{
  \begin{exampleblock}{}
  Quote protects from the call (do not evaluate)

  \hfill e.g., \code{'one} returns plain \code{one}

  \hfill e.g., \code{'(list one 2 "three")} returns
  \code{(list 'one 2 "three")}

  \hfill
  \code{'(list 'one 2 "three")} returns \code{(list (quote one) 2 "three")}
  \end{exampleblock}
  }
\end{frame}


\begin{frame}<presentation>[fragile, label=second-thing]{Second thing second}
  \begin{description}
  \item[variable] \code{(define some-variable 42)}
  \item[procedure] \code{(lambda (argument) (something argument))}
  \end{description}

  \begin{block}{}
    \begin{center}
      Define a procedure
    \end{center}
\begin{lstlisting}[language=Scheme]{Name=proc}
(define my-name-procedure
  (lambda (argument1 argument2)
    (something-with argument1)))
\end{lstlisting}
\end{block}

  \begin{alertblock}{}
\begin{verbatim}
(define (my-name-procedure argument1 argument2)
  (something-with argument1))
\end{verbatim}
  \end{alertblock}
  Call \code{(my-name-procedure 1 "two")}

  \small
  \begin{center}
  \code{define-public} is sugar to \code{define} and export (see «
  \href{https://www.gnu.org/software/guile/manual/html_node/Creating-Guile-Modules.html}{Creating
    Guile Modules (link)}) »
\end{center}
\end{frame}


\begin{frame}<presentation>[fragile, label=def-let]{Local variables \hfill = \code{let}}
  \begin{block}{}
    \begin{center}
      \textbf{Independent local variables}
    \end{center}
\begin{verbatim}
(define (add-plus-2 x y)
  (let ((two 2)
        (x+y (+ x y)))
    (+ x+y two)))
\end{verbatim}
  \end{block}

  \begin{exampleblock}{}
    \begin{center}
      \textbf{Inter-dependant local variables}
    \end{center}
\begin{verbatim}
(define (add-plus-2-bis x y)
  (let* ((two 2)
         (x+two (+ x two))
         (result (+ y x+two)))
    result))
\end{verbatim}
  \end{exampleblock}
\end{frame}



\begin{frame}<presentation>[fragile, label=let-pkg]{Local variables: example%
    \hfill seen in package \texttt{julia-biogenerics}}
  \begin{exampleblock}{}
    \begin{lstlisting}[language=Lisp]
(define-public julia-biogenerics
  (let ((commit "a75abaf459250e2b5e22b4d9adf25fd36d2acab6")
        (revision "1"))
    (package
      (name "julia-biogenerics")
      (version (git-version "0.0.0" revision commit))
    ...
    \end{lstlisting}
  \end{exampleblock}
\end{frame}



\begin{frame}<presentation>[fragile, label=conventions]{Conventions}
  \begin{description}
  \item<1->[predicate] \textbf{ends with question mark (\code{?})}, return boolean
    (\code{\#t} or \code{\#f}\\
    \hfill \uline{note:} \code{\#true} or \code{\#false} works too)

    \hfill e.g., \code{(string-prefix? "hello" "hello-world")}
  \item<2->[variant] \textbf{ends with star mark (\code{*})}
    \hfill e.g., \code{let*}

    \hfill \code{lambda*} more argument
\begin{verbatim}
(lambda* (#:key inputs #:allow-other-keys)
             (setenv "CONFIG_SHELL"
                     (search-input-file inputs "/bin/sh")))
;; seen in package frama-c
\end{verbatim}
  \item<3->[keyword] \textbf{starts with sharp colon (\code{\#:})}

    \hfill e.g., \code{\#:key}, \code{\#:configure-flags}, \code{\#:phases}
  \item<4->[“global“] \textbf{starts with percent (\code{\%})}

    \hfill e.g., \code{\%standard-phases}
    \begin{center}
      \textbf{warning:} keyword starting with \texttt{\#:} is not a convention
    \end{center}
  \end{description}
\end{frame}



\section{Quote, quasiquote and unquote}


\begin{frame}<presentation>[fragile, label=quote-and-co]{Quote, quasiquote, unquote}
  \vspace{-0.3cm}
  \begin{description}
  \item[quote] do not evaluate (keep as it is) \hfill quote \code{'}
  \item[quasiquote] unevaluate except escaped \hfill backtick \code{\`{}}
  \item[unquote] evaluate that escaped \hfill coma \code{,}
  \end{description}

  \vspace{-0.1cm}
  \begin{block}{}
    \begin{center}
      \texttt{guix repl}
    \end{center}
    \vspace{-0.3cm}
    \begin{enumerate}
    \item<2-> Type
      \vspace{-0.3cm}
      \begin{lstlisting}[language=Lisp]
        scheme@(guix-user)> (define ho "path/to/ho")
        scheme@(guix-user)> (string-append ho "/bin/bye")
        scheme@(guix-user)> `(string-append ho "/bin/bye")
        scheme@(guix-user)> `(string-append ,ho "/bin/bye")
      \end{lstlisting}
      \vspace{-0.3cm}
    \item<3-> Type
      \vspace{-0.3cm}
\begin{verbatim}
        scheme@(guix-user)> (eval $4 (interaction-environment))
\end{verbatim}
    \end{enumerate}
  \end{block}
  \visible<4>{
  \begin{alertblock}{}
    \begin{center}
      \textbf{read-time} vs \textbf{eval-time}
    \end{center}
  \end{alertblock}
  }
\end{frame}



\begin{frame}<presentation>[fragile, label=quote-and-co-2]%
  {Quote, quasiquote, unquote II \hfill splicing}
  \begin{description}
  \item[unquote-splicing] as unquote and insert the elements
    \hfill comma-at \code{,@}
  \end{description}

  \begin{center}
    the expression must evaluate to a list
  \end{center}

  \vfill
  \begin{block}{}
    \begin{enumerate}
    \item Type
      \begin{lstlisting}[language=Lisp]
scheme@(guix-user)> (define of (list #:vegetable 'tomatoes
                                     #:dessert (list "cake" "pie")))
scheme@(guix-user)> `(more ,@of that)
scheme@(guix-user)> `(more ,of that)
      \end{lstlisting}
    \end{enumerate}
  \end{block}
\end{frame}



\begin{frame}<presentation>[fragile, label=quote-and-co-3]%
  {Quote, quasiquote, unquote II bis \hfill splicing}

  \begin{lstlisting}[language=Lisp]
    (arguments
      `(,@(package-arguments gsl)
         #:configure-flags (list "--disable-shared")
         #:make-flags (list "CFLAGS=-fPIC")))
    ;; seen in package gsl-static
   \end{lstlisting}

   \vfill
  \begin{block}{}
        \begin{enumerate}
        \item Type
          \begin{lstlisting}[language=Lisp]
 scheme@(guix-user)> ,use(gnu packages maths)
 scheme@(guix-user)> ,pp (package-arguments gsl)
 scheme@(guix-user)> ,pp `(,@(package-arguments gsl)
                            #:configure-flags (list "--disable-shared")
                            #:make-flags (list "CFLAGS=-fPIC"))
          \end{lstlisting}
        \end{enumerate}
      \end{block}
\end{frame}



\begin{frame}<presentation>[fragile, label=quote-and-co-3]%
  {Quote, quasiquote, unquote III \hfill digression}
  \vspace{-0.2cm}
  \begin{exampleblock}{}
    \begin{center}
      \texttt{substitute-keyword-arguments} substitutes keyword arguments
    \end{center}
  \end{exampleblock}

  \vspace{-0.1cm}
  \begin{lstlisting}[language=Lisp]
    (arguments
     (substitute-keyword-arguments (package-arguments hdf4)
       ((#:configure-flags flags) `(cons* "--disable-netcdf" ,flags))))
    ;; seen in package hdf4-alt
   \end{lstlisting}

   \vspace{-0.1cm}
   \begin{block}{}
     \vspace{-0.2cm}
     % \visible<2>{
     \begin{enumerate}
     \item<2->
     \begin{lstlisting}[language=Lisp]
scheme@(guix-user)> ,use(srfi srfi-1)
scheme@(guix-user)> ,pp (lset-difference equal?
          (substitute-keyword-arguments (package-arguments hdf4)
            ((#:configure-flags flags) `(cons* "--disable-netcdf" ,flags)))
          (package-arguments hdf4))
$1 = ((cons* "--disable-netcdf" (list "--enable-shared" "FCFLAGS=-fallow-argument-mismatch"
                                      "FFLAGS=-fallow-argument-mismatch"
                                      "--enable-hdf4-xdr")))
     \end{lstlisting}
     \end{enumerate}
     % }
   \end{block}

\end{frame}



\begin{frame}<presentation>[fragile, label=assoc-ref]{Association list}
  \begin{center}
    (\emph{alist}) association list = list of pairs \code{(this . that)}

    \bigskip

    think: \code{(list (key1 . value1) (key2 . value2) ...)}
  \end{center}
  \begin{block}{}
    \begin{enumerate}
    \item Type
      \begin{lstlisting}[language=Lisp]
 scheme@(guix-user)> (define alst (list '(a . 1) '(2 . 3) '("foo" . v)))
 scheme@(guix-user)> (assoc-ref alst "foo")
 scheme@(guix-user)> (assoc-ref alst 'a)
      \end{lstlisting}
    \item Type
      \begin{lstlisting}[language=Lisp]
      scheme@(guix-user)> (assoc-ref (package-inputs r-torch) "python-pytorch")
    \end{lstlisting}
  \end{enumerate}
  \end{block}
\end{frame}


\begin{frame}<presentation>[fragile, label=example-freedgnuplot]{Ready?%
    \hfill seen in package \texttt{feedgnuplot}}
  \vspace{-0.3cm}
  \begin{lstlisting}[language=Lisp]
     1	(add-after 'install 'wrap
     2	  (lambda* (#:key inputs outputs #:allow-other-keys)
     3	    (let* ((out (assoc-ref outputs "out"))
     4	           (gnuplot (search-input-file inputs "/bin/gnuplot"))
     5	           (modules '("perl-list-moreutils" "perl-exporter-tiny"))
     6	           (PERL5LIB (string-join
     7	                      (map (lambda (input)
     8	                             (string-append (assoc-ref inputs input)
     9	                                            "/lib/perl5/site_perl"))
    10	                           modules)
    11	                      ":")))
    12	      (wrap-program (string-append out "/bin/feedgnuplot")
    13	        `("PERL5LIB" ":" suffix (,PERL5LIB))
    14	        `("PATH" ":" suffix (,(dirname gnuplot)))))))
  \end{lstlisting}
\end{frame}


\begin{frame}[plain]{}

  \begin{center}
    We want G-expression!
  \end{center}

  \vfill

  \begin{flushright}
    Enough of S-expression.
  \end{flushright}
\end{frame}



\section{G-expression}


\begin{frame}<presentation>[fragile, label=arguments]%
  {Pass \texttt{arguments} to the build system}
  % \begin{lstlisting}[language=Lisp]
\begin{verbatim}
 (arguments
  (list #:configure-flags
        #~(list "--enable-dynamic-build"
                #$@(if (target-x86?)
                       #~("--enable-vector-intrinsics=sse")
                       #~())
                "--enable-ldim-alignment")
        #:make-flags #~(list "FC=gfortran -fPIC")
        #:phases
        #~(modify-phases %standard-phases
\end{verbatim}
%\end{lstlisting}

\vfill
\begin{alertblock}{}
  \begin{center}
    \code{\#:} introduces keyword.\\

    What is \code{\#\~{}} or \code{\#\$@}?
  \end{center}
\end{alertblock}
\end{frame}


\begin{frame}<presentation>[fragile, label=g-expression]%
  {G-expression}
  \begin{center}
    {Remember quasiquote and unquote?}
    \vfill

    \begin{alertblock}{}
      \centering
      \begin{tabular}{clclr}
        \code{\#\~{}}&is similar as&\code{\`{}}&with context
        &(unevaluate except escaped)
        \\
        \code{\#\${}}&is similar as&\code{,}&with context
        &(evaluate that escaped)
        \\
        \code{\#\$@{}}&is similar as&\code{,@}&with context
        &(evaluate and insert)
      \end{tabular}
    \end{alertblock}

    \vfill
    where context means system of host machine, store state, etc.
  \end{center}
\end{frame}


\begin{frame}<presentation>[fragile]%
  {Intuition}

\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> ,use(gnu packages maths)
scheme@(guix-user)> (define gsl-name (package-name gsl))
scheme@(guix-user)> `(begin (string-append ,gsl-name "/yet/another"))
\end{lstlisting}

  \vfill
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> `(begin (string-append ,gsl "/yet/another"))
\end{lstlisting}
\end{frame}


\begin{frame}<presentation>[fragile]%
  {Intuition II}

\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> `(begin (string-append ,gsl-name "/yet/another"))
$1 = (begin (string-append "gsl" "/yet/another"))
\end{lstlisting}

  \vfill
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> `(begin (string-append ,gsl "/yet/another"))
$2 = (begin (string-append #<package gsl@2.7.1 gnu/packages/maths.scm:679 77da2800db00> "/yet/another"))
\end{lstlisting}
\end{frame}


\begin{frame}<presentation>[fragile]%
  {Intuition III}

  \begin{lstlisting}[language=Lisp]
scheme@(guix-user)> `(begin (string-append ,gsl-name "/yet/another"))
$1 = (begin (string-append "gsl" "/yet/another"))
\end{lstlisting}

\vfill
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> (eval $1 (interaction-environment))
$3 = "gsl/yet/another"
\end{lstlisting}

\vfill
\begin{exampleblock}{}
\vfill
  \begin{lstlisting}[language=Lisp]
scheme@(guix-user)> `(begin (string-append ,gsl "/yet/another"))
$2 = (begin (string-append #<package gsl@2.7.1 gnu/packages/maths.scm:679 77da2800db00> "/yet/another"))
\end{lstlisting}

\vfill
\begin{center}
    \textbf{What is the result of}
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> (eval $2 (interaction-environment))
\end{lstlisting}
?
\end{center}
\end{exampleblock}
\end{frame}


\begin{frame}<presentation>[fragile]%
  {Intuition IV}
\vspace{-0.25cm}
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> `(begin (string-append ,gsl "/yet/another"))
$2 = (begin (string-append #<package gsl@2.7.1 gnu/packages/maths.scm:679 77da2800db00> "/yet/another"))
\end{lstlisting}

\vspace{-0.25cm}
\begin{exampleblock}{}
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> (eval $2 (interaction-environment))

ice-9/boot-9.scm:1685:16: In procedure raise-exception:
In procedure string-append: Wrong type (expecting string): #<package gsl@2.7.1 gnu/packages/maths.scm:679 77da2800db00>

Entering a new prompt.  Type `,bt' for a backtrace or `,q' to continue.
scheme@(guix-user) [1]>
\end{lstlisting}
\end{exampleblock}

\vfill
\visible<2>{
\begin{alertblock}{}
  \begin{center}
    \textbf{it is an error!}
  \end{center}

  Because \texttt{gsl} is not a string.
\end{alertblock}
}
\end{frame}


\begin{frame}<presentation>[fragile]%
  {Intuition V}

  \vfill
  \begin{lstlisting}[language=Lisp]
scheme@(guix-user)> `(begin (string-append ,gsl "/yet/another"))
\end{lstlisting}

\vfill

\begin{alertblock}{}
\begin{center}
  \begin{tabular}{ccc}
    \texttt{\`{}} &replaced by& \texttt{\#\~{}}
    \\
    \texttt{,} &replaced by& \texttt{\#\${}}
  \end{tabular}
\end{center}
\end{alertblock}

\vfill
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> #~(begin (string-append #$gsl "/yet/another"))
$4 = #<gexp (begin (string-append #<gexp-input #<package gsl@2.7.1 gnu/packages/maths.scm:679 77da2800db00>:out> "/yet/another")) 77da24e66ed0>
\end{lstlisting}
\end{frame}


\begin{frame}<presentation>[fragile]%
  {Intuition VI}
  \begin{exampleblock}{}
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> #~(begin (string-append #$gsl "/yet/another"))
$4 = #<gexp (begin (string-append #<gexp-input #<package gsl@2.7.1 gnu/packages/maths.scm:679 77da2800db00>:out> "/yet/another")) 77da24e66ed0>
\end{lstlisting}
  \end{exampleblock}

\vfill
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> (define gexp->sexp (@@ (guix gexp) gexp->sexp))
scheme@(guix-user)> (gexp->sexp $4 "x86_64-linux" #f)
$5 = #<procedure 77da24f88e40 at guix/gexp.scm:1408:2 (state)>
\end{lstlisting}


\vfill
\begin{block}{}
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> ,run-in-store (gexp->sexp $4 "x86_64-linux" #f)
$6 = (begin
  (string-append
    "/gnu/store/dzx94b3xv4h1ik1bfrbxaw7n84y9r8zz-gsl-2.7.1"
    "/yet/another"))
  \end{lstlisting}
\end{block}
\end{frame}


\begin{center}
  \href{https://simon.tournier.info/posts/2023-11-02-gexp-intuition.html}%
  {https://simon.tournier.info/posts/2023-11-02-gexp-intuition.html}
\end{center}


\begin{frame}<presentation>[fragile, label=g-expression-2]%
  {Example}
  \begin{exampleblock}{}
    \begin{lstlisting}[language=Lisp]
#:phases
#~(modify-phases %standard-phases
[...]
    (replace 'install
      (lambda _
        (mkdir-p (string-append #$output "/bin"))
        (chmod "BQN" #o755)
        (rename-file "BQN" "bqn")
        (install-file "bqn" (string-append #$output "/bin"))))
\end{lstlisting}
\end{exampleblock}
\vfill
\begin{flushright}
  package \verb+cbqn-bootstrap+
\end{flushright}
\end{frame}


\begin{frame}<presentation>[fragile, label=g-expression-2]%
  {Example II}
  \vspace{-0.3cm}
  \begin{exampleblock}{}
    \begin{lstlisting}[language=Lisp]
#:phases
#~(modify-phases %standard-phases
    (delete 'configure)
    (add-before 'build 'generate-bytecode
      (lambda _
        (system (string-append #+dbqn
                               "/bin/dbqn ./genRuntime "
                               #+bqn-sources))))
[...]
(native-inputs (list dbqn bqn-sources))
(inputs (list icedtea-8 libffi))
\end{lstlisting}
  \end{exampleblock}

  \texttt{\#+} plays the same role as \texttt{\#\${}},
  but is a reference to a native package build

  \hfill (cross-compilation context)
\end{frame}


\section{Questions}


\begin{frame}<presentation>[fragile]{Ready?}
\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> `(begin (string-append ,gsl-name "/yet/another"))
\end{lstlisting}

\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> `(begin ,(string-append gsl-name "/yet/another"))
\end{lstlisting}

\begin{lstlisting}[language=Lisp]
scheme@(guix-user)> #~(begin #$(string-append gsl "/yet/another"))
\end{lstlisting}

\vfill
\begin{alertblock}{}
  \begin{center}
    What happens?
  \end{center}
\end{alertblock}

\vfill
\begin{flushright}
  see \texttt{file-append}
\end{flushright}
\end{frame}


\begin{frame}<presentation>[fragile, label=resource]{Resources (links)}
  \begin{description}
  \item[Talk] «
    \href{https://10years.guix.gnu.org/video/guixy-guile-the-derivation-factory-a-tour-of-the-guix-source-tree/}%
    {A tour of the Guix source tree} » (video 40min)
  \item[Talk] «
    \href{https://archive.fosdem.org/2020/schedule/event/gexpressionsguile/}%
    {Introduction to G-Expressions} » (video 30min)

    \vfill
    \begin{center}
      \rule{0.5\textwidth}{0.4pt}

      \textbf{self-promotion}

      \href{https://simon.tournier.info/posts/}%
      {\texttt{https://simon.tournier.info/posts/}}
    \end{center}
    \vfill

  \item[Post] «
    \href{https://simon.tournier.info/posts/2022-11-28-guile-dual-numbers.html}%
    {Derivative, dual numbers and Guile} »
  \item[Post] « \href{https://simon.tournier.info/posts/2023-11-02-gexp-intuition.html}%
    {Guix: an intuition about G-expression} »
  \item[Post] « \href{https://simon.tournier.info/posts/2023-11-01-gexp.html}%
    {Guix: Quasiquote and G-expression} »

    \hfill illustration of reference tracking by G-exp using Fibonacci numbers
  \end{description}
\end{frame}



\begin{frame}<presentation>[label=thanks]{Packaging = practise and practise again}
  If I might,
  \medskip
  \begin{enumerate}
  \item Dive into existing packages and deal with Guix manual and community.
  \item Most of the ``tricks'' is about \uline{\textbf{a lot of}} practise.
    Quoting \texttt{rekado},

    \vfill
    \begin{flushright}
      I wish I had anything to say about this other than:

      \emph{“try again, give up, forget about it, remember it, ask for
        pointers, repeat”}

      \href{https://logs.guix.gnu.org/guix-hpc/2023-10-13.log\#142159}%
      {\textsf{\texttt{\#guix-hpc} on 2023-10-13}}.
    \end{flushright}
  \end{enumerate}
  \vfill
  \begin{center}
    \uline{\emph{do not forget that}~\textbf{packaging is a craft}}
  \end{center}
\end{frame}


\end{document}
