(source
 (origin
   (method url-fetch)
   (uri (string-append "https://www.python.org/ftp/python/"
                       version "/Python-" version ".tar.xz"))
   (patches (search-patches ...))
   (sha256
    (base32
     "09vd7g71i11iz5ydqghwc8kaxr0vgji94hhwwnj77h3kll28r0h6"))))
