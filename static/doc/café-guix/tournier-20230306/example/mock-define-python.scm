(define python                  ;definition of the node python
  (package
    (name "python")
    (version "3.9.9")
    (source ... )                   ;points to URI source code
    (build-system gnu-build-system) ;./configure & make
    (arguments ... )                ; configure flags, etc.
    (inputs (list bzip2             ;other nodes -> graph (DAG)
                  expat gdbm libffi sqlite ...))))
