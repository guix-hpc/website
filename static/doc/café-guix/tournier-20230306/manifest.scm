(specifications->manifest
 (list
  "rubber"

  "texlive-base"
  "texlive-fonts-ec"
  "texlive-kpfonts"
  "texlive-cm-super"
  "texlive-amsfonts"

  "texlive-beamer"
  "texlive-translator"
  "texlive-ulem"
  "texlive-capt-of"
  "texlive-hyperref"
  "texlive-carlisle"

  "texlive-wrapfig"
  "texlive-amsmath"
  "texlive-listings"
  "texlive-pgf"

  "texlive-latex-geometry"
  "texlive-latex-textpos"
  "texlive-babel-french"
  "texlive-latex-fancyvrb"
  "texlive-latex-fancyhdr"
  ))
