(use-modules (guix transformations))

(define transform
  (options->transformation
    '((with-c-toolchain . "python=gcc-toolchain@10.5.0"))))

(packages->manifest
 (map (compose transform specification->package)
      (list "python" "python-numpy")))
