(use-modules (gnu)))
(use-package-modules python python-xyz)

(operating-system
(host-name "komputilo")
(timezone "Europe/Paris")
(locale "fr_FR.utf8")
(keyboard-layout (keyboard-layout "gb"))

(users (append (list (user-account
                      (name "alice")
                      (group "users")))
               %base-user-accounts))

(packages (append (list python python-numpy)
                  %base-packages)))
