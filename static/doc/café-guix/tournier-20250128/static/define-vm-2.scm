(define fichiers
  (list "/root/plot"))

(operating-system
  ...
  (services (cons (simple-service 'rotate-mes-trucs
                                  rottlog-service-type
                                  (list (log-rotation
                                         (frequency 'daily)
                                         (files fichiers))))
                  %base-services)))
