(define python                      ;package definition
  (package
    (name "python")
    (version "3.9.9")
    (source ... )                   ;package source
    (build-system gnu-build-system)
    (arguments ... )
    (inputs (list ...))))
