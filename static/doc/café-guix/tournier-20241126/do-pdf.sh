#!/usr/bin/env bash

guix time-machine -C channels.scm                \
     -- shell --container -m manifest.scm        \
     -- rubber --pdf tournier-20241126.tex
