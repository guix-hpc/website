# Transformations de paquets pour utilisateurices HPC exigeant·es

Les personnes qui travaillent en calcul intensif (HPC) ont la réputation
d’être exigeantes dès qu’il s’agit de flexibilité sur la manière de
construire les logiciels : on veut pouvoir combiner des versions
choisies, activer ou non certaines options de compilation, ajouter ou
non certaines dépendances optionnelles, etc.  Spack est connu pour
répondre à ces besoins en ligne de commande.

Dans cet esprit, mais dans un cadre où on a reproducibilité et
traçabilité par défaut, les options de transformations de paquets de
Guix permettent d’ajuster les logiciels à ses besoins depuis la ligne de
commande.  Nous verrons les principales transformations, du changement
de dépendances avec `--with-input` à l’optimisation pour une
micro-architecture avec `--tune`, en passant par le choix de révisions
spécifiques avec `--with-commit`.  Et bien sûr, ce sera l’occasion de
répondre à vos interrogations en la matière !

## beaucoup d'options !

```
guix build --help-transform
```

## changer les dépendances

### `--with-input`

```
guix build intel-mpi-benchmarks --with-input=openmpi=mpich
```

### visualiser avec `guix graph`

```
guix graph intel-mpi-benchmarks --with-input=openmpi=mpich |xdot -
```

### quand on sait ce qu'on fait : `--with-graft`

```
guix build intel-mpi-benchmarks --with-graft=slurm@22=slurm@20
```

## compiler différemment

### `--tune` : optimiser pour une micro-architecture

```
guix shell --tune eigen-benchmarks -- \
    benchBlasGemm 240 240 240
```

ou encore:

```
guix install --tune gromacs
```

### sauvegarder le résultat dans un manifeste

```
guix shell --tune eigen-benchmarks --export-manifest
```

### `--without-tests` : sauter la suite de tests

```
guix shell python python-numpy \
  --with-latest=openblas --without-tests=openblas -- \
  python3
```

### `--with-debug-info` : conserver les infos de débuggage

### `--with-configure-flag` : changer les drapeaux de compilation

```
guix build lapack \
  --with-configure-flag=lapack=-DBUILD_SHARED_LIBS=OFF
```

### `--with-c-toolchain` : changer de chaîne de compilation

```
guix build eigen-benchmarks \
  --with-c-toolchain=eigen-benchmarks=clang-toolchain
```

## changer le source d'un paquet

### `--with-source`

### `--with-patch`

### `--with-git-url`

### `--with-commit`

### `--with-latest`

```
guix shell python python-numpy \
  --with-latest=openblas --without-tests=openblas -- \
  python3
```

### `--with-version`

```
guix build eigen --with-version=eigen=3.2.0
```

## l'ordre des options !

```
guix build intel-mpi-benchmarks \
  --with-input=openmpi=mpich \
  --with-version=mpich=3.1.2
```
