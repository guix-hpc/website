# Ressources

 - [Manuel de Guile en ligne](https://www.gnu.org/software/guile/manual/guile.html)
 - [Liste de bibliothèques externes sur le site de Guile](https://www.gnu.org/software/guile/libraries/)
 - [Dépôt `awesome-guix` sur Github](https://github.com/franzos/awesome-guix)
 - [Section sur les paires pointées dans la documentation d'Emacs](https://www.gnu.org/software/emacs/manual/html_node/elisp/Dotted-Pair-Notation.html)
 - [Dépôt `nonguix` contenant des définitions de paquets pour des logiciels généralistes non libres ou non compilés depuis le source](https://github.com/nonguix)
 - [Dépôt contenant des définitions de paquets pour des logiciels scientifiques non libres ou non compilés depuis le source](https://codeberg.org/guix-science/guix-science-nonfree)
 - [Article de blog à propos de la compilation intégrale d'un système à partir du code source](https://guix.gnu.org/en/blog/2023/the-full-source-bootstrap-building-from-source-all-the-way-down/)
 - [Article de blog à propos de la compilation intégrale d'un système à partir du code source](https://simon.tournier.info/posts/2023-10-01-bootstrapping.html)
 - [Comparaison entre Guix et Nix](https://lab.abilian.com/Tech/Linux/Packaging/Guix%20vs.%20Nix/)
