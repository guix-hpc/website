(+ 1 2)
(* (+ (1 2) 3))
(list 'one 2 "three")
'one
'(list 'one 2 "three")

(define (add-plus-2 x y)
  (let ((two 2)
        (x+y (+ x y)))
    (+ x+y two)))

(define (add-plus-2-bis x y)
  (let* ((x+two (+ x two))
         (two 2)
         (result (+ y x+two)))
    result))

(define (add-plus-2-bis x y)
  (let ((two 2))
    (let ((x+two (+ x two)))
      (let ((result (+ y x+two)))
        result))))


(number? 42)
(number? "not a number")
(string? 42)
(string? "not a number")
(string-prefix? "hello" "hello-world")
(string-prefix? "bye" "hello-world")

(define* (add-plus-something x y #:optional (value 2))
  (+ x y value))

(define (if-then-else predicate then else)
  (if predicate
      then
      else))

(if-then-else #f
              (+1 "failure")
              'else)

(define-macro (if-then-else predicate then else)
  `(if ,predicate
       ,then
       ,else))

(define-record-type <pkg>               ;name, convention <>
  (pkg name version)                    ;constructor
  pkg?                                  ;predicate
  (name pkg-name)                       ;accessor
  (version pkg-version))

(pkg "hello" "1.2")
(define hi (pkg "hello" "1.2"))
(pkg? hi)
(pkg-name hi)
(pkg-version hi)
