\documentclass[aspectratio=169,ignorenonframetext,presentation]{beamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[french]{babel}
\usepackage{fancyvrb}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing, positioning, arrows.meta}

\newcommand{\tikzmark}[2]{%
  \tikz[remember picture,baseline=(#1.base)]%
  {\node[inner sep=0pt] (#1) {#2 \quad};}}

\newcommand{\tikzbrace}[3]{%
\begin{tikzpicture}[remember picture,overlay]
  \draw[decorate,decoration={brace}] (#1.north east) -- node[right]
  { % XXXX: hfill
    #3}
  (#1.north east |- #2.south east);
\end{tikzpicture}}


\usetheme{Madrid}
\useoutertheme[subsection=true]{smoothbars}

\makeatletter
\usecolortheme{whale}
\usecolortheme{orchid}
\setbeamerfont{block title}{size={}}
\setbeamertemplate{navigation symbols}{}
\setbeamertemplate{itemize items}[triangle]
\makeatletter
\setbeamertemplate{footline}
{
  \leavevmode%
  \hbox{%
    \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,left]{author in head/foot}%
      \usebeamerfont{author in head/foot}\quad S. Tournier
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.7\paperwidth,ht=2.25ex,dp=1ex,center]{title in head/foot}%
      \usebeamerfont{title in head/foot}\insertshorttitle
    \end{beamercolorbox}%
    \begin{beamercolorbox}[wd=.15\paperwidth,ht=2.25ex,dp=1ex,right]{date in head/foot}%
      \insertframenumber{} / \inserttotalframenumber\hspace*{2ex}
    \end{beamercolorbox}}%
  \vskip0pt%
}
\makeatother

\title[Reproductibilité des environnements logiciels avec GNU Guix]%
{Comment refaire plus tard et là-bas\\ce qui a été fait aujourd’hui et ici ?}
\author{Simon Tournier}
\date{15 avril 2022}
\institute{INSERM US 53 - CNRS UAR 2030 \\ \texttt{simon.tournier@u-paris.fr}}
\hypersetup{
  pdfauthor={Simon Tournier},
  pdftitle={Reproductibilité des environnements logiciels avec GNU Guix},
  pdfkeywords={GNU Guix, reproductibilité, déploiement logiciels, gestionnaire de paquets, machine virtuelle},
  pdfsubject={tutoriel court de l'outil GNU Guix},
  pdfcreator={with love},
  pdflang={French}}


\newcommand{\violet}{\textcolor{violet}}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\magenta}{\textcolor{magenta}}
\newcommand{\cyan}{\textcolor{cyan}}
\newcommand{\green}{\textcolor{green}}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\newcommand{\mauve}{\textcolor{mauve}}

\input{listings-scheme}

\newcommand{\someguile}{\textcolor{someguile}}
\newcommand{\somestring}{\textcolor{somestring}}
\newcommand{\somevariable}{\textcolor{somevariable}}


\begin{document}


\begin{frame}[plain, fragile, noframenumbering]{}
  \vspace{5mm}
  \titlepage

  \vfill{}
  \vspace{-12mm}
  \begin{center}
    \href{https://hpc.guix.info}{\texttt{https://hpc.guix.info}}
  \end{center}
  \vfill{}
  \normalsize
  \begin{minipage}{0.2\paperwidth}
    \includegraphics[width=0.2\paperwidth]{logo-café}
  \end{minipage}
  \begin{flushright}
    \begin{minipage}{0.2\paperwidth}
      \vspace{-20mm}
      \includegraphics[width=0.2\paperwidth]{logo-u-paris}
    \end{minipage}
  \end{flushright}
\end{frame}

\begin{frame}[fragile]{Le monde est \emph{open}, n'est-ce pas ?}

  \begin{itemize}
  \item \emph{open} journal
  \item \emph{open} data
  \item \emph{open} source
  \item \emph{open} science
  \item \emph{open} etc.
  \end{itemize}

  \begin{alertblock}{}
    \begin{center}
      Quel est le problème de reproductibilité dans un contexte scientifique ?

      (même si tout devient \emph{open})
    \end{center}
  \end{alertblock}

\end{frame}



\begin{frame}[fragile]{Exemple}
  \begin{lstlisting}[language=C, caption={Fonction de Bessel}, captionpos=t]{Name=open-source}
    #include <stdio.h>
    #include <math.h>

    int main(){
      printf("%E\n", j0f(0x1.33d152p+1f));
    }
  \end{lstlisting}

  \begin{center}
    \begin{tabular}{lcl}
      Alice &voit :& 5.\blue{64}34\blue{4}0E-08 % #no ptions
      \\
      Carole &voit :& 5.\red{96}34\red{3}0E-08 % -lm -fno-builtin
    \end{tabular}
  \end{center}

  \begin{alertblock}{}
    \begin{center}
      Pourquoi ? Le code est disponible pourtant.
    \end{center}
  \end{alertblock}

  \small{%
    Établir si le différence est significative ou non est laissé à l'expertise
    des scientifiques du domaine.
  }
\end{frame}


\begin{frame}[fragile]{Enjeu de la recherche reproductible}
  \begin{exampleblock}{D'un point de vue la « méthode scientifique » :}
  \begin{center}
    {Tout l'enjeu est le \uline{\textbf{contrôle de la variabilité}}}
  \end{center}
  \end{exampleblock}

  \vfill

  \begin{exampleblock}{D'un point de vue du « savoir scientifique » (caractère
      universel) :}
    \begin{itemize}
    \item Un observateur indépendant doit être capable d'observer le même
      résultat.
    \item L'observation doit être pérenne (dans une certaine mesure).
    \end{itemize}
  \end{exampleblock}

  \vfill

  \begin{alertblock}{}
    Dans un monde où (presque) tout est donnée numérique (\emph{data}),
    \begin{center}
      \textbf{comment refaire plus tard et là-bas ce qui a été fait aujourd’hui et ici ?}
    \end{center}
    \begin{flushright}
      (sous entendu avec un « ordinateur »)
    \end{flushright}
  \end{alertblock}
\end{frame}

\begin{frame}[fragile]{Nous allons discuter de…}
  \tableofcontents
  \vfill
  \begin{center}
    \small
    (quelques exemples issus du langage \texttt{C} mais tout est
    transposable à n’importe quel autre pile logicielle)
  \end{center}
\end{frame}

\section{Du problème d’Alice et Carole}

\subsection{Gestionnaire de paquets}


\begin{frame}[fragile]{Quelques questions}
  \begin{lstlisting}[basicstyle=\tiny, language=C]{Name=open-source}
    #include <stdio.h>
    #include <math.h>

    int main(){
      printf("%E\n", j0f(0x1.33d152p+1f));
    }
  \end{lstlisting}

  \begin{center}
    \begin{tabular}{lcl}
      Alice &voit :& 5.\blue{64}34\blue{4}0E-08 % #no options
      \\
      Carole &voit :& 5.\red{96}34\red{3}0E-08 % -lm -fno-builtin
    \end{tabular}
  \end{center}

  \begin{exampleblock}{}
    \begin{itemize}
    \item Quel compilateur ?
    \item Quelles bibliothèques (\texttt{<math.h>}) ?
    \item Quelles versions ?
    \item Quelles options de compilation ?
    \end{itemize}
  \end{exampleblock}
\end{frame}



\begin{frame}[fragile]{En d’autres termes}
  \begin{itemize}
  \item Quelles sont les sources des outils ?
  \item Quelles sont les outils requis pour la construction ?
  \item Quelles sont les outils requis pour l’exécution ?
  \item Comment chaque outil est-il produit  ?
  \end{itemize}

  \begin{exampleblock}{}
    \begin{center}
      Répondre à ces questions signifie \textbf{contrôler la variabilité} de
      l’environnement computationnel
    \end{center}
  \end{exampleblock}

  \vfill

  \begin{alertblock}{}
    \begin{center}
      Comment capturer ces informations ?
    \end{center}
  \end{alertblock}
\end{frame}



\begin{frame}[fragile]{Alice dit « GCC à la version 11.2.0 »}
  \vspace{-0.1cm}
  \includegraphics[width=\textwidth]{graph.png}
  \vfill{}
  \begin{alertblock}{}
    \begin{center}
      Est-ce la même version de GCC si \texttt{mpfr} est à la version 4.0 ?
    \end{center}
  \end{alertblock}
  \vfill
  \begin{flushright}
    Graphe complet : 43 ou 104 ou 125 ou 218 n\oe uds

    \small
    (suivant ce que l’on considère comme graine binaire du \emph{bootstrap})
  \end{flushright}
\end{frame}



\begin{frame}[fragile]{Options de constructions (compilation)}
  \begin{lstlisting}[basicstyle=\tiny, language=C]{Name=open-source}
    #include <stdio.h>
    #include <math.h>

    int main(){
      printf("%E\n", j0f(0x1.33d152p+1f));
    }
  \end{lstlisting}

  \vfill

  \begin{exampleblock}{}
    \begin{center}
      \begin{tabular}{rlll}
        \blue{\texttt{alice@laptop\$}} & \texttt{gcc bessel.c}
        && \texttt{\&\& ./a.out}
        \\
                                       & \texttt{5.643440E-08}
        \\
        \red{\texttt{carole@desktop\$}} & \texttt{gcc bessel.c}
        & \texttt{\mauve{-lm -fno-builtin}}
                                       & \texttt{\&\& ./a.out}
        \\
                                       & \texttt{5.963430E-08}
      \end{tabular}
    \end{center}
  \end{exampleblock}

  \begin{flushright}
    \small{(Ah, sacré \emph{constant folding})}
  \end{flushright}

  \begin{alertblock}{}
    \begin{center}
      Il faut capturer comment \textbf{tous} les outils (n\oe uds du graphe)
      sont produits.
    \end{center}
  \end{alertblock}
\end{frame}



\begin{frame}[fragile]{Gestionnaire de paquets = gestionnaire de graphe}
  \vspace{-0.1cm}
  \begin{alertblock}{}
    \begin{center}
      Comment capturer ces informations ?
    \end{center}
  \end{alertblock}

    \begin{itemize}
    \item Quelles sont les sources des outils ?
      \hfill \texttt{source}
  \item \tikzmark{A}{Quelles sont les outils requis pour la construction ?}
  \item \tikzmark{B}{Quelles sont les outils requis pour l’exécution ?}
  \item Comment chaque outil est-il produit  ?
    \hfill \texttt{build-system}, \texttt{arguments}
  \end{itemize}
  \tikzbrace{A}{B}{\texttt{inputs}, \texttt{propagated-}, \texttt{native-inputs}}

  \vspace{-0.5cm}
  \begin{exampleblock}{}
    \vspace{-0.3cm}
    \lstinputlisting[language=Scheme,columns=space-flexible]{mock-define-pkg.scm}
  \end{exampleblock}
\end{frame}

\subsection{Guix dans tout ça}


\begin{frame}[fragile]{Révision = un graphe spécifique}
  \begin{alertblock}{}
    \begin{center}
      version = graphe
    \end{center}
  \end{alertblock}

\begin{verbatim}
$ guix describe
Generation 77	Apr 05 2022 10:27:45	(current)
  guix 20303c0
    repository URL: https://git.savannah.gnu.org/git/guix.git
    branch: master
    commit: 20303c0b1c75bc4770cdfa8b8c6b33fd6e77c168
\end{verbatim}

  \begin{alertblock}{}
    \begin{center}
      La révision \texttt{20303c0} capture \textbf{tout} le graphe
    \end{center}
  \end{alertblock}

  \begin{exampleblock}{}
    \begin{itemize}
    \item Alice dit « j’ai utilisé Guix à la révision \texttt{20303c0} »
    \item Carole connaît toutes les informations pour reproduire le même environnement
    \end{itemize}
  \end{exampleblock}
\end{frame}



\begin{frame}[fragile]{Paquet spécifique et canal (\emph{channels})}
  Alice développe un outil dont les sources sont à :
  \begin{center}
    \texttt{https://gitlab.inria.fr/projet/un-outil.git}
  \end{center}
  et l’empaquette elle-même via un canal :
    \begin{center}
    \texttt{https://gitlab.inria.fr/projet/un-canal.git}
  \end{center}

  \vfill

  \begin{exampleblock}{}
  Le dépôt Git \texttt{un-canal.git} capture les informations \\
  \hfill pour construire l’outil à partir des sources \texttt{un-outil.git}.
  \end{exampleblock}

\end{frame}



\begin{frame}[fragile]{Collaboration}
  \begin{description}
  \item[Alice] décrit son environnement :
    \begin{itemize}
    \item la révision (Guix lui-même et aussi potentiellement les autres canaux):
      \begin{center}
        \texttt{guix describe -f channels > alice.scm}
      \end{center}
    \item la liste des paquets avec le fichier \texttt{paquets.scm}
    \end{itemize}
    génère son environnement avec, e.g.,
    \begin{center}
      \texttt{guix shell -m paquets.scm}
    \end{center}
    et \uline{\textbf{partage ses deux fichiers}}.
  \item[Carole] génère le même environnement \uline{\textbf{à partir des deux fichiers}} d’Alice,
    \begin{center}
      \texttt{guix time-machine -C alice.scm -{}- shell -m paquets.scm}
    \end{center}
  \item[Dan] peut donc aussi avoir le même environnement qu’Alice et Carole.
  \end{description}
\end{frame}



\begin{frame}[label=time-machine, fragile]{Changer de révision,  en arrière, en avant : \texttt{guix time-machine}}
  \vspace{-1cm}
  \begin{center}
    \begin{tikzpicture}
      % draw horizontal line
      \draw[thick, -Triangle] (0,0) -- (12,0) node[font=\scriptsize,below left=3pt and -8pt]{time};

      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] at (0,-.3) {2018};

      % draw vertical lines
      \foreach \x in {1,...,11}
      \draw (\x cm,3pt) -- (\x cm,-3pt);

      \foreach \x/\descr in {3/Dan,6/Alice,8/Carole}
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] at (\x,-.3) {$\descr$};

      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Dan) at (3,+.3)  {d7e57e};
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Alice) at (6,+.3)  {20303c0};
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Carole) at (8,+.3)  {3d059c};

      \draw[->] (Dan.north) to [out=50,in=150] (Alice.west);
      \draw[->] (Carole.north) to [out=100,in=50] (Alice.east);

      \draw[lightgray!0!red, line width=4pt] (2,-.6) -- +(8,0);
      \foreach \x/\percol in {1/75,9/75}
      \draw[lightgray!\percol!red, line width=4pt] (\x,-.6) -- +(1,0);

      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] at (11,-.3)  {$Vic$};
      \node[font=\scriptsize, text height=1.75ex, text depth=.5ex] (Vic) at (11,+.3)  {0dc00f};
      \draw[dashed,->] (Vic.north) to [out=40,in=90] (Alice.north);

    \end{tikzpicture}
  \end{center}

  \begin{exampleblock}{}
    Pour être \uline{reproductible dans le temps}, il faut :
    \begin{itemize}
    \item Une préservation de \textbf{tous} les codes source~%
      % \small{%
      %   (\href{https://ngyro.com/pog-reports/latest/}%
      %   {$\approx 75\%$ archivés \tiny{(lien)}\small} dans~%
      %   \href{https://www.softwareheritage.org/}%
      %   {Software Heritage \tiny{(lien)}\small})%
      % }\normalsize
    \item Une \emph{backward} compatibilité du noyau Linux
    \item Une compatibilité de l'\emph{hardware} (e.g., CPU, disque dur \tiny{(NVMe)}\normalsize, etc.)
    \end{itemize}
  \end{exampleblock}

  \begin{alertblock}{}
    \begin{center}
      Quelle est la taille de la fenêtre temporelle avec les 3 conditions
      satisfaites ?
    \end{center}
  \end{alertblock}
  \begin{center}
    \scriptsize{(À ma connaissance, le projet Guix réalise une expérimentation
      grandeur nature et quasi-unique depuis sa v1.0 en 2019.)}
  \end{center}
\end{frame}

\section{De pérennité}
\subsection{Préservation de quoi et pourquoi ?}


\begin{frame}[fragile]{comment refaire plus tard et là-bas ce qui a été fait aujourd’hui et ici ?}
  \begin{alertblock}{}
    \begin{center}
      \textbf{traçabilité et transparence}
    \end{center}
  \end{alertblock}
  \begin{center}
    \emph{être capable d’étudier bogue-à-bogue}
  \end{center}

  \begin{exampleblock}{}
    Guix devrait s’occuper de tout
    \begin{center}
      \texttt{guix time-machine -C channels.scm -{}- shell -m manifest.scm}
    \end{center}
    si on spécifie
    \begin{description}
    \item[« comment construire »] \hfill \texttt{channels.scm}
    \item[« quoi construire »] \hfill \texttt{manifest.scm}
    \end{description}
  \end{exampleblock}

  \onslide<2->{
  \begin{center}
    Que faut-il préserver en plus de ces 2 fichiers ?
  \end{center}
  }
\end{frame}



\begin{frame}[fragile]{Préservation de quoi ?}
  \begin{center}
    \texttt{guix time-machine -C channels.scm -{}- shell -m manifest.scm}
  \end{center}
  \begin{itemize}
  \item chaque canal (= dépôt Git qui contient des définitions de paquet)
    \hfill \texttt{channels.scm}
  \item chaque source du paquet
    \hfill \texttt{manifest.scm}
    \begin{exampleblock}{}
      \vspace{-0.3cm}
      \lstinputlisting[language=Scheme,columns=space-flexible]{mock-define-pkg-source.scm}
    \end{exampleblock}
  \end{itemize}
\end{frame}



\begin{frame}[fragile]{Préservation de quoi ? (2)}
  \vspace{-0.29cm}
  \begin{exampleblock}{}
    \begin{itemize}
    \item dépôt Git \hfill (canal)
    \item \texttt{source}
      \begin{itemize}
      \item archive \emph{tarballs} (compressées) \hfill \texttt{url-fetch}
      \item dépôt Git \hfill \texttt{git-fetch}
      \item dépôt Subversion \hfill \texttt{svn-fetch}
      \item dépôt Mercurial \hfill \texttt{hg-fetch}
      \item dépôt CVS \hfill \texttt{cvs-fetch}
      \end{itemize}
    \end{itemize}
  \end{exampleblock}

  \onslide<2->{
    \begin{center}
      \texttt{\$ guix repl -{}- sources.scm | sort | uniq -c | sort -nr}\\
      \begin{tabular}{rl}
        \texttt{13432} & \texttt{url-fetch}\\
        \texttt{6691}  & \texttt{git-fetch}\\
        \texttt{391}   & \texttt{svn-fetch}\\
        \texttt{43}    & \texttt{other}\\
        \texttt{31}    & \texttt{hg-fetch}\\
        \texttt{3}     & \texttt{cvs-fetch}\\
      \end{tabular}
    \end{center}
  }
\end{frame}



\begin{frame}[fragile]{Pourquoi les préserver ?}
  \vspace{-0.25cm}
  \begin{exampleblock}{}
    Parce que les \textbf{services en ligne parfois s’arrêtent}
    \begin{itemize}
    \item \href{https://code.google.com/archive/}{Google Code (lien)} début 2016
    \item Alioth (Debian) en 2018 remplacé par
      \href{https://salsa.debian.org/public}{Salsa}
    \item Gna! en 2017 après 13 années d’activité
    \item Gitourious en 2015 (le second plus populaire service
      d’hébergement pour Git en 2011)
    \item etc.
    \item<2-> \texttt{gforge.inria.fr}
      par exemple \href{https://issues.guix.gnu.org/42162}{Guix issue \#42162 (lien)}
\begin{verbatim}
Believe it or not, gforge.inria.fr was finally phased out on
Sept. 30th.  And believe it or not, despite all the work and all the
chat :-), we lost the source tarball of Scotch 6.1.1 for a short period
of time (I found a copy and uploaded it to berlin a couple of hours
ago).
\end{verbatim}
    \end{itemize}
  \end{exampleblock}
\end{frame}


\subsection{Archivage logiciel : Software Heritage}


\begin{frame}[fragile]{Comment les préserver ?}
  \begin{alertblock}{}
    \begin{center}
      Forge $\neq$ Archive
    \end{center}
  \end{alertblock}

  \begin{flushright}
    L'objectif d'une forge est de permettre à plusieurs développeurs de
    \textbf{participer ensemble au développement} d'un ou plusieurs logiciels,
    le plus souvent à travers le réseau Internet.

    \medskip

    \href{https://fr.wikipedia.org/w/index.php?title=Forge_(informatique)&oldid=192671997}%
    {https://fr.wikipedia.org/wiki/Forge\_(informatique)}
  \end{flushright}

  \vfill

  \begin{flushright}
    L'archivage est un ensemble d'actions qui a pour but de garantir
    l'accessibilité sur le long terme d'informations (dossiers, documents,
    données) que l'on doit ou souhaite conserver pour des raisons juridiques

    \medskip

    \href{https://fr.wikipedia.org/w/index.php?title=Archivage&oldid=186832550}
    {https://fr.wikipedia.org/wiki/Archivage}
  \end{flushright}


  \begin{exampleblock}{}
    \begin{center}
      \href{https://www.softwareheritage.org/}%
      {Software Heritage \emph{« are building the universal software archive »} (lien)}
    \end{center}
  \end{exampleblock}
\end{frame}



\begin{frame}[fragile]{Les services en ligne parfois s’arrêtent\dots}
  \begin{center}
    Pourquoi serait-il différent pour Software Heritage ?
  \end{center}

  Aucune garantie mais…

  \begin{flushright}
    Software Heritage is an open, non-profit initiative unveiled in 2016 by
    Inria. It is supported by a broad panel of institutional and industry
    partners, in collaboration with UNESCO.

    \medskip

    The long term goal is to collect all publicly available software in source
    code form together with its development history, replicate it massively to
    ensure its preservation, and share it with everyone who needs it.
  \end{flushright}

  \begin{exampleblock}{}
    \begin{itemize}
    \item Supporté par des institutions
    \item Avec la mission d’archiver
    \end{itemize}
  \end{exampleblock}

  \begin{center}
    \small{(demo du site SWH)}
  \end{center}

\end{frame}

\subsection{Guix dans tout ça}


\begin{frame}[fragile]{État des lieux de Guix et Software Heritage (SWH)}

  \begin{alertblock}{Guix est capable de}
    \begin{itemize}
    \item sauvegarder \hfill (automatiquement) les sources dans SWH
    \item retrouver \hfill (automatiquement) les sources depuis SWH
    \end{itemize}
  \end{alertblock}
  \begin{exampleblock}{}
    \begin{center}
      Mais pas tous les types de sources !
    \end{center}
  \end{exampleblock}

  \vfill

  \begin{itemize}
  \item Des outils disponibles dans Guix pour les sources :
    \begin{itemize}
    \item dépôt Git \hfill \texttt{git-fetch}
    \item archives \emph{tarballs} (compressées) \hfill \texttt{url-fetch}
    \end{itemize}
  \item \dots et manquants pour tout le reste \hfill (aide bienvenue :-))
  \end{itemize}
\end{frame}



\begin{frame}[fragile]{Quelques stats : préservation de Guix}
  \vspace{-0.25cm}
  \begin{center}
    \href{https://ngyro.com/pog-reports/latest/}{https://ngyro.com/pog-reports/latest/}

    \begin{tabular}{cc}
      \begin{minipage}{0.6\linewidth}
        \includegraphics[width=0.95\textwidth]{all-abs-hist.png}
      \end{minipage}
      &
        \begin{minipage}{0.4\linewidth}
          Sauvegarder dans Software Heritage :
          \begin{itemize}
          \item Git = 98.3\%
          \item \emph{tarballs} $\approx$ 70\%
          \end{itemize}
        \end{minipage}
    \end{tabular}
  \end{center}
\end{frame}



\begin{frame}[fragile]{En pratique}
  \begin{itemize}
  \item Sauvegarder un paquet
    \begin{center}
      \texttt{guix lint -c archival nom-du-paquet}
    \end{center}
  \item Retrouver la source d’un paquet (si elle a « disparu » )
    \begin{center}
      \sout{\texttt{guix help}} \dots bah c’est automatique :-)
    \end{center}
    Normalement, si tout se passe bien,
    \begin{center}
      \texttt{guix time-machine -C channels.scm -{}- shell -m manifest.scm}
    \end{center}
    Guix va regarder, dans l’ordre,
    \begin{itemize}
    \item ce qui est disponible sur le serveur de substituts
    \item si les sources sont à leur endroit attendu \hfill (défini par \texttt{source})
    \item s’il les trouve sur Software Heritage
    \end{itemize}
  \end{itemize}

  \begin{center}
    Même les dépôts des canaux (\texttt{channels.scm})
  \end{center}
\end{frame}



\begin{frame}[fragile]{En pratique (2)}
  \begin{itemize}
  \item   Alice développe un outil dont les sources sont à :
    \begin{center}
      \texttt{https://gitlab.inria.fr/projet/un-outil.git}
    \end{center}
    et l’empaquette elle-même via un canal :
    \begin{center}
      \texttt{https://gitlab.inria.fr/projet/un-canal.git}
    \end{center}
  \item Alice sauvegarde sur Software Heritage ces deux dépôts
  \item Alice joint à sa publication les deux fichiers \texttt{channels.scm}
    et \texttt{manifest.scm}
  \item Carole génère le même environnement qu’Alice avec :

    \hfill \dots même si les serveurs \texttt{inria.fr} sont éteints !
    \begin{center}
      \texttt{guix time-machine -C channels.scm -{}- shell -m manifest.scm}
    \end{center}
  \end{itemize}
\end{frame}


\begin{frame}[fragile]{En pratique (3)}
  \begin{exampleblock}{}
\begin{verbatim}
$ guix time-machine -C channels.scm -- shell -m manifest.scm
Updating channel 'guix' from Git repository at 'https://git.savannah.gnu.org/git/guix.git'...
Updating channel 'example' from Git repository at 'https://whatever-here.org/does-not-matter.git'...
SWH: found revision 67c9f2143aa6f545419ae913b4ae02af4cd3effc with directory at 'https://archive.softwareheritage.org/api/1/directory/fe423e88ce277d3fc230c88d408e42b14a3a458c/'
SWH vault: requested bundle cooking, waiting for completion...
swh:1:rev:67c9f2143aa6f545419ae913b4ae02af4cd3effc.git/
[...]
fatal: could not read Username for 'https://github.com': No such device or address
Trying content-addressed mirror at berlin.guix.gnu.org...
Trying to download from Software Heritage...
SWH: found revision e1eefd033b8a2c4c81babc6fde08ebb116c6abb8 with directory at'https://archive.softwareheritage.org/api/1/directory/c3e538ed2de412d54c567ed7c8cfc46cbbc35d07/'
[...]
\end{verbatim}
  \end{exampleblock}

  \begin{center}
    \href{https://simon.tournier.info/posts/2021-10-25-software-heritage.html}%
    {https://simon.tournier.info/posts/2021-10-25-software-heritage.html}
  \end{center}
\end{frame}



\begin{frame}[fragile]{Oui, mais\dots}
  \begin{itemize}
  \item Comment identifier mon code ?
    \begin{center}
      \href{https://www.softwareheritage.org/2020/07/09/intrinsic-vs-extrinsic-identifiers/}%
      {Identifiant intrinsèque vs extrinsèque (lien)}
    \end{center}
    tag (v1.2.3) vs hash (20303c0), quel hash ? etc.
  \item Comment s’assurer que tout le graphe est bien préservé ?
  \item Que se passe-t-il s'il manque les sources d'un seul n\oe ud ?
  \item On n’a pas parlé du \emph{boostrap} (les racines du graphes).
  \item etc.
  \end{itemize}
\end{frame}

\section{Ce qu’il faut retenir}


\begin{frame}{En bref}
  \begin{itemize}
  \item Une commande
    \begin{center}
      \texttt{guix time-machine -C channels.scm -{}- shell -m manifest.scm}
    \end{center}
    et deux fichiers
  \item Si c'est dans Guix, des efforts (collectif et infrastructure) pour la préservation
  \item Si c'est dans un canal séparé, ça fonctionne mais plus de boulot « manuel »
  \item Difficile de garantir 100\% car il y a beaucoup de cas particuliers
  \item Des progrès\dots et d’autres à venir ?
  \end{itemize}

  \vfill

  \begin{center}
    À vous de compléter
  \end{center}
\end{frame}




\begin{frame}[plain, noframenumbering]{}
  \begin{center}
    \vfill{}
    \Large{%
      \textbf{%
        Des questions ?%
      }}\normalsize

    \vfill{}
    \texttt{guix-science@gnu.org}

    \vfill{}
    \includegraphics[width=0.2\paperwidth]{logo-café}\\
    \href{https://hpc.guix.info/events/2022/café-guix/}{\texttt{https://hpc.guix.info/events/2022/café-guix/}}

    % \vfill{}
    % \tiny{%
    % Copyright \copyright{} 2021 Simon Tournier%
    % }
  \end{center}
\end{frame}


\end{document}
