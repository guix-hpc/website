title: About
---

Guix-HPC is an effort to optimize [GNU Guix](https://guix.gnu.org) for
_reproducible scientific workflows in high-performance computing_ (HPC).
Our [introductory article](/blog/2017/09/guix-hpc-debut) explains how we got
started and gives an overview of what we want to achieve.  We
regularly publish [articles](/blog) on this Web site highlighting specific
features or achievements.  Stay tuned!

Guix-HPC is a joint software development project currently involving
three research institutes: [Inria](https://www.inria.fr/en/towards-reproducible-software-environments-hpc),
the
[Max Delbrück Center for Molecular Medicine (MDC)](https://www.mdc-berlin.de/47864296/en/news/2017/20170905-wissenschaftliches-rechnen-erfolgreich-reproduzieren), [University of Tennessee Health Science Center (UTHSC)](https://genenetwork.org/facilities/)
and the [Utrecht Bioinformatics Center (UBC)](https://ubc.uu.nl/reproducible-software-environments-in-hpc-with-guix/).

Check out our activity reports:
[2024](/blog/2025/02/guix-hpc-activity-report-2024/),
[2023](/blog/2024/02/guix-hpc-activity-report-2023/),
[2022](/blog/2023/02/guix-hpc-activity-report-2022/),
[2021](/blog/2022/02/guix-hpc-activity-report-2021/),
[2020](/blog/2021/02/guix-hpc-activity-report-2020/),
[2019](/blog/2020/02/guix-hpc-activity-report-2019/),
[2018](/blog/2019/02/guix-hpc-activity-report-2018/).

# Articles

The material below covers our work and motivation for Guix-HPC:

  - [_Toward practical transparent verifiable and long-term reproducible
    research using Guix_](https://doi.org/10.1038/s41597-022-01720-9),
    Nature Scientific Data (volume 9, Oct. 2022)
  - [_Reproducibility and Performance: Why
    Choose?_](https://hal.inria.fr/hal-03604971/), [CiSE May–June 2022
    issue](https://www.computer.org/csdl/magazine/cs/2022/03)
    (volume 24, issue 3)
  - [_[Re] Storage Tradeoffs in a Collaborative Backup Service for
    Mobile Devices_](https://doi.org/10.5281/zenodo.3886739),
    ReScience C, June 2020
  - [_PiGx: Reproducible Genomics Analysis Pipelines with
    GNU Guix_](https://doi.org/10.1093/gigascience/giy123), GigaScience
    [ICG-13](http://www.icg-13.org/)
    ([video](https://hpc.guix.info/blog/2019/01/pigx-paper-awarded-at-the-international-conference-on-genomics-icg-13/)),
    Dec. 2018
  - [_Reproducible genomics analysis pipelines with
    GNU Guix_](https://www.biorxiv.org/content/early/2018/04/11/298653),
    Apr. 2018
  - [_Reproducible and User-Controlled Software Environments in HPC with Guix_](https://hal.inria.fr/hal-01161771/en)
    ([slides](https://guix.gnu.org/guix-reppar-20150825.pdf)),
	paper presented at the 2nd International Workshop on Reproducibility
	in Parallel Computing ([RepPar](http://www.reppar.org/2015/)),
	Aug. 2015

Check out the Guix web site for [more
publications](https://guix.gnu.org/en/publications/).

# Talks

The following talks present our work and vision for reproducible
software deployment:

  - [_Reproducibility and performance: why choose? CPU tuning in
    GNU Guix_](https://fosdem.org/2023/schedule/event/cpu_tuning_gnu_guix/),
    [FOSDEM](https://fosdem.org/2023/schedule/track/hpc_big_data_and_data_science/),
    Feb. 2023
  - [_Guix, toward practical transparent, verifiable and long-term
    reproducible
    research_](https://fosdem.org/2023/schedule/event/openresearch_guix/),
    [FOSDEM](https://fosdem.org/2023/schedule/track/open_research_tools_and_technology/),
    Feb. 2023
  - [_Towards reproducible Jupyter
    notebooks_](https://fosdem.org/2020/schedule/event/reprod_jupyter_guix/),
    [FOSDEM](https://fosdem.org/2020/), Feb. 2020
  - [_Beyond Bundles—Reproducible Software Environments with
    GNU Guix_](https://cds.cern.ch/record/2316926), [CERN Computing
    Seminars](http://cseminar.web.cern.ch/cseminar/), May 2018
  - [_Tying software deployment to scientific
    workflows_](https://fosdem.org/2018/schedule/event/guix_workflows/),
    [FOSDEM](https://fosdem.org/2018/), Feb. 2018
  - [_Reproducible and user-controlled software management in HPC with GNU Guix_](https://www.youtube.com/watch?v=cH6wCL6GeOQ&list=PLir-OOQiOhXZX_2zmUJz0fx8RLALi3tkK&index=26) ([PDF](https://guix.gnu.org/guix-bosc-20170724.pdf)),
    [BOSC](https://www.open-bio.org/wiki/BOSC_2017_Schedule), July 2017
  - [_Optimized and Reproducible HPC Deployment_](https://archive.fosdem.org/2017/schedule/event/hpc_deployment_guix/),
	[FOSDEM](https://fosdem.org/2017),
	Feb. 2017
  - [_Workflow Management with GNU Guix_](https://archive.fosdem.org/2017/schedule/event/guixworkflowmanagement/),
	[FOSDEM](https://fosdem.org/2017),
	Feb. 2017

Check out the Guix web site for [more
talks](https://guix.gnu.org/en/blog/tags/talks/).

# Code

Most of the code developed for Guix-HPC aims to consolidate [the code
base](https://git.savannah.gnu.org/cgit/guix.git/)
and [package collection](/browse) of Guix
proper, and thus be pushed upstream.  This has already given rise to a
large collection of bioinformatics, algebra, and R packages, as well as
features to simplify Guix deployment on clusters.

Some auxiliary tools and package sets are maintained elsewhere, or kept
in a staging area until they are mature enough to be submitted for
inclusion in Guix:

  - The [Guix Workflow Language](https://www.guixwl.org/), a lightweight
    framework implementing reproducible computational pipelines.
  - [hpcguix-web](https://github.com/UMCUGenetics/hpcguix-web) is a web
    interface that allows users to search for packages and guides them
    the installation and the job submission process.
  - Check out the [third-party channels](/channels) providing scientific
    and HPC packages!

All this is [free software](https://www.gnu.org/philosophy/free-sw.html)
that you are welcome to use and contribute to!

# Cluster Deployments

Here are known deployments of Guix on clusters and contact information:

  - [Max Delbrück Center for Molecular Medicine](https://www.mdc-berlin.de) (Germany)
      - 250-node cluster + workstations
      - contact: Ricardo Wurmus
  - [Utrecht Bioinformatics Center](https://ubc.uu.nl) (The Netherlands)
      - 68-node cluster (1,000+ cores)
      - contact: Roel Janssen
  - [Australian Centre for Ecogenomics](http://ecogenomic.org/) (Australia)
      - 21-node cluster (1,000 cores)
      - contact: Ben Woodcroft
  - [PlaFRIM](https://www.plafrim.fr/en/home/) (France)
	  - 120-node heterogeneous cluster (3,000+ cores)
	  - contact: Ludovic Courtès
  - [GriCAD](https://gricad.univ-grenoble-alpes.fr/) (France)
	  - 72-node “Dahu” cluster (1,000+ cores)
	  - contact: Violaine Louvet, Pierre-Antoine Bouttier
  - [GliCID](https://www.glicid.fr/) (France)
      - 230-node cluster (4,000+ cores)
      - contact: Yann Dupont
  - [UTHSC](https://uthsc.edu/) (USA)
      - 11-node [HPC Octopus cluster 264 cores)](https://genenetwork.org/facilities/). Pangenome and genetics research cluster
      - contact: [Pjotr Prins](http://thebird.nl/)
  - [Grid’5000](https://www.grid5000.fr/w/Grid5000:Home) (France)
      - 31 clusters, 828 nodes (12,000+ cores)
      - contact: Dimitri Delabroye, Lucas Nussbaum

If you would like to be listed here, please email us at `guix-hpc@gnu.org`.

# Contact

There are several ways we can get in touch:

  - Subscribe to the [`guix-science` mailing
	list](https://lists.gnu.org/mailman/listinfo/guix-science).
  - Email us at `guix-hpc@gnu.org`.
  - Come chat [on the `#guix-hpc` IRC channel of the Libera Chat
    network](https://web.libera.chat/?nick=PotentialUser-?#guix-hpc)…
  - … or on the French-speaking [Café Guix Mattermost
    channel](https://mattermost.univ-nantes.fr/signup_user_complete/?id=njdxbdazafddtq6wsm6cgrr95r)

# Logotype

The [Guix-HPC
logo](https://gitlab.inria.fr/guix-hpc/website/-/blob/master/image-sources/guixhpc-logo.svg)
was designed by Roel Janssen as a derivative of the [Guix
logo](https://guix.gnu.org/en/graphics/).  It is distributed under the
following terms:

> Copyright © 2015 Luis Felipe López Acevedo
>
> Copyright © 2017 Roel Janssen
>
> Permission is granted to copy, distribute and/or modify this work
> under the terms of the [Creative Commons Attribution-ShareAlike 4.0
> International License (CC-BY-SA
> 4.0)](https://creativecommons.org/licenses/by-sa/4.0/).

# Join us!

If you are using Guix in an HPC context at your workplace, or if you
would like to discuss ways to address your own HPC use cases, or if you
are an HPC vendor interested in improving the software deployment
experience for your users, please consider joining us!
