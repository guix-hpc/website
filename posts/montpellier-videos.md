title: Videos of the 2023 workshop are on-line
author: Céline Acary-Robert, Pierre-Antoine Bouttier, Ludovic Courtès, Alexandre Dehne-Garcia, Simon Tournier
date: 2024-01-29 10:00:00
tags: Community, Conferences, Talks
---

Back in November, the [First Workshop on Reproducible Software
Environments for Research and High-Performance
Computing](https://hpc.guix.info/events/2023/workshop/) was held in
Montpellier, France.  Coming from France primarily but also from
Czechia, Germany, the Netherlands, Slovakia, Spain, and the United
Kingdom to name a few, 120 people—scientists, high-performance computing
(HPC) practitioners, system administrators, and enthusiasts alike—came
to listen to the talks, attend the tutorials, and talk to one another.

![Group picture of the attendees on Friday, November 10th, 2023.  By Tess Gobain.](https://hpc.guix.info/static/images/workshop-group-photo-2023.jpg)

Our ambition was to gather people from diverse backgrounds with a shared
interest in improving their research workflows and development
practices.  The 11 talks and 8 tutorials, along with the hallway
discussions and group dinner (very nice!), have allowed us to share
skills and experience.

Today, we’re publishing [**videos of the
talks**](https://hpc.guix.info/events/2023/workshop/program) including
short interviews with the
[speakers](https://hpc.guix.info/events/2023/workshop/speakers)
(tutorials were not recorded but supporting material is linked from the
[program](https://hpc.guix.info/events/2023/workshop/program)).

Our gratitude goes to the video team at Institut Agro for taking care of
the live stream during the event and for editing those videos—thank you!
Many thanks to our publicly-funded academic sponsors who made this event
possible: [ISDM](https://isdm.umontpellier.fr/), our primary sponsor for
this event, [Institut Agro](https://www.institut-agro.fr/en) for hosting
the workshop in such a beautiful place, and
[EuroCC²](https://www.eurocc-access.eu/) and [Inria
Academy](https://www.inria-academy.fr/) for their financial and
logistical support.

“When will be the *second* workshop?”, participants would ask as we were
wrapping up.  We don’t know yet, but if you’d like to host the next
edition or to sponsor it, do [get in touch with
us](mailto:contact-guixhpc-days@services.cnrs.fr)!

The *bonus video* below will give you a feel of what the event in
Montpellier was like…

![Short video giving an overview of the event and the venue.](/static/videos/workshop-2023/99-aftermovie.webm)

> *Video by Institut Agro’s video team, published under
> [CC-BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/).
> [Guix
> artwork](https://git.savannah.gnu.org/cgit/guix/guix-artwork.git/tree/promotional/guix-hpc-workshop-2023)
> by Luis Felipe published under
> [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).*

Enjoy!
