title: Celebrating 10 years of Guix in Paris, 16–18 September
author: Ludovic Courtès, Tanguy Le Carrour, Simon Tournier
tags: Community, Conferences, Talks
date: 2022-06-13 15:00:00
slug: celebrating-10-years-of-guix-in-paris
---

It’s been [ten years of
GNU Guix](https://guix.gnu.org/en/blog/2022/10-years-of-stories-behind-guix/)!  To
celebrate, and to share knowledge and enthusiasm, a [birthday
event](https://10years.guix.gnu.org) will take place on **September
16–18th, 2022**, in Paris, France.  The program is being finalized, but
you can [already register](https://10years.guix.gnu.org)!

> **Update** (2022-07-12): [Preliminary
> program](https://10years.guix.gnu.org/program) published!

![10 year anniversary artwork](/static/images/blog/10-years-of-guix_colorful-10.gif)

This is a community event with several twists to it:

  - Friday, September 16th, is dedicated to **reproducible research
    workflows and high-performance computing** (HPC)—the focuses of the
    [Guix-HPC](https://hpc.guix.info) effort.  It will consist of talks
    and experience reports by scientists and practitioners.
  - Saturday targets **Guix and free software enthusiasts**, users and
    developers alike.  We will reflect on ten years of Guix, show what
    it has to offer, and present on-going developments and future
    directions.
  - on Sunday, users, developers, developers-to-be, and other
    contributors will **discuss technical and community topics** and
    join forces for hacking sessions, [unconference
    style](https://en.wikipedia.org/wiki/Unconference).

[Check out the web site](https://10years.guix.gnu.org) and consider
registering as soon as possible so we can better estimate the size of
the birthday cake!

If you’re interested in presenting a topic, in facilitating a session,
or in organizing a hackathon, please get in touch with the organizers at
`guix-birthday-event@gnu.org` and we’ll be happy to make room for you.
We’re also looking for people to help with logistics, in particular
during the event; please let us know if you can give a hand.

Whether you’re a scientist, an enthusiast, or a power user, we’d love to
see you in September.  Stay tuned for updates!

> _Originally published [on the Guix
> blog](https://guix.gnu.org/en/blog/2022/celebrating-10-years-of-guix-in-paris/)._
