title: Announcing the First Workshop on Reproducible Software Environments
author: Simon Tournier, Ludovic Courtès
tags: Community, Conferences, Talks
date: 2023-09-18 14:30:00
slug: first-workshop-on-reproducible-software-environments
---

We’re excited to announce the [First Workshop on Reproducible Software
Environments for Research and High-Performance Computing
(HPC)](https://hpc.guix.info/events/2023/workshop/), which will take
place in **Montpellier, France**, on **November 8–10th, 2023**!  The
[preliminary
program](https://hpc.guix.info/events/2023/workshop/program/) is
on-line, and now’s the time for you to
[register](https://repro4research.sciencesconf.org/registration)!

This event can be seen as a followup to the research session of the [Ten
Years of Guix](https://10years.guix.gnu.org/program/#Friday) event and
the earlier [French-speaking Workshop on Reproducible Software
Environments](https://hpc.guix.info/events/2021/atelier-reproductibilit%C3%A9-environnements/).

The [program](https://hpc.guix.info/events/2023/workshop/program) features
talks by scientists, engineers, and system administrators from different
backgrounds who will share their experience with Guix, as well as tutorials on
GitLab, Guix, and other tools that support scientific workflows—from bioinfo
analyses to HPC and source code archival.

The [list of
speakers](https://hpc.guix.info/events/2023/workshop/speakers) shows a
variety of positions and scientific disciplines—psychology, linear
algebra, biophysics, medicine, bioinfo, system administration—that we believe very
much shows that software environment reproducibility is a cross-cutting
concern that can be tackled whether or not one identifies themself as a
“geek”.

[Yann
Dupont](https://hpc.guix.info/events/2023/workshop/speakers/#Yann-Dupont),
system architect at the GliCID HPC center in France, writes:

> Guix is the perfect Swiss Army knife that every digital plumber
> should have in their toolkit.  We use it extensively, not only to
> enhance the software we offer to researchers, but also to build the
> GLiCID infrastructure.

Working with bioinformatics and genomics research teams, [Ricardo
Wurmus](https://hpc.guix.info/events/2023/workshop/speakers/#Ricardo-Wurmus),
also known for his outstanding contributions to Guix, subscribes to this view:

> As a software engineer working in large and often changing teams I
> depend on Guix to ensure that development environments as well as
> complicated production deployments are free from surprises.  In my role
> to support researchers with complex scientific software environments I
> cannot think of a more flexible and reliable foundation for reproducibly
> customizable deployments to laptops, HPC systems, and the cloud.  I’m
> excited to see that the program is full of experience reports and
> tutorials by experienced HPC practitioners, and I can’t wait to get a
> chance to learn more about how Guix is used in other research
> environments.

To [Nicolas
Vallet](https://hpc.guix.info/events/2023/workshop/speakers/#Nicolas-Vallet),
medical doctor and researcher in the Hematology and Cell Therapy department of
the University Hospital of Tours (France), it’s about sharing research results:

> As a scientist, I've experienced frustration when attempting to run packages
> described in research papers but encountering compatibility issues with my
> system.  My goal is to ensure that my research will be accessible to a wide
> audience, regardless of their location or technical expertise.  Guix has
> provided me with a solution to achieve it.  I'm now proud to share not only
> the raw data and analysis pipeline from my projects, but also detailed
> instructions on how to recreate the transparent computational environment
> used, making my research more accessible to others.

Whether you’re a scientist, a practitioner, a newcomer, or a power user, we’d
love to see you in November.

Stay tuned for updates!
