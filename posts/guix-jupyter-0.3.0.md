title: Guix-Jupyter 0.3.0 released!
slug: guix-jupyter-0.3.0-released
date: 2024-11-14 16:00
author: Ludovic Courtès
tags: Research, Reproducibility, Jupyter, Releases
---

We are pleased to announce
[Guix-Jupyter 0.3.0](https://codeberg.org/guix-science/guix-jupyter), a
long-overdue release of our Guix-powered Jupyter kernel for
self-contained and reproducible notebooks.

![Guix-Jupyter logo.](https://hpc.guix.info/static/images/blog/guix-jupyter/guix-jupyter.png)

This comes after a three-year hiatus (more on that below) and was
largely motivated by recent discussions on notebooks that revealed that
yes, support for reproducible deployment in Jupyter Notebook is still
much needed, at a time where the vast majority of published notebooks
are [not reproducible](https://doi.org/10.1145/3324884.3416585)!  It’s
also the first release made from its [new home at
Codeberg](https://codeberg.org/guix-science/guix-jupyter).

# Getting it

You can obtain it [straight from
Guix](https://hpc.guix.info/package/guix-jupyter) and spawn Jupyter
Notebook with:

```sh
guix shell jupyter guix-jupyter -- jupyter notebook
```

Alternatively, you can get the source [from
Git](https://codeberg.org/guix-science/guix-jupyter):

```sh
git clone https://codeberg.org/guix-science/guix-jupyter
cd guix-jupyter
git checkout v0.3.0  # or dc8cc183a573c63a07863d70ecff1fbf64e1dfc3
git tag -v v0.3.0
```

Running `guix shell --pure` (with no arguments) gives you an interactive
development environment.

# What’s new?

True to its mission of [bringing reproducible deployment to the
execution environment of Jupyter
notebooks](https://hpc.guix.info/blog/2019/10/towards-reproducible-jupyter-notebooks/),
this release adds new features and improvements.

Previous versions included the `;;guix pin` “magic”, which lets you
*pin* (or *lock*) the `guix` channel—the main
[channel](https://guix.gnu.org/manual/devel/en/html_node/Channels.html)
of Guix, with its 30k packages or so.  However it lacked a way to
specify *additional* channels to get access to more packages.

This is now solved with the new `;;guix channel` magic:

![Notebook using the ‘guix channel’ magic.](/static/images/blog/guix-jupyter/channels-magic.png)

In the example above, we fetch the latest version of the
[Guix-Science](https://codeberg.org/guix-science/guix-science) channel
together with the latest version of the Guix channel, following the
[instructions for adding new
channels](https://hpc.guix.info/channel/guix-science).  By passing a
channel spec that includes `commit` references, we can [pin both
channels and any channels they depend
on](https://guix.gnu.org/manual/en/html_node/Replicating-Guix.html).

Other noteworthy improvements are in the built-in kernel for
[Guile](https://www.gnu.org/software/guile) programming.  One of them
is, at long last, support for completion:

![Notebook showing completion of top-level Scheme bindings with the Guile kernel.](/static/images/blog/guix-jupyter/guile-completion.png)

Check out the
[`NEWS`](https://codeberg.org/guix-science/guix-jupyter/src/commit/dc8cc183a573c63a07863d70ecff1fbf64e1dfc3/NEWS)
file for more info.

# Make it yours!

The project has been dormant for the past couple of years.  This is
largely because the previous version “did the job”, but also because
yours, truly, works primarily with HPC practitioners who are more into
[Org-Mode](https://orgmode.org) and/or fiddling at a terminal connected
to their favorite supercomputer.

It remains that Jupyter is a popular and pleasant tool for reproducible
research workflows, but one that desperately lacks a solution to deploy
the execution environment of notebooks in a reproducible fashion.
Guix-Jupyter is perhaps the only one of this kind.  It is more than a
mere proof of concept at this point but it can certainly benefit from
user feedback.

With the move from Inria’s GitLab instance to
[Codeberg](https://codeberg.org/guix-science/guix-jupyter), reporting
issues and submitting merge requests is finally made easier.  Do not shy
away from contributing!  If you’d like to get involved, we can also get
in touch on [the `guix-science` mailing
list](https://lists.gnu.org/mailman/listinfo/guix-science) or on the
usual [Guix communication channels](https://guix.gnu.org/en/contact/).

Until then, happy hacking with Jupyter!
