title: First French-speaking workshop on reproducible software environments
author: Ludovic Courtès, Konrad Hinsen, Simon Tournier
date: 2021-04-09 15:30
tags: Community, Conferences, Talks
---

> _We are organizing the first [French-speaking workshop on the
> reproducibility of software
> environments](https://hpc.guix.info/events/2021/atelier-reproductibilit%C3%A9-environnements/)
> for scientists, engineers, and system administrators.  The workshop
> will take place on-line on **May 17–18th, 2021** from 09:00 to
> 12:30 CEST.  Stay tuned for more reproducible research events!_

Nous avons le plaisir d’annoncer le premier atelier francophone sur la
reproductibilité des environnements logiciels, qui aura lieu en ligne
les matinées des **17 et 18 mai 2021** — programme et informations
pratiques sur [la page de
l’événement](https://hpc.guix.info/events/2021/atelier-reproductibilité-environnements/).

Cet atelier fait suite à l’intérêt porté par la communauté francophone
du calcul scientifique aux questions de reproductibilité, notamment lors
de l’Action Nationale de Formation [UST4HPC
2021](https://calcul.math.cnrs.fr/2021-01-anf-ust4hpc-2021.html) et avec
la [journée reproductibilité de la Société Informatique de France
(SIF)](https://www.societe-informatique-de-france.fr/journee-reproductibilite/)
qui aura lieu le 10 mai.  Elle s’inscrit aussi dans le cadre des
[activités du groupe
Guix-HPC](https://hpc.guix.info/blog/2021/02/guix-hpc-activity-report-2020/).

Au programme, sept retours d’expériences de scientifiques et de
responsables d’administration système sur le déploiement logiciel dans
les centres de calcul avec Guix mais aussi Spack ou `module`, et sur la
création de pipelines reproductibles pour la recherche avec Debian,
Org-Mode et Guix.

Ces exposés seront suivis d’échanges sur les attentes et propositions de
chacun·e, aussi bien du point de vue scientifique qu’en termes
d’administration de centre de calcul.

La participation est libre et gratuite mais nous vous invitons toutefois
à [vous
inscrire](https://hpc.guix.info/events/2021/atelier-reproductibilité-environnements/).
