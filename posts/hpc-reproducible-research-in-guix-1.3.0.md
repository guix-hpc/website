title: HPC & reproducible research in Guix 1.3.0
date: 2021-05-19 14:30:00
author: Simon Tournier, Ludovic Courtès
slug: hpc-reproducible-research-in-guix-1.3.0
tags: Packages, Releases
---

Version 1.3.0 of GNU Guix was [announced a few days
ago](https://guix.gnu.org/en/blog/2021/gnu-guix-1.3.0-released/).  Some 212
people contributed to more than 8,300 commits since version 1.2.0
released in November 2020.  This post focuses on important changes for HPC
users, admins, and scientific practitioners.

# Going declarative

A distinguishing Guix feature is its support for _declarative deployment_:
instead of running a bunch of `guix install` and `guix upgrade` commands, you
run `guix package --manifest=manifest.scm`, where
[`manifest.scm`](https://guix.gnu.org/manual/en/guix.html#profile_002dmanifest)
lists the software you want to install in a snippet that looks like this:

```scheme
;; This is 'manifest.scm'.
(specifications->manifest
  (list "emacs" "hwloc@2.4.1" "gcc-toolchain"))
```

Doing that installs exactly the packages listed.  You can have that file
under version control or share it — a very handy tool from a
reproducible science perspective.

The [new `guix package --export-manifest`
command](https://guix.gnu.org/manual/en/html_node/Invoking-guix-package.html)
produces a manifest based on
the contents _of an existing profile_.  That makes it easy to transition from
the classic “imperative” model, where you run `guix install` as needed, to the
more formal declarative model.  Moreover, this [paves the
way](http://issues.guix.gnu.org/45919#10) to an interesting use case:
replicating a Docker image built with [`guix
pack`](https://guix.gnu.org/manual/en/guix.html#Invoking-guix-pack) where the
useful information is lost.

Likewise, the [new `guix package --export-channels`
command](https://guix.gnu.org/manual/en/html_node/Invoking-guix-package.html)
exports a _channel spec_ “pinned” to the Guix revision that was used to
deploy the packages found in your profile.  The channel spec allows you
to [_replicate
Guix_](https://guix.gnu.org/manual/en/html_node/Replicating-Guix.html)
while the manifest allows you to redeploy the package set of interest —
on a different machine, or at a different point in time.  Assuming
you’ve stored these two files as `channels.scm` and `manifest.scm`,
redeploying the exact same software environment boils down to running
this command:

```
guix time-machine -C channels.scm -- package -m manifest.scm
```

Our vision is that these two files [should accompany scientific
publications](https://hpc.guix.info/blog/2020/06/reproducible-research-articles-from-source-code-to-pdf/)
as an executable, transparent, time-independent spec to recreate the
software environment used in the computational experiment.

# Packaging facilities

New [package
transformations](https://guix.gnu.org/manual/en/html_node/Package-Transformation-Options.html)
were added to the already long list:

  - `--with-patch` provides a way to build a bunch of packages with
    a patch applied to one or several of them.
  - `--with-latest` instructs Guix to (attempt to) build the latest
    upstream version of a given package.

Packagers, do not miss the new Go recursive importer available with [`guix
import`](https://guix.gnu.org/manual/en/html_node/Invoking-guix-import.html),
that can create package definitions or templates thereof for whole sets of Go
packages.  The `guix import crate` command, for Rust packages, now honors
“semantic versioning” when used in recursive mode.

Last but not least, the introduction of the `GUIX_EXTENSIONS_PATH` Guix search
path makes it possible for Guix extensions, such as the [Guix Workflow
Language (GWL)](https://guixwl.org/), to have their modules automatically
discovered.

# Faster substitutes

One thing you will hopefully notice is that substitute installation
(downloading pre-built binaries) became faster, [as we explained
before](https://guix.gnu.org/en/blog/2021/getting-bytes-to-disk-more-quickly/).
This is in part due to the opportunistic use of zstd compression, which
has a high decompression throughput.  The daemon and [`guix
publish`](https://guix.gnu.org/manual/en/html_node/Invoking-guix-publish.html)
support zstd as an additional compression method, next to gzip and lzip.

Another change that can help fetch substitutes more quickly is _local
substitute server discovery_.  The [new `--discover` option of
`guix-daemon`](https://guix.gnu.org/manual/en/html_node/Invoking-guix_002ddaemon.html)
instructs it to discover and use substitute servers on the local-area
network (LAN) advertised with the mDNS/DNS-SD protocols, using Avahi.
Similarly, `guix publish` has a [new `--advertise`
option](https://guix.gnu.org/manual/en/html_node/Invoking-guix-publish.html)
to advertise itself on the LAN.

On some machines with limited resources, building the Guix modules is
an expensive operation.  A new procedure,
`channel-with-substitutes-available` from the `(guix ci)` module, can
now be used to pull Guix to the latest commit which has already been
built by the build farm.  Refer to the documentation for [an
example](https://guix.gnu.org/manual/en/html_node/Channels-with-Substitutes.html).

# More scientific packages

Here are highlights among the 2,000 packages added and 3,100 packages
upgraded since the previous release:

 - [GCC](https://hpc.guix.info/package/gcc-toolchain) 10.3.0 is available.
 - [Julia](https://hpc.guix.info/package/julia) has been upgraded to
    1.5.3, and Guix now provides 45 Julia packages.
 - [Octave](https://hpc.guix.info/package/octave) has been upgraded to
    6.2.0.
 - [Scipy](https://hpc.guix.info/package/python-scipy) has been upgraded to
   1.6.0 and [Python](https://hpc.guix.info/package/python) to 3.8.2.
 - [GHC]((https://hpc.guix.info/package/ghc)) 8.8.3 is available and
   [OCaml](https://hpc.guix.info/package/ocaml) 4.11.1 as well.  OCaml is now
   [bootstrapped](https://www.freelists.org/post/bootstrappable/Announcing-the-bootstrap-of-OCaml)
   entirely from source _via_ [camlboot](https://github.com/Ekdohibs/camlboot/).
 - [Open MPI](https://hpc.guix.info/package/openmpi) is now at 4.1.0.
 - For statisticians, [R](https://hpc.guix.info/package/r) has been upgraded
   to 4.0.4 and there’s now a total of 1,639 R packages, many of which come
   from [Bioconductor](https://www.bioconductor.org/) 3.12.

The Guix reference manual *is fully translated* into
[French](https://guix.gnu.org/manual/fr/html_node/),
[German](https://guix.gnu.org/manual/de/html_node/), and
[Spanish](https://guix.gnu.org/manual/es/html_node/), with [preliminary
translations](https://translate.fedoraproject.org/projects/guix/documentation-manual/)
in [Russian](https://guix.gnu.org/manual/ru/html_node/),
[Chinese](https://guix.gnu.org/manual/zh-cn/html_node/), and other
languages.  Guix itself is fully translated in French, German, and
Slovak, and partially translated in [almost twenty other
languages](https://translate.fedoraproject.org/projects/guix/guix/).
Translations are [now handled on
Weblate](https://translate.fedoraproject.org/projects/guix/), and [you
can
help](https://guix.gnu.org/manual/devel/en/html_node/Translating-Guix.html)!

Do not miss the [release
notes](https://guix.gnu.org/blog/2021/gnu-guix-1.3.0-released/) for more.

### Try it!

The [installation script](https://guix.gnu.org/install.sh) has been
improved to allow for more automation.  Now, you could run it with:

```
# yes | ./install.sh
```

to proceed all the Guix binary install on any Linux system without any prompt!

You may also be interested in trying the [Guix System demonstration VM
image](https://ftp.gnu.org/gnu/guix/guix-system-vm-image-1.3.0.x86_64-linux.qcow2)
which now supports clipboard integration with the host and dynamic
resizing thanks to the SPICE protocol.

If you’re on Debian or a derivative distribution, you will soon be able to
[`apt install guix`](https://packages.debian.org/guix).

For first-time users, do not miss the [“Getting
Started”](https://guix.gnu.org/manual/en/html_node/Getting-Started.html)
section and try out `guix help` to get an overview of the available commands.

Enjoy the new release!
