title: Guix-HPC at FOSDEM
author: Ludovic Courtès
date: 2023-01-24 14:00
tags: Conferences, Talks, FOSDEM
---

As has been the case [for 9 years
(!)](https://guix.gnu.org/en/blog/tags/fosdem/), Guix will be present at
[FOSDEM](https://fosdem.org/2023), the big annual free software
developer conference in Europe.  There will be [no less than ten
Guix-related
talks](https://guix.gnu.org/blog/2023/meet-guix-at-fosdem-2023/), of
which the following are particularly relevant to the HPC and
reproducible research communities:

  - In the [Open Research Tools
    track](https://fosdem.org/2023/schedule/track/open_research_tools_and_technology/),
    [_Guix, toward practical transparent, verifiable and long-term
    reproducible
    research_](https://fosdem.org/2023/schedule/event/openresearch_guix/)
    will be an introduction to Guix (by Simon Tournier) for an audience
    of scientists interested in coming up with scientific practices that
    improves verifiability and transparency.
  - In the [RISC-V
    track](https://fosdem.org/2023/schedule/track/risc_v/), Efraim
    Flashner will talk about the latest breakthroughs in [_Porting
    RISC-V to
    GNU Guix_](https://fosdem.org/2023/schedule/event/rv_gnu_guix/)—and
    the other way around.
  - In the [HPC
    track](https://fosdem.org/2023/schedule/track/hpc_big_data_and_data_science/),
    Ludovic Courtès will give a lightning talk about CPU tuning in Guix
    entitled [_Reproducibility and performance: why
    choose?_](https://fosdem.org/2023/schedule/event/cpu_tuning_gnu_guix/).

There are lots of exciting talks in each of these tracks, check it out!
Talks will be live-streamed so you can join and chat with us even if
you’re not physically present.

Prior to FOSDEM, the community will meet in person for the [Guix
Days](https://guix.gnu.org/blog/2023/meet-guix-at-fosdem-2023/), two
days to informally discuss organizational matters, technical issues, and
road maps.

See you in Brussels!
