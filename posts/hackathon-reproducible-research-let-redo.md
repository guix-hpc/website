title: Reproducible Research Hackathon—let’s redo!
slug: reproducible-research-hackathon-let-redo
author: Simon Tournier
date: 2023-05-12 12:00
tags: Community, Reproducibility
---

It's time to run the second Reproducible Research hackathon!  The first one
was
from... [2020](/blog/2020/07/reproducible-research-hackathon-experience-report/),
already!  The date: **Tuesday June, 27th**.  Start: 9h30 (CEST) End: 17h30.

> **Update**: Check out [this
> report](https://hpc.guix.info/blog/2023/07/reproducible-research-hackathon-experience-report/)
> about the hackathon.

We propose to collectively tackle some of the issues about reproducible
research:

 - identify stumbling blocks in using Guix to write end-to-end pipelines,
 - document how to achieve this,
 - feed the [Guix-Past](https://gitlab.inria.fr/guix-hpc/guix-past) channel
    by other old packages,
 - provide `guix.scm` for some papers already published.

Anyone is welcome!  Feel free to join if you would like to hack with us.

We suggest to pick articles from the [ReScience
C](https://rescience.github.io/) or [COMPUTO](https://computo.sfds.asso.fr/) –
they provide a high level of transparency about the materials required for
redoing.  The best experiment would to choose articles from
[2020](https://rescience.github.io/read/#volume-6-2020).  As a warm up, maybe
[Courtès, L., _Storage Tradeoffs in a Collaborative Backup Service for Mobile
Devices_](https://rescience.github.io/bibliography/Courtes_2020.html)?  Or else
from the [Ten Years Reproducibility
Challenge](https://rescience.github.io/ten-years/) which took advantage of
GNU Guix.  If you prefer to work on your own topic that you would like to
redo, you are welcome.

We will meet **Tuesday 27th June** at **9:30 CEST** on the `#guix-hpc` channel
of irc.libera.chat You can use this [web
client](https://web.libera.chat/?nick=PotentialUser-?#guix-hpc) (set the
nickname you wish) to reach us.  We will provide a link for one BigBlueButton
instance (video meeting), stay tuned!

> ▶ Join us on [BigBlueButton](https://bbb.inria.fr/cou-pmi-g09-ute) at
> 9:30 CEST! ◀
>
> Here’s a [pad 🗒](https://codimd.math.cnrs.fr/DZB3kDhZTT-HxwIm-wc0Dw)
> for note-taking during the day.

At the end of day, we would like to draw the lines of an experiment report
summarizing the successes or the roadblocks.

--- 

There's a lot we can do and we'd love to [hear your
ideas](https://hpc.guix.info/about)!

Drop us an email at [`guix-science@gnu.org`](mailto:guix-science@gnu.org).
